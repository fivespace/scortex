--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: sergey; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO sergey;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: sergey
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO sergey;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sergey
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: sergey; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO sergey;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: sergey
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO sergey;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sergey
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: sergey; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO sergey;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: sergey
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO sergey;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sergey
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: sergey; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone NOT NULL,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(75) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO sergey;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: sergey; Tablespace: 
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO sergey;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: sergey
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO sergey;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sergey
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: sergey
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO sergey;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sergey
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: sergey; Tablespace: 
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO sergey;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: sergey
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO sergey;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sergey
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: sergey; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO sergey;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: sergey
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO sergey;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sergey
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: sergey; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO sergey;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: sergey
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO sergey;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sergey
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: sergey; Tablespace: 
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO sergey;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: sergey
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO sergey;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sergey
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: sergey; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO sergey;

--
-- Name: fabrics_category; Type: TABLE; Schema: public; Owner: sergey; Tablespace: 
--

CREATE TABLE fabrics_category (
    id integer NOT NULL,
    title character varying(100) NOT NULL,
    slug character varying(200),
    is_public boolean NOT NULL,
    section_id integer
);


ALTER TABLE public.fabrics_category OWNER TO sergey;

--
-- Name: fabrics_category_id_seq; Type: SEQUENCE; Schema: public; Owner: sergey
--

CREATE SEQUENCE fabrics_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fabrics_category_id_seq OWNER TO sergey;

--
-- Name: fabrics_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sergey
--

ALTER SEQUENCE fabrics_category_id_seq OWNED BY fabrics_category.id;


--
-- Name: fabrics_fabric; Type: TABLE; Schema: public; Owner: sergey; Tablespace: 
--

CREATE TABLE fabrics_fabric (
    id integer NOT NULL,
    title character varying(200) NOT NULL,
    description text NOT NULL,
    slug character varying(200),
    is_public boolean NOT NULL,
    category_id integer NOT NULL
);


ALTER TABLE public.fabrics_fabric OWNER TO sergey;

--
-- Name: fabrics_fabric_id_seq; Type: SEQUENCE; Schema: public; Owner: sergey
--

CREATE SEQUENCE fabrics_fabric_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fabrics_fabric_id_seq OWNER TO sergey;

--
-- Name: fabrics_fabric_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sergey
--

ALTER SEQUENCE fabrics_fabric_id_seq OWNED BY fabrics_fabric.id;


--
-- Name: fabrics_section; Type: TABLE; Schema: public; Owner: sergey; Tablespace: 
--

CREATE TABLE fabrics_section (
    id integer NOT NULL,
    title character varying(100) NOT NULL,
    slug character varying(200),
    is_public boolean NOT NULL
);


ALTER TABLE public.fabrics_section OWNER TO sergey;

--
-- Name: fabrics_section_id_seq; Type: SEQUENCE; Schema: public; Owner: sergey
--

CREATE SEQUENCE fabrics_section_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fabrics_section_id_seq OWNER TO sergey;

--
-- Name: fabrics_section_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sergey
--

ALTER SEQUENCE fabrics_section_id_seq OWNED BY fabrics_section.id;


--
-- Name: site_content_indexslider; Type: TABLE; Schema: public; Owner: sergey; Tablespace: 
--

CREATE TABLE site_content_indexslider (
    id integer NOT NULL,
    image character varying(100) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    is_public boolean NOT NULL
);


ALTER TABLE public.site_content_indexslider OWNER TO sergey;

--
-- Name: site_content_indexslider_id_seq; Type: SEQUENCE; Schema: public; Owner: sergey
--

CREATE SEQUENCE site_content_indexslider_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.site_content_indexslider_id_seq OWNER TO sergey;

--
-- Name: site_content_indexslider_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sergey
--

ALTER SEQUENCE site_content_indexslider_id_seq OWNED BY site_content_indexslider.id;


--
-- Name: site_content_page; Type: TABLE; Schema: public; Owner: sergey; Tablespace: 
--

CREATE TABLE site_content_page (
    id integer NOT NULL,
    alias character varying(200) NOT NULL,
    slug character varying(200) NOT NULL
);


ALTER TABLE public.site_content_page OWNER TO sergey;

--
-- Name: site_content_page_id_seq; Type: SEQUENCE; Schema: public; Owner: sergey
--

CREATE SEQUENCE site_content_page_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.site_content_page_id_seq OWNER TO sergey;

--
-- Name: site_content_page_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sergey
--

ALTER SEQUENCE site_content_page_id_seq OWNED BY site_content_page.id;


--
-- Name: site_content_text; Type: TABLE; Schema: public; Owner: sergey; Tablespace: 
--

CREATE TABLE site_content_text (
    id integer NOT NULL,
    header character varying(200) NOT NULL,
    content text NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    page_id integer,
    attachement character varying(100)
);


ALTER TABLE public.site_content_text OWNER TO sergey;

--
-- Name: site_content_text_id_seq; Type: SEQUENCE; Schema: public; Owner: sergey
--

CREATE SEQUENCE site_content_text_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.site_content_text_id_seq OWNER TO sergey;

--
-- Name: site_content_text_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sergey
--

ALTER SEQUENCE site_content_text_id_seq OWNED BY site_content_text.id;


--
-- Name: testimonials_testimonial; Type: TABLE; Schema: public; Owner: sergey; Tablespace: 
--

CREATE TABLE testimonials_testimonial (
    id integer NOT NULL,
    author character varying(200) NOT NULL,
    body text NOT NULL,
    company character varying(200) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


ALTER TABLE public.testimonials_testimonial OWNER TO sergey;

--
-- Name: testimonials_testimonial_id_seq; Type: SEQUENCE; Schema: public; Owner: sergey
--

CREATE SEQUENCE testimonials_testimonial_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.testimonials_testimonial_id_seq OWNER TO sergey;

--
-- Name: testimonials_testimonial_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sergey
--

ALTER SEQUENCE testimonials_testimonial_id_seq OWNED BY testimonials_testimonial.id;


--
-- Name: wares__sex_choices_model; Type: TABLE; Schema: public; Owner: sergey; Tablespace: 
--

CREATE TABLE wares__sex_choices_model (
    id integer NOT NULL,
    title character varying(7) NOT NULL,
    ware_fk_id integer NOT NULL
);


ALTER TABLE public.wares__sex_choices_model OWNER TO sergey;

--
-- Name: wares__sex_choices_model_id_seq; Type: SEQUENCE; Schema: public; Owner: sergey
--

CREATE SEQUENCE wares__sex_choices_model_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wares__sex_choices_model_id_seq OWNER TO sergey;

--
-- Name: wares__sex_choices_model_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sergey
--

ALTER SEQUENCE wares__sex_choices_model_id_seq OWNED BY wares__sex_choices_model.id;


--
-- Name: wares__size_choices_model; Type: TABLE; Schema: public; Owner: sergey; Tablespace: 
--

CREATE TABLE wares__size_choices_model (
    id integer NOT NULL,
    title character varying(3) NOT NULL,
    ware_fk_id integer NOT NULL
);


ALTER TABLE public.wares__size_choices_model OWNER TO sergey;

--
-- Name: wares__size_choices_model_id_seq; Type: SEQUENCE; Schema: public; Owner: sergey
--

CREATE SEQUENCE wares__size_choices_model_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wares__size_choices_model_id_seq OWNER TO sergey;

--
-- Name: wares__size_choices_model_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sergey
--

ALTER SEQUENCE wares__size_choices_model_id_seq OWNED BY wares__size_choices_model.id;


--
-- Name: wares__ware_colors_model; Type: TABLE; Schema: public; Owner: sergey; Tablespace: 
--

CREATE TABLE wares__ware_colors_model (
    id integer NOT NULL,
    title character varying(20) NOT NULL,
    hex_code character varying(6) NOT NULL,
    ware_fkey_id integer NOT NULL
);


ALTER TABLE public.wares__ware_colors_model OWNER TO sergey;

--
-- Name: wares__ware_colors_model_id_seq; Type: SEQUENCE; Schema: public; Owner: sergey
--

CREATE SEQUENCE wares__ware_colors_model_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wares__ware_colors_model_id_seq OWNER TO sergey;

--
-- Name: wares__ware_colors_model_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sergey
--

ALTER SEQUENCE wares__ware_colors_model_id_seq OWNED BY wares__ware_colors_model.id;


--
-- Name: wares__ware_images_model; Type: TABLE; Schema: public; Owner: sergey; Tablespace: 
--

CREATE TABLE wares__ware_images_model (
    id integer NOT NULL,
    image character varying(100) NOT NULL,
    ware_foreign_k_id integer NOT NULL
);


ALTER TABLE public.wares__ware_images_model OWNER TO sergey;

--
-- Name: wares_category; Type: TABLE; Schema: public; Owner: sergey; Tablespace: 
--

CREATE TABLE wares_category (
    id integer NOT NULL,
    title character varying(100) NOT NULL,
    slug character varying(200),
    description text,
    image character varying(100),
    is_public boolean NOT NULL,
    is_on_home_page boolean NOT NULL,
    description_bottom text
);


ALTER TABLE public.wares_category OWNER TO sergey;

--
-- Name: wares_category_id_seq; Type: SEQUENCE; Schema: public; Owner: sergey
--

CREATE SEQUENCE wares_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wares_category_id_seq OWNER TO sergey;

--
-- Name: wares_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sergey
--

ALTER SEQUENCE wares_category_id_seq OWNED BY wares_category.id;


--
-- Name: wares_order; Type: TABLE; Schema: public; Owner: sergey; Tablespace: 
--

CREATE TABLE wares_order (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    phone character varying(18) NOT NULL,
    email character varying(254) NOT NULL,
    order_items character varying(300),
    colors character varying(300),
    sexes character varying(300),
    sizes character varying(300),
    order_qty integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


ALTER TABLE public.wares_order OWNER TO sergey;

--
-- Name: wares_order_id_seq; Type: SEQUENCE; Schema: public; Owner: sergey
--

CREATE SEQUENCE wares_order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wares_order_id_seq OWNER TO sergey;

--
-- Name: wares_order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sergey
--

ALTER SEQUENCE wares_order_id_seq OWNED BY wares_order.id;


--
-- Name: wares_ware; Type: TABLE; Schema: public; Owner: sergey; Tablespace: 
--

CREATE TABLE wares_ware (
    id integer NOT NULL,
    title character varying(200) NOT NULL,
    slug character varying(200),
    consist text,
    image character varying(100) NOT NULL,
    brand character varying(200),
    price integer,
    is_public boolean NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    category_id integer NOT NULL
);


ALTER TABLE public.wares_ware OWNER TO sergey;

--
-- Name: wares_ware_id_seq; Type: SEQUENCE; Schema: public; Owner: sergey
--

CREATE SEQUENCE wares_ware_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wares_ware_id_seq OWNER TO sergey;

--
-- Name: wares_ware_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sergey
--

ALTER SEQUENCE wares_ware_id_seq OWNED BY wares_ware.id;


--
-- Name: wares_wareimagesmodel_id_seq; Type: SEQUENCE; Schema: public; Owner: sergey
--

CREATE SEQUENCE wares_wareimagesmodel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wares_wareimagesmodel_id_seq OWNER TO sergey;

--
-- Name: wares_wareimagesmodel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sergey
--

ALTER SEQUENCE wares_wareimagesmodel_id_seq OWNED BY wares__ware_images_model.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY fabrics_category ALTER COLUMN id SET DEFAULT nextval('fabrics_category_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY fabrics_fabric ALTER COLUMN id SET DEFAULT nextval('fabrics_fabric_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY fabrics_section ALTER COLUMN id SET DEFAULT nextval('fabrics_section_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY site_content_indexslider ALTER COLUMN id SET DEFAULT nextval('site_content_indexslider_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY site_content_page ALTER COLUMN id SET DEFAULT nextval('site_content_page_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY site_content_text ALTER COLUMN id SET DEFAULT nextval('site_content_text_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY testimonials_testimonial ALTER COLUMN id SET DEFAULT nextval('testimonials_testimonial_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY wares__sex_choices_model ALTER COLUMN id SET DEFAULT nextval('wares__sex_choices_model_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY wares__size_choices_model ALTER COLUMN id SET DEFAULT nextval('wares__size_choices_model_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY wares__ware_colors_model ALTER COLUMN id SET DEFAULT nextval('wares__ware_colors_model_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY wares__ware_images_model ALTER COLUMN id SET DEFAULT nextval('wares_wareimagesmodel_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY wares_category ALTER COLUMN id SET DEFAULT nextval('wares_category_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY wares_order ALTER COLUMN id SET DEFAULT nextval('wares_order_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY wares_ware ALTER COLUMN id SET DEFAULT nextval('wares_ware_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: sergey
--

COPY auth_group (id, name) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sergey
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: sergey
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sergey
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: sergey
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add permission	2	add_permission
5	Can change permission	2	change_permission
6	Can delete permission	2	delete_permission
7	Can add group	3	add_group
8	Can change group	3	change_group
9	Can delete group	3	delete_group
10	Can add user	4	add_user
11	Can change user	4	change_user
12	Can delete user	4	delete_user
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can add Слайд	7	add_indexslider
20	Can change Слайд	7	change_indexslider
21	Can delete Слайд	7	delete_indexslider
22	Can add Страница	8	add_page
23	Can change Страница	8	change_page
24	Can delete Страница	8	delete_page
25	Can add Тестовый блок	9	add_text
26	Can change Тестовый блок	9	change_text
27	Can delete Тестовый блок	9	delete_text
28	Can add Отзыв	10	add_testimonial
29	Can change Отзыв	10	change_testimonial
30	Can delete Отзыв	10	delete_testimonial
31	Can add Категория	11	add_category
32	Can change Категория	11	change_category
33	Can delete Категория	11	delete_category
34	Can add Материал	12	add_ware
35	Can change Материал	12	change_ware
36	Can delete Материал	12	delete_ware
37	Can add Изображение	13	add__ware_images_model
38	Can change Изображение	13	change__ware_images_model
39	Can delete Изображение	13	delete__ware_images_model
40	Can add Размер	14	add__size_choices_model
41	Can change Размер	14	change__size_choices_model
42	Can delete Размер	14	delete__size_choices_model
43	Can add Пол	15	add__sex_choices_model
44	Can change Пол	15	change__sex_choices_model
45	Can delete Пол	15	delete__sex_choices_model
46	Can add Цвет	16	add__ware_colors_model
47	Can change Цвет	16	change__ware_colors_model
48	Can delete Цвет	16	delete__ware_colors_model
49	Can add Заказ	17	add_order
50	Can change Заказ	17	change_order
51	Can delete Заказ	17	delete_order
52	Can add Раздел	18	add_section
53	Can change Раздел	18	change_section
54	Can delete Раздел	18	delete_section
55	Can add Категория	19	add_category
56	Can change Категория	19	change_category
57	Can delete Категория	19	delete_category
58	Can add Ткань	20	add_fabric
59	Can change Ткань	20	change_fabric
60	Can delete Ткань	20	delete_fabric
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sergey
--

SELECT pg_catalog.setval('auth_permission_id_seq', 60, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: sergey
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$12000$PoqZhu9Yqlb5$IHRrO05+U9fuMt020HdX8HH0B5d/wgOpoZ17OlktZBc=	2015-01-14 14:15:31.098592+04	t	sergey				t	t	2015-01-14 12:35:01.820525+04
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: sergey
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sergey
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sergey
--

SELECT pg_catalog.setval('auth_user_id_seq', 1, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: sergey
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sergey
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: sergey
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2015-01-14 12:35:33.65946+04	1	Макеты	1		8	1
2	2015-01-14 12:36:03.454276+04	1	Макеты	3		8	1
3	2015-01-14 12:45:33.853268+04	2	Домашняя	1		8	1
4	2015-01-14 12:48:02.241699+04	3	Ткани	1		8	1
5	2015-01-14 12:49:38.639247+04	4	Оплата	1		8	1
6	2015-01-14 12:51:20.53955+04	5	Доставка	1		8	1
7	2015-01-14 12:52:44.950033+04	6	Контакты	1		8	1
8	2015-01-14 12:54:40.671039+04	6	Контакты	2	Ни одно поле не изменено.	8	1
9	2015-01-14 12:56:11.072737+04	7	О компании	1		8	1
10	2015-01-14 12:58:51.769171+04	8	Документы	1		8	1
11	2015-01-14 12:59:30.487703+04	8	Документы	2	Изменены attachement для Тестовый блок "". Изменены attachement для Тестовый блок "". Изменены attachement для Тестовый блок "". Изменены attachement для Тестовый блок "".	8	1
12	2015-01-14 13:00:03.250787+04	9	Макеты	1		8	1
13	2015-01-14 13:01:04.4713+04	9	Макеты	2	Добавлен Тестовый блок "". Добавлен Тестовый блок "". Добавлен Тестовый блок "". Добавлен Тестовый блок "".	8	1
14	2015-01-14 13:01:23.103292+04	9	Макеты	2	Ни одно поле не изменено.	8	1
15	2015-01-14 13:02:48.27005+04	10	Нанесение	1		8	1
16	2015-01-14 13:35:18.487223+04	2	Домашняя	2	Изменены content для Тестовый блок "".	8	1
17	2015-01-14 14:10:17.423458+04	10	Нанесение	2	Добавлен Тестовый блок "". Добавлен Тестовый блок "".	8	1
18	2015-01-14 14:15:58.593136+04	1	Синтетические ткани. 100% полиэстер	1		18	1
19	2015-01-14 14:16:16.211851+04	2	Хлопчатобумажные ткани. 100% хлопок	1		18	1
20	2015-01-14 14:17:19.71622+04	1	Трикотажные изделия	1		19	1
21	2015-01-14 14:17:58.973427+04	2	Эко-сумки и фартуки	1		19	1
22	2015-01-14 14:18:14.05605+04	3	Халаты и полотенца	1		19	1
23	2015-01-14 14:18:26.246978+04	4	Платки	1		19	1
24	2015-01-14 14:19:20.381955+04	1	Синтетические ткани. 100% полиэстер	3		18	1
25	2015-01-14 14:19:26.363502+04	3	Синтетические ткани. 100% полиэстер	1		18	1
26	2015-01-14 14:19:50.456785+04	5	Платки	1		19	1
27	2015-01-14 14:20:14.59134+04	6	Подушки	1		19	1
28	2015-01-14 14:20:23.629916+04	7	Куртки и ветровки	1		19	1
29	2015-01-14 14:20:33.694797+04	8	Спортивная атрибутика	1		19	1
30	2015-01-14 16:56:15.745359+04	8	Спортивная атрибутика	2	Добавлен Ткань "Кулирка". Добавлен Ткань "Интерлок". Добавлен Ткань "Футер".	19	1
31	2015-01-14 16:56:36.98328+04	8	Спортивная атрибутика	2	Удален Ткань "Кулирка". Удален Ткань "Интерлок". Удален Ткань "Футер".	19	1
32	2015-01-14 16:57:19.220484+04	1	Трикотажные изделия	2	Добавлен Ткань "Кулирка". Добавлен Ткань "Интерлок". Добавлен Ткань "Футер".	19	1
33	2015-01-14 16:58:52.223735+04	2	Эко-сумки и фартуки	2	Добавлен Ткань "Бязь". Добавлен Ткань "Двунитка". Добавлен Ткань "Саржа".	19	1
34	2015-01-14 16:59:20.601078+04	3	Халаты и полотенца	2	Добавлен Ткань "Вафельное полотно". Добавлен Ткань "Махровое полотно".	19	1
35	2015-01-14 18:18:40.764924+04	1	Хлопчатобумажные ткани. 100% хлопок	1		18	1
36	2015-01-14 18:18:56.678178+04	2	Синтетические ткани. 100% полиэстер	1		18	1
37	2015-01-14 18:19:31.285668+04	1	Трикотажные изделия	1		19	1
38	2015-01-14 18:19:45.2646+04	2	Эко-сумки и фартуки	1		19	1
39	2015-01-14 18:20:28.306223+04	3	Халаты и полотенца	1		19	1
40	2015-01-14 18:20:40.463942+04	4	Платки	1		19	1
41	2015-01-14 18:20:48.857416+04	5	Подушки	1		19	1
42	2015-01-14 18:23:11.936627+04	6	Куртки и ветровки	1		19	1
43	2015-01-14 18:23:22.962425+04	7	Спортивная атрибутика	1		19	1
44	2015-01-14 18:23:54.011111+04	1	Трикотажные изделия	2	Добавлен Ткань "Кулирка". Добавлен Ткань "Интерлок". Добавлен Ткань "Футер".	19	1
45	2015-01-14 18:24:18.937216+04	2	Эко-сумки и фартуки	2	Добавлен Ткань "Бязь". Добавлен Ткань "Двунитка". Добавлен Ткань "Саржа".	19	1
46	2015-01-14 18:24:37.416345+04	3	Халаты и полотенца	2	Добавлен Ткань "Вафельное полотно". Добавлен Ткань "Махровое полотно".	19	1
47	2015-01-14 18:25:14.425717+04	4	Платки	2	Добавлен Ткань "Шифон". Добавлен Ткань "Шармус". Добавлен Ткань "Мокрый шелк". Добавлен Ткань "Дешайн".	19	1
48	2015-01-14 18:26:44.931022+04	5	Подушки	2	Добавлен Ткань "Атлас". Добавлен Ткань "Сатен". Добавлен Ткань "Микрофибра". Добавлен Ткань "Габардин".	19	1
49	2015-01-14 18:27:29.527573+04	6	Куртки и ветровки	2	Добавлен Ткань "Оксфорд". Добавлен Ткань "Дьюспа". Добавлен Ткань "Тафета".	19	1
50	2015-01-14 18:27:35.785723+04	6	Куртки и ветровки	2	Изменены is_public для Ткань "Тафета".	19	1
51	2015-01-14 18:27:57.839168+04	7	Спортивная атрибутика	2	Добавлен Ткань "Ложная сетка". Добавлен Ткань "Прима".	19	1
52	2015-01-14 18:28:03.655301+04	7	Спортивная атрибутика	2	Изменены is_public для Ткань "Ложная сетка". Изменены is_public для Ткань "Прима".	19	1
53	2015-01-14 18:46:32.485107+04	1	Футболки	1		11	1
54	2015-01-14 18:52:20.781347+04	1	Футболка с коротким рукавом из кулироной глади с круглым воротом	1		12	1
55	2015-01-14 19:07:36.152803+04	5	1231	3		17	1
56	2015-01-14 19:07:36.163412+04	4	qwe	3		17	1
57	2015-01-14 19:07:36.165697+04	3	fs	3		17	1
58	2015-01-14 19:07:36.168079+04	2	jack	3		17	1
59	2015-01-14 19:07:36.170192+04	1	Иван	3		17	1
60	2015-01-19 12:19:43.33168+04	6	Контакты	2	Изменены content для Тестовый блок "". Изменены content для Тестовый блок "".	8	1
61	2015-01-19 12:24:37.597907+04	6	Контакты	2	Изменены content для Тестовый блок "".	8	1
62	2015-01-19 12:29:32.941265+04	6	Контакты	2	Изменены content для Тестовый блок "".	8	1
63	2015-01-19 12:33:47.428346+04	6	Контакты	2	Изменены content для Тестовый блок "".	8	1
64	2015-01-19 12:36:17.06559+04	6	Контакты	2	Изменены content для Тестовый блок "".	8	1
65	2015-01-19 12:55:00.574558+04	1	Футболки	2	Изменен description_bottom.	11	1
66	2015-01-19 13:19:07.608742+04	11	Каталог	1		8	1
67	2015-01-19 14:50:14.754136+04	11	Каталог	2	Изменены header и attachement для Тестовый блок "".	8	1
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sergey
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 67, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: sergey
--

COPY django_content_type (id, name, app_label, model) FROM stdin;
1	log entry	admin	logentry
2	permission	auth	permission
3	group	auth	group
4	user	auth	user
5	content type	contenttypes	contenttype
6	session	sessions	session
7	Слайд	site_content	indexslider
8	Страница	site_content	page
9	Тестовый блок	site_content	text
10	Отзыв	testimonials	testimonial
11	Категория	wares	category
12	Материал	wares	ware
13	Изображение	wares	_ware_images_model
14	Размер	wares	_size_choices_model
15	Пол	wares	_sex_choices_model
16	Цвет	wares	_ware_colors_model
17	Заказ	wares	order
18	Раздел	fabrics	section
19	Категория	fabrics	category
20	Ткань	fabrics	fabric
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sergey
--

SELECT pg_catalog.setval('django_content_type_id_seq', 20, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: sergey
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2015-01-14 12:34:46.493675+04
2	auth	0001_initial	2015-01-14 12:34:46.602314+04
3	admin	0001_initial	2015-01-14 12:34:46.655904+04
5	fabrics	0002_auto_20150112_1941	2015-01-14 12:34:46.749154+04
6	sessions	0001_initial	2015-01-14 12:34:46.761464+04
7	site_content	0001_initial	2015-01-14 12:34:46.771982+04
8	site_content	0002_indexslider_is_public	2015-01-14 12:34:46.782996+04
9	site_content	0003_texts	2015-01-14 12:34:46.795955+04
10	site_content	0004_auto_20141223_0044	2015-01-14 12:34:46.815824+04
11	site_content	0005_auto_20141223_0045	2015-01-14 12:34:46.834619+04
12	site_content	0006_page	2015-01-14 12:34:46.856731+04
13	site_content	0007_text_page	2015-01-14 12:34:46.880047+04
14	site_content	0008_auto_20141223_0102	2015-01-14 12:34:46.920736+04
15	site_content	0009_auto_20141223_0105	2015-01-14 12:34:46.957411+04
16	site_content	0010_auto_20141224_2216	2015-01-14 12:34:46.997202+04
17	site_content	0011_text_attachement	2015-01-14 12:34:47.056303+04
18	testimonials	0001_initial	2015-01-14 12:34:47.068094+04
19	testimonials	0002_auto_20141222_1931	2015-01-14 12:34:47.078122+04
20	wares	0001_initial	2015-01-14 12:34:47.319236+04
21	wares	0002_auto_20141221_1926	2015-01-14 12:34:47.416226+04
22	wares	0003_auto_20141221_1930	2015-01-14 12:34:47.522135+04
23	wares	0004_ware__has_thumb	2015-01-14 12:34:47.634953+04
24	wares	0005_auto_20141221_1942	2015-01-14 12:34:47.739463+04
25	wares	0006_remove_ware_moderator	2015-01-14 12:34:47.85892+04
26	wares	0007_auto_20141221_2032	2015-01-14 12:34:48.038454+04
27	wares	0008_auto_20141222_1937	2015-01-14 12:34:48.18939+04
28	wares	0009_category_is_on_home_page	2015-01-14 12:34:48.355089+04
29	wares	0010_order	2015-01-14 12:34:48.512847+04
30	wares	0011_auto_20141224_0302	2015-01-14 12:34:49.030219+04
31	wares	0012_auto_20141224_2216	2015-01-14 12:34:49.335203+04
32	wares	0013_auto_20150113_0008	2015-01-14 12:34:49.563196+04
33	fabrics	0001_initial	2015-01-14 18:15:22.666187+04
34	fabrics	0002_auto_20150114_1717	2015-01-14 18:17:05.751712+04
35	wares	0014_auto_20150119_1154	2015-01-19 12:54:14.714971+04
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sergey
--

SELECT pg_catalog.setval('django_migrations_id_seq', 35, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: sergey
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
l4zb2t553e2qnzjwlo9f29zz2ygldva9	ZDIxODExYTdlNjJlNDRjZDcwNjUyMTNjNGVkN2I3ODVjODk3YjY3NDp7Il9hdXRoX3VzZXJfaGFzaCI6IjA2OGQ3ZWEwNGQ1MTExN2NlMzI2YmViNGEyZDY2NGY4YWVmMGVhZmYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9	2015-01-28 12:35:18.687156+04
i5mrpfdbztaypa72b9qmgjl9v3qmbo1h	ZDIxODExYTdlNjJlNDRjZDcwNjUyMTNjNGVkN2I3ODVjODk3YjY3NDp7Il9hdXRoX3VzZXJfaGFzaCI6IjA2OGQ3ZWEwNGQ1MTExN2NlMzI2YmViNGEyZDY2NGY4YWVmMGVhZmYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9	2015-01-28 12:43:13.629834+04
92yr7b66cl4h7speiam6pzosi9pqblzt	ZDIxODExYTdlNjJlNDRjZDcwNjUyMTNjNGVkN2I3ODVjODk3YjY3NDp7Il9hdXRoX3VzZXJfaGFzaCI6IjA2OGQ3ZWEwNGQ1MTExN2NlMzI2YmViNGEyZDY2NGY4YWVmMGVhZmYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9	2015-01-28 12:47:19.492021+04
tg955uzpg55exzivzeeg2q9iydft3lbm	ZDIxODExYTdlNjJlNDRjZDcwNjUyMTNjNGVkN2I3ODVjODk3YjY3NDp7Il9hdXRoX3VzZXJfaGFzaCI6IjA2OGQ3ZWEwNGQ1MTExN2NlMzI2YmViNGEyZDY2NGY4YWVmMGVhZmYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9	2015-01-28 14:09:07.1215+04
x4flycwt7ca8aokftrq1yiv4si2dh0m1	ZDIxODExYTdlNjJlNDRjZDcwNjUyMTNjNGVkN2I3ODVjODk3YjY3NDp7Il9hdXRoX3VzZXJfaGFzaCI6IjA2OGQ3ZWEwNGQ1MTExN2NlMzI2YmViNGEyZDY2NGY4YWVmMGVhZmYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9	2015-01-28 14:15:31.100804+04
\.


--
-- Data for Name: fabrics_category; Type: TABLE DATA; Schema: public; Owner: sergey
--

COPY fabrics_category (id, title, slug, is_public, section_id) FROM stdin;
1	Трикотажные изделия	trikotazhnye-izdeliia	t	1
2	Эко-сумки и фартуки	eko-sumki-i-fartuki	t	1
3	Халаты и полотенца	khalaty-i-polotentsa	t	1
4	Платки	platki	t	2
5	Подушки	podushki	t	2
6	Куртки и ветровки	kurtki-i-vetrovki	t	2
7	Спортивная атрибутика	sportivnaia-atributika	t	2
\.


--
-- Name: fabrics_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sergey
--

SELECT pg_catalog.setval('fabrics_category_id_seq', 7, true);


--
-- Data for Name: fabrics_fabric; Type: TABLE DATA; Schema: public; Owner: sergey
--

COPY fabrics_fabric (id, title, description, slug, is_public, category_id) FROM stdin;
1	Кулирка		kulirka	t	1
2	Интерлок		interlok	t	1
3	Футер		futer	t	1
4	Бязь		biaz	t	2
5	Двунитка		dvunitka	t	2
6	Саржа		sarzha	t	2
7	Вафельное полотно		vafelnoe-polotno	t	3
8	Махровое полотно		makhrovoe-polotno	t	3
9	Шифон		shifon	t	4
10	Шармус		sharmus	t	4
11	Мокрый шелк		mokryi-shelk	t	4
12	Дешайн		deshain	t	4
13	Атлас		atlas	t	5
14	Сатен		saten	t	5
15	Микрофибра		mikrofibra	t	5
16	Габардин		gabardin	t	5
17	Оксфорд		oksford	t	6
18	Дьюспа		diuspa	t	6
19	Тафета		tafeta	t	6
20	Ложная сетка		lozhnaia-setka	t	7
21	Прима		prima	t	7
\.


--
-- Name: fabrics_fabric_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sergey
--

SELECT pg_catalog.setval('fabrics_fabric_id_seq', 21, true);


--
-- Data for Name: fabrics_section; Type: TABLE DATA; Schema: public; Owner: sergey
--

COPY fabrics_section (id, title, slug, is_public) FROM stdin;
1	Хлопчатобумажные ткани. 100% хлопок	khlopchatobumazhnye-tkani-100-kh	t
2	Синтетические ткани. 100% полиэстер	sinteticheskie-tkani-100-poli	t
\.


--
-- Name: fabrics_section_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sergey
--

SELECT pg_catalog.setval('fabrics_section_id_seq', 2, true);


--
-- Data for Name: site_content_indexslider; Type: TABLE DATA; Schema: public; Owner: sergey
--

COPY site_content_indexslider (id, image, created_at, is_public) FROM stdin;
\.


--
-- Name: site_content_indexslider_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sergey
--

SELECT pg_catalog.setval('site_content_indexslider_id_seq', 1, false);


--
-- Data for Name: site_content_page; Type: TABLE DATA; Schema: public; Owner: sergey
--

COPY site_content_page (id, alias, slug) FROM stdin;
3	Ткани	tkani
4	Оплата	oplata
5	Доставка	dostavka
7	О компании	o-kompanii
8	Документы	dokumenty
9	Макеты	makety
2	Домашняя	domashniaia
10	Нанесение	nanesenie
6	Контакты	kontakty
11	Каталог	katalog
\.


--
-- Name: site_content_page_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sergey
--

SELECT pg_catalog.setval('site_content_page_id_seq', 11, true);


--
-- Data for Name: site_content_text; Type: TABLE DATA; Schema: public; Owner: sergey
--

COPY site_content_text (id, header, content, created_at, updated_at, page_id, attachement) FROM stdin;
1	Добро пожаловать на наш сайт	<p style="text-align:center">Уже более 5 лет мы занимаемся производством текстильной корпоративной рекламно- сувенирной продукции от простых футболок до бескаркасной мебели, имея за плечами большой опыт сотрудничества с крупнейшими компаниями, можем обеспечить качественный сервис и индивидуальный подход. Наша задача сделать ваш бизнес узнаваемым. Наша миссия делать мир прекрасным</p>\r\n	2015-01-14 12:45:33.831129+04	2015-01-14 12:45:33.831182+04	2	
2	Уникальное торговое предложение	<p style="text-align:center">Приветствую Вас на сайте нашей компании. Уже более 5 лет мы занимаемся производством текстильной корпоративной рекламно-сувенирной продукции от простых футболок до бескаркасной мебели, имея за плечами большой опыт сотрудничества с крупнейшими компаниями, можем обеспечить качественный сервис и индивидуальный подход. Наша задача сделать ваш бизнес узнаваемым. Наша миссия делать мир прекрасным</p>\r\n	2015-01-14 12:45:33.849737+04	2015-01-14 12:45:33.849803+04	2	
3	Слоган	<p>Печать на футболках<br />\r\nоптом и от одной штуки<br />\r\nс доставкой по России</p>\r\n	2015-01-14 12:45:33.851046+04	2015-01-14 12:45:33.851095+04	2	
5	Ткани	<p>Тщательно подобранная ткань придает изделию изысканность, законченность и приятные тактильные ощущения. Все ткани, используемые при пошиве изделий на заказ, подобраны с особой тщательностью, имеют международную сертификацию и гипоаллергенны.</p>\r\n	2015-01-14 12:48:02.240276+04	2015-01-14 12:48:02.240325+04	3	
6	Оплата заказа	<p>Оплатить заказ можно двумя способами: безналичным расчетом и банковской картой</p>\r\n	2015-01-14 12:49:38.636144+04	2015-01-14 12:49:38.636184+04	4	
7	Безналичный расчет	<p>Сотрудником компании предоставляется счет с печатью посредством e-mail, который можно оплатить банковским переводом. Физические лица могут оплатить счет в любом отделении банка.</p>\r\n	2015-01-14 12:49:38.637214+04	2015-01-14 12:49:38.637256+04	4	
8	Банковской картой	<p>Сотрудником компании по средствам e-mail, предоставляется ссылка на счет, который можно оплатить в течении 3-х дней. Оплата производится без комиссий</p>\r\n	2015-01-14 12:49:38.638109+04	2015-01-14 12:49:38.638159+04	4	
9	Доставка	<p>Вам необходима доставка? Не волнуйтесь, мы осуществляем доставку по всей России</p>\r\n	2015-01-14 12:51:20.535937+04	2015-01-14 12:51:20.535973+04	5	
10	Самовывоз	<p>Вы можете забратьзаказ сами в нашем магазине<br />\r\nпо адресу: ул. Большая Черемушкинская, дом 25, стр. 97</p>\r\n	2015-01-14 12:51:20.536778+04	2015-01-14 12:51:20.536803+04	5	
11	Курьером	<p>Доставка малогабаритных грузов пешим курьером, в пределах МКАД от 500 рублей</p>\r\n	2015-01-14 12:51:20.537354+04	2015-01-14 12:51:20.537379+04	5	
12	Крупногабаритные грузы	<p>Доставка крупногабаритных грузов<br />\r\nот 1 000 рублей в пределах МКАД</p>\r\n	2015-01-14 12:51:20.53793+04	2015-01-14 12:51:20.537955+04	5	
13	Доставка по России	<p>Доставка по России транспортной компанией Деловые линии. ПЭК, Байкал-Сервис. Доставка любой другой транспортной компанией 700 руб. Доставка от транспортной компании до пункта назначения оплачивается при получении заказа</p>\r\n	2015-01-14 12:51:20.538691+04	2015-01-14 12:51:20.538726+04	5	
14	Контакты	<p>Адрес: Москва, Большая Черемушкинская, дом 25, строение 97<br />\r\nВремя работы: Пн-Пт с 10.00 до 19.00</p>\r\n	2015-01-14 12:52:44.947315+04	2015-01-14 12:52:44.947352+04	6	
17	О нас	<p>Компания Scortex &mdash; современный и иннновационный подход к разработке и пошиву текстильной промо продукции. Scortex &mdash; это 5 лет успешной работы на рынке промо продукции, за это время компания завоевала признание многих крупных компаний. Постоянными партнерами, которой являтся компания Nvidia, Кира Пластинина студио, Актион Диджитал, VSN-realty.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Направление работы компании Scortex: разработка, пошив тестильной сувенирной продукции высокого качества. Scortex &mdash; это своевремнный качественный сервис: использование совершенного высокотехнологичного оборудования и сервисов в работе.</p>\r\n	2015-01-14 12:56:11.071743+04	2015-01-14 12:56:11.071782+04	7	
18	Документы	<p>Одним из приоритетов Scortex является прозрачность ценовой политики и работы в целом. В этом разделе можно скачать все необходимы документы для работы с компанией</p>\r\n	2015-01-14 12:58:51.764076+04	2015-01-14 12:58:51.764116+04	8	
19	Название документа		2015-01-14 12:59:30.479897+04	2015-01-14 12:58:51.765369+04	8	page-attachements/Bez-imeni-2_tUp8Q4U.png
20	Название документа		2015-01-14 12:59:30.481943+04	2015-01-14 12:58:51.766213+04	8	page-attachements/id1-icon-4_ffDq4UG.png
21	Название документа		2015-01-14 12:59:30.483467+04	2015-01-14 12:58:51.76724+04	8	page-attachements/id1-icon-3.png
4	довольных клиентов	<p>1000000</p>\r\n	2015-01-14 13:35:18.484261+04	2015-01-14 12:45:33.85215+04	2	
15	Хедер	<a class="to-map" href="/kontakty"></a>\r\n<p><a class="tel" href="tel:84993981835">8 (499) 398-18-35</a></p>\r\n<p><a class="mailto" href="mailto:info@scortex.ru">info@scortex.ru</a></p>\r\n	2015-01-19 12:36:17.063841+04	2015-01-14 12:52:44.948293+04	6	
22	Название документа		2015-01-14 12:59:30.485501+04	2015-01-14 12:58:51.768022+04	8	page-attachements/id1-icon-2.png
23	Макеты	<p>Основа работы Scortex &mdash; доступность и предоставление качественного сервиса 24 часа в сутки. В этом разделе вы найдете все необходимые макеты в кривых (в векторе) для самостоятельного создания портотипа вашего изделия.</p>\r\n	2015-01-14 13:00:03.249549+04	2015-01-14 13:00:03.249587+04	9	
24	Макет		2015-01-14 13:01:04.465102+04	2015-01-14 13:01:04.465164+04	9	page-attachements/id1-icon-3_CN7JIIL.png
25	Макет		2015-01-14 13:01:04.466535+04	2015-01-14 13:01:04.466598+04	9	page-attachements/id1-icon-2_wBQF0LE.png
26	Макет		2015-01-14 13:01:04.468295+04	2015-01-14 13:01:04.468353+04	9	page-attachements/id1-icon-4_eCBhCA7.png
27	Макет		2015-01-14 13:01:04.470109+04	2015-01-14 13:01:04.470245+04	9	page-attachements/id1-icon-1.png
28	Нанесение	<p>Нанесение логотипа на одежду или любое текстильное изделие &mdash; это отдельная индустрия в производстве рекламного текстиля, в которой много нюансов и деталей. Самыми распространенными методами нанесения логотипов на текстиль являются: шелкография, термотрансферное нанесение, сублимационное нанесение и вышивка</p>\r\n	2015-01-14 13:02:48.266825+04	2015-01-14 13:02:48.266868+04	10	
29	Шелкография или прямой метод нанесения	<p>Под этим методом подразумевают прямой контакт краски и текстиля, как правило, нанесение производится на изделия из хлопковых и нейлоновых тканей. Шелкография подразделяется на несколько подвидов печати, с применением различного оборудования, выбор которого зависит от характера печати и количества изделий</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<th>&nbsp;</th>\r\n\t\t\t<th>Карусельный станок</th>\r\n\t\t\t<th>Цифровой принтер</th>\r\n\t\t\t<th>Рулонное оборудование</th>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td>Минимальное количество изделий</td>\r\n\t\t\t<td>От 30 штук и более</td>\r\n\t\t\t<td>От 1 до 30 штук</td>\r\n\t\t\t<td>Печать от 10 квадратных метров</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td>Технология нанесения</td>\r\n\t\t\t<td>По изделию проводят<br />\r\n\t\t\tракелем с краской, после<br />\r\n\t\t\tчего краска попадает<br />\r\n\t\t\tна изделие</td>\r\n\t\t\t<td>Изделие находится<br />\r\n\t\t\tв принтере, краска наносится<br />\r\n\t\t\tс помощью печатающей<br />\r\n\t\t\tголовки</td>\r\n\t\t\t<td>Рулон ткани заправляют<br />\r\n\t\t\tв специальный аппарат,<br />\r\n\t\t\tткань и краска подаются<br />\r\n\t\t\tодновременно</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td>Ценообразование</td>\r\n\t\t\t<td>Количество изделий, цветов, сложность нанесения</td>\r\n\t\t\t<td>Количество изделий</td>\r\n\t\t\t<td>Количество квадратных метров печати</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n	2015-01-14 13:02:48.267903+04	2015-01-14 13:02:48.267935+04	10	
30	Термотрансферное нанесение	<p>Метод заключается в использовании заготовки нанесения, которая переносится с использованием высоких температур, как правило, нанесение производится на изделия из хлопковых и нейлоновых тканей. Заготовки, называют термотрансферами, термотрансфера бывают нескольких видов:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<th>&nbsp;</th>\r\n\t\t\t<th>Пластизоль</th>\r\n\t\t\t<th>Флок</th>\r\n\t\t\t<th>Флекс</th>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td>количество цветов печати</td>\r\n\t\t\t<td>До 6 цветов</td>\r\n\t\t\t<td>1 цвет</td>\r\n\t\t\t<td>1 цвет</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td>характеристика</td>\r\n\t\t\t<td>Краска, нанесенная на пленку</td>\r\n\t\t\t<td>Гладкая пленка</td>\r\n\t\t\t<td>Ворсистая пленка, напоминающая замшу</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td>ценообразование</td>\r\n\t\t\t<td>Площадь печати, количество изделий</td>\r\n\t\t\t<td>Площадь вещи</td>\r\n\t\t\t<td>Площадь вещи</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n	2015-01-14 13:02:48.269034+04	2015-01-14 13:02:48.269084+04	10	
31	Сублимационное нанесение	<p>Метод нанесения заключается в пропитке ткани краской, краска становится структурой ткани, нанесение становится не заметным на ощупь, отличительная черта метода &mdash; нанесение исключительно на синтетические ткани</p>\r\n	2015-01-14 14:10:17.421215+04	2015-01-14 14:10:17.421254+04	10	
32	Вышивка	<p>Метод нанесения принципиально отличается от других, так как при вышивке используются полиэфирные шелковые нитки. В зависимости от программы можно варьировать качество и скорость вышивки</p>\r\n	2015-01-14 14:10:17.422306+04	2015-01-14 14:10:17.422352+04	10	
16	Футер	<p><a href="tel:84993981835">8 (499) 398-18-35</a></p>\r\n\r\n<p><a href="mailto:info@scortex.ru">info@scortex.ru</a></p>\r\n\r\n<p>Москва, Б. Черемушкинская, д. 25, с. 97</p>\r\n\r\n<p>Пн-Пт: с 10.00 - 19.00</p>\r\n	2015-01-19 12:19:43.310795+04	2015-01-14 12:52:44.949166+04	6	
33	Скачать прайс-лист		2015-01-19 14:50:14.744458+04	2015-01-19 13:19:07.60473+04	11	page-attachements/whitebark-pine-mount-hood-82994-990x742.jpg
\.


--
-- Name: site_content_text_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sergey
--

SELECT pg_catalog.setval('site_content_text_id_seq', 33, true);


--
-- Data for Name: testimonials_testimonial; Type: TABLE DATA; Schema: public; Owner: sergey
--

COPY testimonials_testimonial (id, author, body, company, created_at, updated_at) FROM stdin;
\.


--
-- Name: testimonials_testimonial_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sergey
--

SELECT pg_catalog.setval('testimonials_testimonial_id_seq', 1, false);


--
-- Data for Name: wares__sex_choices_model; Type: TABLE DATA; Schema: public; Owner: sergey
--

COPY wares__sex_choices_model (id, title, ware_fk_id) FROM stdin;
1	Мужской	1
2	Женский	1
\.


--
-- Name: wares__sex_choices_model_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sergey
--

SELECT pg_catalog.setval('wares__sex_choices_model_id_seq', 2, true);


--
-- Data for Name: wares__size_choices_model; Type: TABLE DATA; Schema: public; Owner: sergey
--

COPY wares__size_choices_model (id, title, ware_fk_id) FROM stdin;
1	S	1
2	M	1
3	L	1
\.


--
-- Name: wares__size_choices_model_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sergey
--

SELECT pg_catalog.setval('wares__size_choices_model_id_seq', 3, true);


--
-- Data for Name: wares__ware_colors_model; Type: TABLE DATA; Schema: public; Owner: sergey
--

COPY wares__ware_colors_model (id, title, hex_code, ware_fkey_id) FROM stdin;
1	синий	013671	1
\.


--
-- Name: wares__ware_colors_model_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sergey
--

SELECT pg_catalog.setval('wares__ware_colors_model_id_seq', 1, true);


--
-- Data for Name: wares__ware_images_model; Type: TABLE DATA; Schema: public; Owner: sergey
--

COPY wares__ware_images_model (id, image, ware_foreign_k_id) FROM stdin;
1	wares/images/cd355ac1f473167c0a9c381867d9b203.jpg	1
2	wares/images/39780354af1df5750be7d9d43245268e.jpg	1
3	wares/images/3d96b870a38648e0cadcb5de2500529e.jpg	1
4	wares/images/bb960b9a64353928c5dc806a7e4ae077.jpg	1
5	wares/images/a5f6be8c33c18513b9102c2cdf2647b9.jpg	1
\.


--
-- Data for Name: wares_category; Type: TABLE DATA; Schema: public; Owner: sergey
--

COPY wares_category (id, title, slug, description, image, is_public, is_on_home_page, description_bottom) FROM stdin;
1	Футболки	futbolki	<p>Omnis sit illum eveniet porro provident culpa delectus. Delectus molestiae earum harum tempore. Voluptatem dolorum qui vel et et non.</p>\r\n	categories/0468d6dac78cb3d7e37962846eac113a.jpg	t	t	<p>Omnis sit illum eveniet porro provident culpa delectus. Delectus molestiae earum harum tempore. Voluptatem dolorum qui vel et et non.&nbsp;Omnis sit illum eveniet porro provident culpa delectus. Delectus molestiae earum harum tempore. Voluptatem dolorum qui vel et et non.</p>\r\n
\.


--
-- Name: wares_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sergey
--

SELECT pg_catalog.setval('wares_category_id_seq', 1, true);


--
-- Data for Name: wares_order; Type: TABLE DATA; Schema: public; Owner: sergey
--

COPY wares_order (id, name, phone, email, order_items, colors, sexes, sizes, order_qty, created_at, updated_at) FROM stdin;
6	1231	+7 (123) 321-31-23	12@12.12	Футболка с коротким рукавом из кулироной глади с круглым воротом				123123	2015-01-14 19:31:28.159232+04	2015-01-14 19:31:28.15927+04
\.


--
-- Name: wares_order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sergey
--

SELECT pg_catalog.setval('wares_order_id_seq', 6, true);


--
-- Data for Name: wares_ware; Type: TABLE DATA; Schema: public; Owner: sergey
--

COPY wares_ware (id, title, slug, consist, image, brand, price, is_public, created_at, updated_at, category_id) FROM stdin;
1	Футболка с коротким рукавом из кулироной глади с круглым воротом	futbolka-s-korotkim-rukavom-iz	100% хлопок	wares/futbolki/6aad043ee5bf938c3027719f3c905c3c.jpg	STAN	1000	t	2015-01-14 18:52:20.063407+04	2015-01-14 18:52:20.063458+04	1
\.


--
-- Name: wares_ware_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sergey
--

SELECT pg_catalog.setval('wares_ware_id_seq', 1, true);


--
-- Name: wares_wareimagesmodel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sergey
--

SELECT pg_catalog.setval('wares_wareimagesmodel_id_seq', 5, true);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_key UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_codename_key; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_key UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_group_id_key; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_key UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_key UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_45f3b1d93ec8c61c_uniq; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_45f3b1d93ec8c61c_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: fabrics_category_pkey; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY fabrics_category
    ADD CONSTRAINT fabrics_category_pkey PRIMARY KEY (id);


--
-- Name: fabrics_category_title_key; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY fabrics_category
    ADD CONSTRAINT fabrics_category_title_key UNIQUE (title);


--
-- Name: fabrics_fabric_pkey; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY fabrics_fabric
    ADD CONSTRAINT fabrics_fabric_pkey PRIMARY KEY (id);


--
-- Name: fabrics_section_pkey; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY fabrics_section
    ADD CONSTRAINT fabrics_section_pkey PRIMARY KEY (id);


--
-- Name: fabrics_section_title_key; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY fabrics_section
    ADD CONSTRAINT fabrics_section_title_key UNIQUE (title);


--
-- Name: site_content_indexslider_pkey; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY site_content_indexslider
    ADD CONSTRAINT site_content_indexslider_pkey PRIMARY KEY (id);


--
-- Name: site_content_page_pkey; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY site_content_page
    ADD CONSTRAINT site_content_page_pkey PRIMARY KEY (id);


--
-- Name: site_content_text_pkey; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY site_content_text
    ADD CONSTRAINT site_content_text_pkey PRIMARY KEY (id);


--
-- Name: testimonials_testimonial_pkey; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY testimonials_testimonial
    ADD CONSTRAINT testimonials_testimonial_pkey PRIMARY KEY (id);


--
-- Name: wares__sex_choices_model_pkey; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY wares__sex_choices_model
    ADD CONSTRAINT wares__sex_choices_model_pkey PRIMARY KEY (id);


--
-- Name: wares__size_choices_model_pkey; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY wares__size_choices_model
    ADD CONSTRAINT wares__size_choices_model_pkey PRIMARY KEY (id);


--
-- Name: wares__ware_colors_model_pkey; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY wares__ware_colors_model
    ADD CONSTRAINT wares__ware_colors_model_pkey PRIMARY KEY (id);


--
-- Name: wares_category_pkey; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY wares_category
    ADD CONSTRAINT wares_category_pkey PRIMARY KEY (id);


--
-- Name: wares_category_title_key; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY wares_category
    ADD CONSTRAINT wares_category_title_key UNIQUE (title);


--
-- Name: wares_order_pkey; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY wares_order
    ADD CONSTRAINT wares_order_pkey PRIMARY KEY (id);


--
-- Name: wares_ware_pkey; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY wares_ware
    ADD CONSTRAINT wares_ware_pkey PRIMARY KEY (id);


--
-- Name: wares_ware_title_45275a575da1d34a_uniq; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY wares_ware
    ADD CONSTRAINT wares_ware_title_45275a575da1d34a_uniq UNIQUE (title, category_id);


--
-- Name: wares_wareimagesmodel_pkey; Type: CONSTRAINT; Schema: public; Owner: sergey; Tablespace: 
--

ALTER TABLE ONLY wares__ware_images_model
    ADD CONSTRAINT wares_wareimagesmodel_pkey PRIMARY KEY (id);


--
-- Name: auth_group_permissions_0e939a4f; Type: INDEX; Schema: public; Owner: sergey; Tablespace: 
--

CREATE INDEX auth_group_permissions_0e939a4f ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_8373b171; Type: INDEX; Schema: public; Owner: sergey; Tablespace: 
--

CREATE INDEX auth_group_permissions_8373b171 ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_417f1b1c; Type: INDEX; Schema: public; Owner: sergey; Tablespace: 
--

CREATE INDEX auth_permission_417f1b1c ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_0e939a4f; Type: INDEX; Schema: public; Owner: sergey; Tablespace: 
--

CREATE INDEX auth_user_groups_0e939a4f ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_e8701ad4; Type: INDEX; Schema: public; Owner: sergey; Tablespace: 
--

CREATE INDEX auth_user_groups_e8701ad4 ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_8373b171; Type: INDEX; Schema: public; Owner: sergey; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_8373b171 ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_e8701ad4; Type: INDEX; Schema: public; Owner: sergey; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_e8701ad4 ON auth_user_user_permissions USING btree (user_id);


--
-- Name: django_admin_log_417f1b1c; Type: INDEX; Schema: public; Owner: sergey; Tablespace: 
--

CREATE INDEX django_admin_log_417f1b1c ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_e8701ad4; Type: INDEX; Schema: public; Owner: sergey; Tablespace: 
--

CREATE INDEX django_admin_log_e8701ad4 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_de54fa62; Type: INDEX; Schema: public; Owner: sergey; Tablespace: 
--

CREATE INDEX django_session_de54fa62 ON django_session USING btree (expire_date);


--
-- Name: fabrics_category_730f6511; Type: INDEX; Schema: public; Owner: sergey; Tablespace: 
--

CREATE INDEX fabrics_category_730f6511 ON fabrics_category USING btree (section_id);


--
-- Name: fabrics_fabric_b583a629; Type: INDEX; Schema: public; Owner: sergey; Tablespace: 
--

CREATE INDEX fabrics_fabric_b583a629 ON fabrics_fabric USING btree (category_id);


--
-- Name: site_content_text_1a63c800; Type: INDEX; Schema: public; Owner: sergey; Tablespace: 
--

CREATE INDEX site_content_text_1a63c800 ON site_content_text USING btree (page_id);


--
-- Name: wares__sex_choices_model_9ee275c1; Type: INDEX; Schema: public; Owner: sergey; Tablespace: 
--

CREATE INDEX wares__sex_choices_model_9ee275c1 ON wares__sex_choices_model USING btree (ware_fk_id);


--
-- Name: wares__size_choices_model_9ee275c1; Type: INDEX; Schema: public; Owner: sergey; Tablespace: 
--

CREATE INDEX wares__size_choices_model_9ee275c1 ON wares__size_choices_model USING btree (ware_fk_id);


--
-- Name: wares__ware_colors_model_9ee275c1; Type: INDEX; Schema: public; Owner: sergey; Tablespace: 
--

CREATE INDEX wares__ware_colors_model_9ee275c1 ON wares__ware_colors_model USING btree (ware_fkey_id);


--
-- Name: wares_ware_b583a629; Type: INDEX; Schema: public; Owner: sergey; Tablespace: 
--

CREATE INDEX wares_ware_b583a629 ON wares_ware USING btree (category_id);


--
-- Name: wares_wareimagesmodel_9ee275c1; Type: INDEX; Schema: public; Owner: sergey; Tablespace: 
--

CREATE INDEX wares_wareimagesmodel_9ee275c1 ON wares__ware_images_model USING btree (ware_foreign_k_id);


--
-- Name: auth_content_type_id_508cf46651277a81_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_content_type_id_508cf46651277a81_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissio_group_id_689710a9a73b7457_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_group_id_689710a9a73b7457_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user__permission_id_384b62483d7071f0_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user__permission_id_384b62483d7071f0_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permiss_user_id_7f0938558328534a_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permiss_user_id_7f0938558328534a_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djan_content_type_id_697914295151027a_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT djan_content_type_id_697914295151027a_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fabrics_categ_section_id_10c9ac43061ada5b_fk_fabrics_section_id; Type: FK CONSTRAINT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY fabrics_category
    ADD CONSTRAINT fabrics_categ_section_id_10c9ac43061ada5b_fk_fabrics_section_id FOREIGN KEY (section_id) REFERENCES fabrics_section(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fabrics_fab_category_id_69355112a521bf8a_fk_fabrics_category_id; Type: FK CONSTRAINT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY fabrics_fabric
    ADD CONSTRAINT fabrics_fab_category_id_69355112a521bf8a_fk_fabrics_category_id FOREIGN KEY (category_id) REFERENCES fabrics_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: site_content_t_page_id_7e3369622ead1995_fk_site_content_page_id; Type: FK CONSTRAINT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY site_content_text
    ADD CONSTRAINT site_content_t_page_id_7e3369622ead1995_fk_site_content_page_id FOREIGN KEY (page_id) REFERENCES site_content_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wares__sex_choices_ware_fk_id_7a33148455869c77_fk_wares_ware_id; Type: FK CONSTRAINT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY wares__sex_choices_model
    ADD CONSTRAINT wares__sex_choices_ware_fk_id_7a33148455869c77_fk_wares_ware_id FOREIGN KEY (ware_fk_id) REFERENCES wares_ware(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wares__size_choice_ware_fk_id_4cda5492ad9e1631_fk_wares_ware_id; Type: FK CONSTRAINT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY wares__size_choices_model
    ADD CONSTRAINT wares__size_choice_ware_fk_id_4cda5492ad9e1631_fk_wares_ware_id FOREIGN KEY (ware_fk_id) REFERENCES wares_ware(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wares__ware_colo_ware_fkey_id_41fa9745391b0b5e_fk_wares_ware_id; Type: FK CONSTRAINT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY wares__ware_colors_model
    ADD CONSTRAINT wares__ware_colo_ware_fkey_id_41fa9745391b0b5e_fk_wares_ware_id FOREIGN KEY (ware_fkey_id) REFERENCES wares_ware(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wares__ware_ware_foreign_k_id_3f852388d6651452_fk_wares_ware_id; Type: FK CONSTRAINT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY wares__ware_images_model
    ADD CONSTRAINT wares__ware_ware_foreign_k_id_3f852388d6651452_fk_wares_ware_id FOREIGN KEY (ware_foreign_k_id) REFERENCES wares_ware(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wares_ware_category_id_30cc4964cf0052b2_fk_wares_category_id; Type: FK CONSTRAINT; Schema: public; Owner: sergey
--

ALTER TABLE ONLY wares_ware
    ADD CONSTRAINT wares_ware_category_id_30cc4964cf0052b2_fk_wares_category_id FOREIGN KEY (category_id) REFERENCES wares_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: public; Type: ACL; Schema: -; Owner: sergey
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM sergey;
GRANT ALL ON SCHEMA public TO sergey;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

