# -*- coding: utf-8 -*-

import json

from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import get_object_or_404

from .models import Section, Fabric
from site_content.views import get_content, get_meta

def index(req):
    sections = Section.objects.all().filter(is_public=True)

    for section in sections:
        section.categories = section.category_set.all().filter(is_public=True).order_by('id')

        for category in section.categories:
            category.fabrics = category.fabric_set.all().filter(is_public=True).order_by('id')
            dir(category.fabrics)

    content = get_content('Ткани')
    meta = get_meta('Ткани')

    return render(req, 'fabrics/index.html', {
        'tkani': True,
        'sections': sections,
        'content': content,
        'meta': meta
    })

def get_description(req, id):
    fabric = get_object_or_404(Fabric, pk=id)
    res_data = {}
    res_data['title'] = fabric.title
    res_data['description'] = fabric.description

    return HttpResponse(json.dumps(res_data), content_type="application/json")

