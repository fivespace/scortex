# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fabrics', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='section',
            name='is_public',
            field=models.BooleanField(default=True, verbose_name=b'\xd0\x9e\xd0\xbf\xd1\x83\xd0\xb1\xd0\xbb\xd0\xb8\xd0\xba\xd0\xbe\xd0\xb2\xd0\xb0\xd0\xbd\xd0\xbe'),
            preserve_default=True,
        ),
    ]
