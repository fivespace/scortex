from django import template

register = template.Library()

@register.filter(name="in")
def inside(value, args):
    if args is None:
        return False
    arg_list = [int(arg) for arg in args.split(',')]
    print arg_list
    return value in arg_list