# -*- coding: utf-8 -*-

from django.db import models

from slugify import slugify

class Section(models.Model):
    title = models.CharField(max_length=100, verbose_name='Раздел', unique=True)
    slug = models.CharField(max_length=200, blank=True, null=True)
    is_public = models.BooleanField(default=True, verbose_name='Опубликовано')

    def __unicode__(self):
        return u'%s' % self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title[:30].lower())
        super(Section, self).save(*args, **kwargs) # Call the "real" save() method.

    class Meta:
        verbose_name='Раздел'
        verbose_name_plural='Разделы'

class Category(models.Model):
    title = models.CharField(max_length=100, verbose_name='Категория', unique=True)
    section = models.ForeignKey(Section, null=True)
    slug = models.CharField(max_length=200, blank=True, null=True)
    is_public = models.BooleanField(default=False, verbose_name='Опубликовано')

    def __unicode__(self):
        return u'%s' % self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title[:30].lower())
        super(Category, self).save(*args, **kwargs) # Call the "real" save() method.

    class Meta:
        verbose_name='Категория'
        verbose_name_plural='Категории'

class Fabric(models.Model):
    title = models.CharField(max_length=200, verbose_name='Название', help_text='Название ткани должно быть уникальным в рамках категории')
    description = models.TextField(verbose_name=u'Описание', blank=True)
    category = models.ForeignKey(Category, verbose_name='Категория')
    slug = models.CharField(max_length=200, blank=True, null=True)
    is_public = models.BooleanField(default=False, verbose_name='Опубликовано')

    def __unicode__(self):
        return u'%s' % self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title[:30].lower())
        super(Fabric, self).save(*args, **kwargs) # Call the "real" save() method.

    class Meta:
        verbose_name='Ткань'
        verbose_name_plural='Ткани'

