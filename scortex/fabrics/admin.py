from django.contrib import admin
from .models import Section, Category, Fabric

@admin.register(Section)
class SectionAdmin(admin.ModelAdmin):
    exclude = ('slug', 'is_public')
    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}

class FabricInline(admin.TabularInline):
    model = Fabric
    extra = 1

    list_display = ('title', 'category',)
    exclude = ('slug',)

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'section',)
    readonly_fields = ('id',)
    exclude = ('slug',)

    inlines = [FabricInline]


