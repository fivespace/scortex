# -*- coding: utf-8 -*-

from django.apps import AppConfig

class FabricsConfig(AppConfig):
    name='fabrics'
    verbose_name=u'Ткани'