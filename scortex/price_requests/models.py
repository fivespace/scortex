# -*- coding: utf-8 -*-

from django.db import models

class PriceRequest(models.Model):
    name = models.CharField(max_length=200, verbose_name=u'Имя')
    phone = models.CharField(max_length=200, verbose_name=u'Телефон', blank=True, null=True)
    email = models.EmailField(max_length=254, verbose_name='Email', blank=True, null=True)
    request_type = models.CharField(max_length=200, verbose_name=u'Тип', blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name=u'Дата обновления')

    def __unicode__(self):
        if self.phone:
            return u"%s, %s" % (self.name, self.phone)
        elif self.email:
            return u"%s, %s" % (self.name, self.email)
        else:
            return u"%s" % (self.name)

    class Meta:
        verbose_name=u'Запрос'
        verbose_name_plural=u'Запросы'
