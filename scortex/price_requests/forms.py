from django import forms
from .models import PriceRequest

class RequestForm(forms.ModelForm):
    class Meta:
        model = PriceRequest
        fields = ['name', 'phone', 'email', 'request_type']
