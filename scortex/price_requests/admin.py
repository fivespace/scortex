from django.contrib import admin
from .models import PriceRequest

@admin.register(PriceRequest)
class PriceRequestAdmin(admin.ModelAdmin):
    search_fields = ('name','phone','email')
    list_display = ['__unicode__', 'created_at']
    readonly_fields = ['request_type', 'created_at',]
    ordering = ('-created_at',)
