import json
import mimetypes

from django.shortcuts import render
from django.http import HttpResponse, HttpResponseBadRequest

from .forms import RequestForm

def price_request(req):
    if req.method == "POST" and req.is_ajax():
        form = RequestForm(req.POST)

        if form.is_valid():
            form.save()


            return HttpResponse('OK')
        else:
            errors = {}
            if form.errors:
                errors = form.errors.as_json()
            return HttpResponseBadRequest(json.dumps(errors), content_type="application/json")
    else:
        return HttpResponseBadRequest()

def file_test(req):
    pass
