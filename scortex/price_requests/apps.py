# -*- coding: utf-8 -*-

from django.apps import AppConfig

class PriceRequestConfig(AppConfig):
    name='price_requests'
    verbose_name=u'Запросы прайса'
