# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('price_requests', '0002_auto_20150726_1724'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pricerequest',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 7, 26, 14, 24, 50, 642104, tzinfo=utc), verbose_name=b'\xd0\x94\xd0\xb0\xd1\x82\xd0\xb0 \xd1\x81\xd0\xbe\xd0\xb7\xd0\xb4\xd0\xb0\xd0\xbd\xd0\xb8\xd1\x8f', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='pricerequest',
            name='updated_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 7, 26, 14, 24, 58, 365614, tzinfo=utc), verbose_name=b'\xd0\x94\xd0\xb0\xd1\x82\xd0\xb0 \xd0\xbe\xd0\xb1\xd0\xbd\xd0\xbe\xd0\xb2\xd0\xbb\xd0\xb5\xd0\xbd\xd0\xb8\xd1\x8f', auto_now=True),
            preserve_default=False,
        ),
    ]
