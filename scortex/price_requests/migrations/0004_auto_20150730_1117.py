# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('price_requests', '0003_auto_20150726_1724'),
    ]

    operations = [
        migrations.AddField(
            model_name='pricerequest',
            name='request_type',
            field=models.CharField(max_length=200, null=True, verbose_name='\u0422\u0438\u043f', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='pricerequest',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='pricerequest',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u043e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u0438\u044f'),
            preserve_default=True,
        ),
    ]
