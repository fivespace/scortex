# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PriceRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name='\u0418\u043c\u044f')),
                ('phone', models.CharField(max_length=200, null=True, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d', blank=True)),
                ('email', models.EmailField(max_length=254, null=True, verbose_name=b'Email', blank=True)),
            ],
            options={
                'verbose_name': '\u0417\u0430\u043f\u0440\u043e\u0441',
                'verbose_name_plural': '\u0417\u0430\u043f\u0440\u043e\u0441\u044b',
            },
            bases=(models.Model,),
        ),
    ]
