from django.conf.urls import patterns, include, url
from django.contrib import admin
from scortex import settings

handler404 = 'site_content.views.custom_404'

urlpatterns = patterns('',
    url(r'^', include('site_content.urls')),
    url(r'^tkani/?$', 'fabrics.views.index'),
    url(r'^catalog/?', include('wares.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/order/?$', 'wares.views.create_order'),
    url(r'^api/price_request/?$', 'price_requests.views.price_request'),
    url(r'^api/test/?$', 'price_requests.views.file_test'),
    url(r'^api/fabric/(?P<id>\d+)/?$', 'fabrics.views.get_description'),
    (r'^ckeditor/', include('ckeditor.urls')),
)

urlpatterns += patterns('',
    (r'^media/(?P<path>[a-zA-Z0-9-_\/\.]+)', 'django.views.static.serve', {
    'document_root': settings.MEDIA_ROOT}))

