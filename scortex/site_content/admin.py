# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import indexSlider, Text, Page

@admin.register(indexSlider)
class indexSliderAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'created_at', 'image_tag')
    readonly_fields = ('created_at', 'image_tag')
    sorting = ['created_at']

class TextInline(admin.TabularInline):
    exclude = ('created_at',)
    readonly_fields = ('updated_at',)
    model = Text

    # def has_delete_permission(self, request, obj=None):
    #     return False

@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    list_display = ('__unicode__',)
    exclude = ('slug',)
    inlines = [TextInline,]

    def get_formsets_with_inlines(self, request, obj=None):
        for inline in self.get_inline_instances(request, obj):
            if isinstance(inline, TextInline) and not hasattr(obj, 'alias'):
                inline.exclude = ['attachement']
                inline.extra = 0
                inline.max_num = 0
            elif isinstance(inline, TextInline) and not obj.alias == u'Документы' and not obj.alias == u'Макеты' and not obj.alias == u'Каталог':
                inline.exclude = ['attachement']
                inline.extra = 0
                inline.max_num = 0

            yield inline.get_formset(request, obj), inline
