from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^$', 'site_content.views.index'),
    url(r'^dostavka/?$', 'site_content.views.dostavka'),
    url(r'^oplata/?$', 'site_content.views.oplata'),
    url(r'^nanesenie/?$', 'site_content.views.nanesenie'),
    url(r'^kontakty/?$', 'site_content.views.kontakty'),
    url(r'^o_kompanii/?$', 'site_content.views.o_kompanii'),
    url(r'^dokumenti/?$', 'site_content.views.dokumenti'),
    url(r'^maketi/?$', 'site_content.views.maketi'),
)