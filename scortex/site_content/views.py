# -*- coding: utf-8 -*-

import itertools

from django.shortcuts import render
from django.db.models import Q

from site_content.models import indexSlider, Page
from testimonials.models import Testimonial
from wares.models import Category

def get_content(alias = None):
    if alias:
        page = Page.objects.filter(Q(alias=alias) | Q(alias='Контакты'))
        page_content = [p.text_set.all() for p in page]
        page_content_flatten = itertools.chain(*page_content)
        page_content_list = list(page_content_flatten)

        ids = [c.id for c in page_content_list]
        return dict(zip(ids, page_content_list))
    else:
        page = Page.objects.get(alias='Контакты')
        page_content = page.text_set.all()
        ids = [c.id for c in page_content]
        return dict(zip(ids, page_content))

def get_meta(alias):
    page = Page.objects.get(alias=alias)
    meta = {}
    meta['title'] = page.html_title
    meta['description'] = page.html_description

    return meta

def index(req):
    slider = indexSlider.objects.all().filter(is_public=True)[:15]
    testimonials = Testimonial.objects.all().order_by('-created_at')[:15]
    categories = Category.objects.all().filter(is_public=True, is_on_home_page=True)
    content = get_content('Домашняя')
    meta = get_meta(alias='Домашняя')

    return render(req, 'site_content/index.html', {
        'slider': slider,
        'categories': categories,
        'testimonials': testimonials,
        'content': content,
        'meta': meta
    })

def dostavka(req):
    content = get_content('Доставка')
    meta = get_meta(alias='Доставка')

    return render(req, 'site_content/dostavka.html', {
        'dostavka': True,
        'content': content,
        'meta': meta
    })

def oplata(req):
    content = get_content('Оплата')
    meta = get_meta(alias='Оплата')

    return render(req, 'site_content/oplata.html', {
        'oplata': True,
        'content': content,
        'meta': meta
    })

def nanesenie(req):
    content = get_content('Нанесение')
    meta = get_meta(alias='Нанесение')

    return render(req, 'site_content/nanesenie.html', {
        'nanesenie': True,
        'content': content,
        'meta': meta
    })

def kontakty(req):
    content = get_content()
    meta = get_meta(alias='Контакты')

    return render(req, 'site_content/kontakty.html', {
        'kontakty': True,
        'content': content,
        'meta': meta
    })

def o_kompanii(req):
    content = get_content('О компании')
    meta = get_meta(alias='О компании')

    return render(req, 'site_content/o_kompanii.html', {
        'o_kompanii': True,
        'content': content,
        'meta': meta
    })

def dokumenti(req):
    content = get_content('Документы')
    content_convert_to_list = []
    meta = get_meta(alias='Документы')

    for key, value in content.iteritems():
        content_convert_to_list.append(value)

    return render(req, 'site_content/dokumenti.html', {
        'dokumenti': True,
        'content': content,
        'content_list': content_convert_to_list,
        'meta': meta,
        'page_name': ','.join([u'Документы', u'Документ'])
    })

def maketi(req):
    content = get_content('Макеты')
    meta = get_meta(alias='Макеты')
    content_convert_to_list = []

    for key, value in content.iteritems():
        content_convert_to_list.append(value)

    filtered = [el for el in content_convert_to_list if el.attachement]

    return render(req, 'site_content/maketi.html', {
        'maketi': True,
        'content': content,
        'content_list': filtered,
        'meta': meta,
        'page_name': ','.join([u'Макеты', u'Макет'])
    })

def custom_404(req):
    return render(req, 'site_content/404.html')
