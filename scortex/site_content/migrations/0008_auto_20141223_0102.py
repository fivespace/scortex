# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('site_content', '0007_text_page'),
    ]

    operations = [
        migrations.AlterField(
            model_name='text',
            name='page',
            field=models.ForeignKey(blank=True, to='site_content.Page', null=True),
            preserve_default=True,
        ),
    ]
