# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('site_content', '0008_auto_20141223_0102'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='page',
            name='title',
        ),
        migrations.AddField(
            model_name='page',
            name='slug',
            field=models.CharField(default=' ', max_length=200, blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='page',
            name='alias',
            field=models.CharField(max_length=200, verbose_name=b'\xd0\x9d\xd0\xb0\xd0\xb7\xd0\xb2\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5', blank=True),
            preserve_default=True,
        ),
    ]
