# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import site_content.models


class Migration(migrations.Migration):

    dependencies = [
        ('site_content', '0010_auto_20141224_2216'),
    ]

    operations = [
        migrations.AddField(
            model_name='text',
            name='attachement',
            field=models.FileField(null=True, upload_to=site_content.models.pageAttachementUpload, blank=True),
            preserve_default=True,
        ),
    ]
