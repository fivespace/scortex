# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('site_content', '0013_auto_20150305_0002'),
    ]

    operations = [
        migrations.AlterField(
            model_name='page',
            name='html_description',
            field=models.CharField(default=b'Scortex', help_text=b'250 \xd1\x81\xd0\xb8\xd0\xbc\xd0\xb2\xd0\xbe\xd0\xbb\xd0\xbe\xd0\xb2 \xd0\xbc\xd0\xb0\xd0\xba\xd1\x81.', max_length=250, verbose_name=b'Description', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='page',
            name='html_title',
            field=models.CharField(default=b'Scortex', help_text=b'250 \xd1\x81\xd0\xb8\xd0\xbc\xd0\xb2\xd0\xbe\xd0\xbb\xd0\xbe\xd0\xb2 \xd0\xbc\xd0\xb0\xd0\xba\xd1\x81.', max_length=250, verbose_name=b'Title', blank=True),
            preserve_default=True,
        ),
    ]
