# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('site_content', '0004_auto_20141223_0044'),
    ]

    operations = [
        migrations.AlterField(
            model_name='text',
            name='header',
            field=models.CharField(max_length=200, verbose_name=b'\xd0\x97\xd0\xb0\xd0\xb3\xd0\xbe\xd0\xbb\xd0\xbe\xd0\xb2\xd0\xbe\xd0\xba', blank=True),
            preserve_default=True,
        ),
    ]
