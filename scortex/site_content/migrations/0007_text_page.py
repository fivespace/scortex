# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('site_content', '0006_page'),
    ]

    operations = [
        migrations.AddField(
            model_name='text',
            name='page',
            field=models.ForeignKey(to='site_content.Page'),
            preserve_default=False,
        ),
    ]
