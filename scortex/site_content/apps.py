# -*- coding: utf-8 -*-

from django.apps import AppConfig

class SiteContentConfig(AppConfig):
    name='site_content'
    verbose_name=u'Контент'