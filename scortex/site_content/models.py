# -*- coding: utf-8 -*-
import os

from wares.models import file_hashify

from django.db import models
from PIL import Image
from ckeditor.fields import RichTextField
from slugify import slugify

def slider_upload(instance, filename):
    filename = file_hashify(filename)
    return '/'.join(['index-slider', filename])

def pageAttachementUpload(instance, filename):
    filename = '%s%s' % (slugify(os.path.splitext(filename)[0]), os.path.splitext(filename)[1])
    return '/'.join(['page-attachements', filename])

class indexSlider(models.Model):
    image = models.ImageField(upload_to=slider_upload, verbose_name=u'Слайд')
    is_public = models.BooleanField(default=False, verbose_name='Опубликовано')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата загрузки')

    def __unicode__(self):
        return u'Слайд №%s' % self.id

    def image_tag(self):
        return '<img src="%s_thumbnail%s" />' % (os.path.splitext(self.image.url)[0], os.path.splitext(self.image.url)[1])

    image_tag.short_description = 'Слайд'
    image_tag.allow_tags = True

    class Meta:
        verbose_name='Слайд'
        verbose_name_plural='Слайды'

    def save(self):
        if not self.id and not self.image:
            return

        super(indexSlider, self).save()

        max_size = (740, 470)

        image = Image.open(self.image)
        (width, height) = image.size

        aspect_ratio = width / float(height)
        new_height = int( max_size[0] / aspect_ratio )

        if new_height < 470:
            final_height = new_height
            final_width = max_size[0]
        else:
            final_height = max_size[1]
            final_width = int(aspect_ratio * max_size[1])

        new_size = ( final_width, final_height )
        image = image.resize(new_size, Image.ANTIALIAS)
        thumbnail = image.copy()
        image.save(self.image.path)

        thumbnail.thumbnail((100, 100), Image.ANTIALIAS)
        thumbnail.save('%s_thumbnail%s' % (os.path.splitext(self.image.path)[0], os.path.splitext(self.image.path)[1]))

class Page(models.Model):
    alias = models.CharField(max_length=200, verbose_name='Название')
    html_title = models.CharField(max_length=250, verbose_name='Title', blank=True, default="Scortex", help_text="250 символов макс.")
    html_description = models.CharField(max_length=250, verbose_name='Description', blank=True, default="Scortex", help_text="250 символов макс.")
    slug = models.CharField(max_length=200, blank=True)

    def __unicode__(self):
        return self.alias

    def save(self, *args, **kwargs):
        self.slug = slugify(self.alias[:30].lower())
        if not self.html_title:
            self.html_title = self.alias[:250]
        super(Page, self).save(*args, **kwargs) # Call the "real" save() method.

    class Meta:
        verbose_name='Страница'
        verbose_name_plural='Страницы'

class Text(models.Model):
    header = models.CharField(max_length=200, verbose_name='Заголовок', blank=True)
    content = RichTextField(verbose_name='Текст', blank=True)
    page = models.ForeignKey(Page, blank=True, null=True)
    attachement = models.FileField(upload_to=pageAttachementUpload, blank=True, null=True)
    created_at = models.DateTimeField(auto_now=True, verbose_name='Дата создания')
    updated_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата изменения')

    def __unicode__(self):
        return ''

    def get_happies(self):
        return list(self.content[3:-6])

    def get_data_wo_p_tag(self):
        return self.content[3:-6]

    class Meta:
        verbose_name='Тестовый блок'
        verbose_name_plural='Текстовые блоки'
