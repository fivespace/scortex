require('jquery');
require('magnific');
require('unslider');
require('maskedinput');

function getCookie(name) {
    var cookieValue = null;
    if(document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for(var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);

            // Does this cookie string begin with the name we want?
            if(cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }

    return cookieValue;
}

function mapInit() {
    var myMap = new ymaps.Map ('kontakty-map', {
        center: [55.675279, 37.584512],
        zoom: 16,
        controls: ['zoomControl']
    });

    myMap.controls.add('zoomControl', { left: 5, top: 5 });
    myMap.behaviors.disable('scrollZoom');

    var myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
        hintContent: 'Scortex. Б. Черемушкинская, д. 25, с. 97'
    }, {
        iconLayout: 'default#image',
        iconImageHref: '/public/img/map-placemark.png',
        iconImageSize: [84, 112],
        iconImageOffset: [-42, -112],
        balloonShadow: false
    });

    myMap.geoObjects.add(myPlacemark);
}

$(function() {
    $('.input__tel').mask('+7 (999) 999-99-99');

    $('.welcome-slider-wrapper').unslider({
        dots: true,
        delay: 5000,
        fluid: false
    });

    $('.testimonials-slider-wrapper').unslider({
        arrows: true,
        delay: 5000,
        fluid: true
    });

    if($('.btn-order').length) {
        $('.btn-order').magnificPopup({
            type: 'inline',
            midClick: true
        });
    }

    if($('.btn-list').length) {

        $('.btn-list').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            var $this = $(this);
            var $priceListForm = $('#list-order_form');

            $.magnificPopup.open({
                items: {
                    src: $this.attr('href')
                },
                type: 'inline'
            });

            $priceListForm.data('redirect-to', $this.data('redirect-to'));
        });
    }

    if($('.catalogue-detail-images').length) {
        var bigs = $('.catalogue-detail-bigs').children();
        var thumbs = $('.catalogue-detail-thumb');

        thumbs.on('click', function(e) {
            e.stopPropagation();
            var $this = $(this);
            var index = thumbs.index($this);
            var big = $(bigs[index]);

            if(!$this.hasClass('active')) {
                big.siblings().removeClass('is_shown');
                big.addClass('is_shown');
                $this.siblings().removeClass('active');
                $this.addClass('active');
            }
        });
    }

    if($('#modal_order').length) {

        var $container = $('#modal_order');
        var $form = $('#modal-order_form');
        var $success = $container.find('[data-form-success]');

        var _onFormSuccess = function() {
            $success.addClass('is-shown');
        };

        $form.submit(function(e) {
            e.preventDefault();

            var csrftoken = getCookie('csrftoken');
            var $this = $(this);

            if($('.catalogue-detail-props').length) {
                var sexesInputs = $('input[name=sexes_choices]:checked');
                var sexesTemp = [];
                var colorsInputs = $('input[name=colors_choices]:checked');
                var colorsTemp = [];
                var sexesForm = $this.find('input[name=sexes]');
                var colorsForm = $this.find('input[name=colors]');

                sexesInputs.each(function() {
                    sexesTemp.push($(this).val());
                });

                colorsInputs.each(function() {
                    colorsTemp.push($(this).val());
                });

                sexesForm.val(sexesTemp.length ? sexesTemp.join(', ') : 'н/а');
                colorsForm.val(colorsTemp.length ? colorsTemp.join(', ') : 'н/а');
                $('input[name=order_items]').val($('.catalogue-detail-props__title').html());
            }

            $this = $(this);

            var action = $this.attr('action');
            var method = $this.attr('method');
            var data = $this.serialize();
            var btn = $this.find('button[type=submit]');

            $.ajax({
                url: action,
                type: method,
                data: data,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('X-CSRFToken', csrftoken);
                    var errors = $('.form-error-text');
                    var inputs = $('.form-error');

                    errors.each(function(i, el) {
                        $(el).remove();
                    });

                    inputs.each(function(i, el) {
                        $(el).removeClass('form-error');
                    });

                    btn.prop('disabled', true).html('Отправляется...');
                },

                success: function() {
                    // console.log(res);
                    btn.html('Отправлено');

                    _onFormSuccess();
                },

                error: function(err) {
                    console.log(err);

                    if(err && err.responseText) {
                        var resText = err.responseText;
                        var resTextAsJson = JSON.parse(resText);
                        var jsonErr = JSON.parse(resTextAsJson);

                        var $input = '';

                        for(var prop in jsonErr) {
                            $input = $('input[name=' + prop + ']');
                            $input.addClass('form-error');

                            jsonErr[prop].forEach(function(el) { // jshint ignore:line
                                $input.after('<div class="form-error-text">' + el.message + '</div>'); // jshint ignore:line
                            }); // jshint ignore:line
                        }
                    }

                    btn.html('Ошибка');

                    setTimeout(function() {
                        btn.html('Оставить заявку').prop('disabled', false);
                    }, 3000);

                }
            });

        });
    }

    if($('#list_order').length) {

        var $containerList = $('#list_order');
        var $formList = $('#list-order_form');
        var $requestTypeInput = $formList.find('[name=request_type]');
        var $successList = $containerList.find('[data-form-success]');

        function _getPageName() {
            var $pageName = $('[data-page-name]');

            if ($pageName.length) {
                return $pageName.data('page-name').split(',')[1];
            } else {
                return 'Прайс-лист';
            }
        }

        function _composeTexts() {
            var $header = $containerList.find('header');
            var $btn = $formList.find('[type=submit]');

            var headerPrefix = 'Получить';
            var btnPrefix = 'Скачать';
            var appendix = (_getPageName()).toLowerCase();

            $header.text(headerPrefix + ' ' + appendix);
            $btn.text(btnPrefix + ' ' + appendix);
        }

        function _fillRequestType() {
            var pageName = (_getPageName()).toLowerCase();
            $requestTypeInput.val(pageName);
        }

        _composeTexts();
        _fillRequestType();

        var _onFormSuccessList = function() {
            $successList.addClass('is-shown');
            window.location.replace($formList.data('redirect-to'));
        };

        $formList.submit(function(e) {
            e.preventDefault();

            var csrftoken = getCookie('csrftoken');
            var $this = $(this);

            var action = $this.attr('action');
            var method = $this.attr('method');
            var data = $this.serialize();
            var btn = $this.find('button[type=submit]');

            $.ajax({
                url: action,
                type: method,
                data: data,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('X-CSRFToken', csrftoken);
                    var errors = $('.form-error-text');
                    var inputs = $('.form-error');

                    errors.each(function(i, el) {
                        $(el).remove();
                    });

                    inputs.each(function(i, el) {
                        $(el).removeClass('form-error');
                    });

                    btn.prop('disabled', true).html('Отправляется...');
                },

                success: function() {
                    // console.log(res);
                    btn.html('Отправлено');

                    _onFormSuccessList();
                },

                error: function(err) {
                    console.log(err);

                    if(err && err.responseText) {
                        var resText = err.responseText;
                        var resTextAsJson = JSON.parse(resText);
                        var jsonErr = JSON.parse(resTextAsJson);

                        var $input = '';

                        for(var prop in jsonErr) {
                            $input = $('input[name=' + prop + ']');
                            $input.addClass('form-error');

                            jsonErr[prop].forEach(function(el) { // jshint ignore:line
                                $input.after('<div class="form-error-text">' + el.message + '</div>'); // jshint ignore:line
                            }); // jshint ignore:line
                        }
                    }

                    btn.html('Ошибка');

                    setTimeout(function() {
                        btn.html('Скачать прайс-лист').prop('disabled', false);
                    }, 2000);

                }
            });

        });
    }

    if($('#kontakty-map').length) {
        ymaps.ready(mapInit);
    }

    if($('.tkani-category').length) {
        var links = $('.fabric-details-link');

        links.on('click', function(e) {
            e.preventDefault();
            var $this = $(this);
            var href = $this.attr('href');
            var modalId = $this.attr('data-modal');
            var modal = $(modalId);
            var modalTitle = modal.find(modalId + '-title');
            var modalDesc = modal.find(modalId + '-description');

            $.ajax({
                url: href,
                success: function(res) {
                    modalTitle.html(res.title);
                    if(res.description) {
                        modalDesc.html(res.description);
                    } else {
                        modalDesc.html('Описание для данной ткани еще отсутствует');
                    }

                    $.magnificPopup.open({
                        items: {
                            src: modalId,
                            type: 'inline'
                        }
                    });
                }
            });

        });
    }
});
