# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wares', '0015_auto_20150122_2246'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='order_qty',
            field=models.CharField(default=b'1', max_length=200, null=True, verbose_name=b'\xd0\x9a\xd0\xbe\xd0\xbb\xd0\xb8\xd1\x87\xd0\xb5\xd1\x81\xd1\x82\xd0\xb2\xd0\xbe', blank=True),
            preserve_default=True,
        ),
    ]
