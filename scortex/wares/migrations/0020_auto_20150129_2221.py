# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wares', '0019_auto_20150129_2210'),
    ]

    operations = [
        migrations.AlterField(
            model_name='_ware_colors_model',
            name='hex_code',
            field=models.CharField(help_text=b'\xd0\x92\xd0\xba\xd0\xbb\xd1\x8e\xd1\x87\xd0\xb0\xd1\x8f #', max_length=7, verbose_name=b'HEX \xd0\xba\xd0\xbe\xd0\xb4 \xd1\x86\xd0\xb2\xd0\xb5\xd1\x82\xd0\xb0'),
            preserve_default=True,
        ),
    ]
