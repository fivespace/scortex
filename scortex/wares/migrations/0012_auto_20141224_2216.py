# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import wares.models


class Migration(migrations.Migration):

    dependencies = [
        ('wares', '0011_auto_20141224_0302'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='_sex_choices_model',
            options={'verbose_name': '\u041f\u043e\u043b', 'verbose_name_plural': '\u0421\u043f\u0438\u0441\u043e\u043a \u043c\u043e\u0434\u0435\u043b\u0435\u0439'},
        ),
        migrations.AlterModelOptions(
            name='_size_choices_model',
            options={'verbose_name': '\u0420\u0430\u0437\u043c\u0435\u0440', 'verbose_name_plural': '\u0414\u043e\u0441\u0442\u0443\u043f\u043d\u044b\u0435 \u0440\u0430\u0437\u043c\u0435\u0440\u044b'},
        ),
        migrations.AlterModelOptions(
            name='_ware_colors_model',
            options={'verbose_name': '\u0426\u0432\u0435\u0442', 'verbose_name_plural': '\u0414\u043e\u0441\u0442\u0443\u043f\u043d\u044b\u0435 \u0446\u0432\u0435\u0442\u0430'},
        ),
        migrations.AlterModelOptions(
            name='_ware_images_model',
            options={'verbose_name': '\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', 'verbose_name_plural': '\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f'},
        ),
        migrations.RemoveField(
            model_name='ware',
            name='_has_thumb',
        ),
        migrations.AlterField(
            model_name='_sex_choices_model',
            name='title',
            field=models.CharField(max_length=7, verbose_name=b'\xd0\x9f\xd0\xbe\xd0\xbb', choices=[('\u041c\u0443\u0436\u0441\u043a\u043e\u0439', '\u041c\u0443\u0436\u0441\u043a\u043e\u0439'), ('\u0416\u0435\u043d\u0441\u043a\u0438\u0439', '\u0416\u0435\u043d\u0441\u043a\u0438\u0439'), ('\u0423\u043d\u0438\u0441\u0435\u043a\u0441', '\u0423\u043d\u0438\u0441\u0435\u043a\u0441')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='_size_choices_model',
            name='title',
            field=models.CharField(max_length=3, verbose_name=b'\xd0\xa0\xd0\xb0\xd0\xb7\xd0\xbc\xd0\xb5\xd1\x80', choices=[(b'XS', b'XS'), (b'S', b'S'), (b'M', b'M'), (b'L', b'L'), (b'XL', b'XL'), (b'XXL', b'XXL')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='_ware_images_model',
            name='image',
            field=models.ImageField(help_text=b'560x570', upload_to=wares.models.ware_images_upload, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f', blank=True),
            preserve_default=True,
        ),
    ]
