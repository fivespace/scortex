# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wares', '0003_auto_20141221_1930'),
    ]

    operations = [
        migrations.AddField(
            model_name='ware',
            name='_has_thumb',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
