# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wares', '0008_auto_20141222_1937'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='is_on_home_page',
            field=models.BooleanField(default=False, verbose_name=b'\xd0\x9d\xd0\xb0 \xd0\xb3\xd0\xbb\xd0\xb0\xd0\xb2\xd0\xbd\xd0\xbe\xd0\xb9'),
            preserve_default=True,
        ),
    ]
