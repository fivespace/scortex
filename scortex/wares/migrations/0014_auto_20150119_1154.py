# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('wares', '0013_auto_20150113_0008'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='description_bottom',
            field=ckeditor.fields.RichTextField(null=True, verbose_name=b'\xd0\x9e\xd0\xbf\xd0\xb8\xd1\x81\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5 (\xd0\xbd\xd0\xb8\xd0\xb7)', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='_ware_colors_model',
            name='hex_code',
            field=models.CharField(help_text=b'\xd0\x91\xd0\xb5\xd0\xb7 #', max_length=6, verbose_name=b'HEX \xd0\xba\xd0\xbe\xd0\xb4 \xd1\x86\xd0\xb2\xd0\xb5\xd1\x82\xd0\xb0'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='description',
            field=ckeditor.fields.RichTextField(null=True, verbose_name=b'\xd0\x9e\xd0\xbf\xd0\xb8\xd1\x81\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5 (\xd0\xb2\xd0\xb5\xd1\x80\xd1\x85)', blank=True),
            preserve_default=True,
        ),
    ]
