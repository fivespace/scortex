# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wares', '0004_ware__has_thumb'),
    ]

    operations = [
        migrations.RenameField(
            model_name='_ware_colors_model',
            old_name='ware_fk',
            new_name='ware_fkey',
        ),
    ]
