# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wares', '0023_auto_20150304_2327'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='html_h1',
            field=models.CharField(help_text=b'h1 \xd1\x82\xd1\x8d\xd0\xb3 \xd0\xbd\xd0\xb0 \xd1\x81\xd1\x82\xd1\x80\xd0\xb0\xd0\xbd\xd0\xb8\xd1\x86\xd0\xb5', max_length=250, null=True, verbose_name=b'H1', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ware',
            name='html_h1',
            field=models.CharField(help_text=b'h1 \xd1\x82\xd1\x8d\xd0\xb3 \xd0\xbd\xd0\xb0 \xd1\x81\xd1\x82\xd1\x80\xd0\xb0\xd0\xbd\xd0\xb8\xd1\x86\xd0\xb5', max_length=250, null=True, verbose_name=b'H1', blank=True),
            preserve_default=True,
        ),
    ]
