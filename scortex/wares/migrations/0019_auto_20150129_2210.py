# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wares', '0018_auto_20150129_2139'),
    ]

    operations = [
        migrations.AddField(
            model_name='ware',
            name='fabric',
            field=models.CharField(max_length=10, null=True, verbose_name=b'\xd0\xa2\xd0\xba\xd0\xb0\xd0\xbd\xd1\x8c', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ware',
            name='order',
            field=models.IntegerField(null=True, verbose_name=b'\xd0\x9f\xd0\xbe\xd1\x80\xd1\x8f\xd0\xb4\xd0\xba\xd0\xbe\xd0\xb2\xd1\x8b\xd0\xb9 \xd0\xbd\xd0\xbe\xd0\xbc\xd0\xb5\xd1\x80', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='_size_choices_model',
            name='title',
            field=models.CharField(max_length=3, verbose_name=b'\xd0\xa0\xd0\xb0\xd0\xb7\xd0\xbc\xd0\xb5\xd1\x80', choices=[(b'1', b'XS'), (b'2', b'S'), (b'3', b'M'), (b'4', b'L'), (b'5', b'XL'), (b'6', b'XXL')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='ware',
            name='brand',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name=b'\xd0\x91\xd1\x80\xd0\xb5\xd0\xbd\xd0\xb4', choices=[(b'STAN', b'STAN'), (b'Trisar', b'Trisar'), (b'Scortex', b'Scortex')]),
            preserve_default=True,
        ),
    ]
