# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wares', '0017_auto_20150125_1531'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='_size_choices_model',
            options={'ordering': ['title'], 'verbose_name': '\u0420\u0430\u0437\u043c\u0435\u0440', 'verbose_name_plural': '\u0414\u043e\u0441\u0442\u0443\u043f\u043d\u044b\u0435 \u0440\u0430\u0437\u043c\u0435\u0440\u044b'},
        ),
    ]
