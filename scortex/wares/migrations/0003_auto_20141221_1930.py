# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wares', '0002_auto_20141221_1926'),
    ]

    operations = [
        migrations.RenameField(
            model_name='ware',
            old_name='cover',
            new_name='image',
        ),
        migrations.RemoveField(
            model_name='ware',
            name='_has_thumb',
        ),
    ]
