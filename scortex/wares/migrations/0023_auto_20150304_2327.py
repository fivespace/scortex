# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wares', '0022_auto_20150224_2112'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='html_description',
            field=models.CharField(default=b'Scortex', help_text=b'150 \xd1\x81\xd0\xb8\xd0\xbc\xd0\xb2\xd0\xbe\xd0\xbb\xd0\xbe\xd0\xb2 \xd0\xbc\xd0\xb0\xd0\xba\xd1\x81.', max_length=150, verbose_name=b'Description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='category',
            name='html_title',
            field=models.CharField(default=b'Scortex', help_text=b'70 \xd1\x81\xd0\xb8\xd0\xbc\xd0\xb2\xd0\xbe\xd0\xbb\xd0\xbe\xd0\xb2 \xd0\xbc\xd0\xb0\xd0\xba\xd1\x81.', max_length=70, verbose_name=b'Title', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ware',
            name='html_description',
            field=models.CharField(default=b'Scortex', help_text=b'150 \xd1\x81\xd0\xb8\xd0\xbc\xd0\xb2\xd0\xbe\xd0\xbb\xd0\xbe\xd0\xb2 \xd0\xbc\xd0\xb0\xd0\xba\xd1\x81.', max_length=150, verbose_name=b'Description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ware',
            name='html_title',
            field=models.CharField(default=b'Scortex', help_text=b'70 \xd1\x81\xd0\xb8\xd0\xbc\xd0\xb2\xd0\xbe\xd0\xbb\xd0\xbe\xd0\xb2 \xd0\xbc\xd0\xb0\xd0\xba\xd1\x81.', max_length=70, verbose_name=b'Title', blank=True),
            preserve_default=True,
        ),
    ]
