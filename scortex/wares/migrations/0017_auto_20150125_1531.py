# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wares', '0016_auto_20150122_2251'),
    ]

    operations = [
        migrations.AddField(
            model_name='ware',
            name='application',
            field=models.CharField(max_length=200, null=True, verbose_name=b'\xd0\x9d\xd0\xb0\xd0\xbd\xd0\xb5\xd1\x81\xd0\xb5\xd0\xbd\xd0\xb8\xd0\xb5', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ware',
            name='extra_info',
            field=models.TextField(null=True, verbose_name=b'\xd0\x94\xd0\xbe\xd0\xbf\xd0\xbe\xd0\xbb\xd0\xbd\xd0\xb8\xd1\x82\xd0\xb5\xd0\xbb\xd1\x8c\xd0\xbd\xd0\xb0\xd1\x8f \xd0\xb8\xd0\xbd\xd1\x84\xd0\xbe\xd1\x80\xd0\xbc\xd0\xb0\xd1\x86\xd0\xb8\xd1\x8f', blank=True),
            preserve_default=True,
        ),
    ]
