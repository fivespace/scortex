# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('wares', '0014_auto_20150119_1154'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='order_qty',
            field=models.IntegerField(default=1, null=True, verbose_name=b'\xd0\x9a\xd0\xbe\xd0\xbb\xd0\xb8\xd1\x87\xd0\xb5\xd1\x81\xd1\x82\xd0\xb2\xd0\xbe', blank=True, validators=[django.core.validators.MaxValueValidator(999999), django.core.validators.MinValueValidator(1)]),
            preserve_default=True,
        ),
    ]
