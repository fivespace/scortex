# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wares', '0006_remove_ware_moderator'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='WareImagesModel',
            new_name='_ware_images_model',
        ),
        migrations.RenameField(
            model_name='_ware_images_model',
            old_name='ware_fk',
            new_name='ware_foreign_k',
        ),
    ]
