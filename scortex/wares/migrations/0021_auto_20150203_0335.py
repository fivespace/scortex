# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wares', '0020_auto_20150129_2221'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ware',
            name='fabric',
            field=models.CharField(max_length=200, null=True, verbose_name=b'\xd0\xa2\xd0\xba\xd0\xb0\xd0\xbd\xd1\x8c', blank=True),
            preserve_default=True,
        ),
    ]
