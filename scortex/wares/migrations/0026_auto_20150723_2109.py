# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wares', '0025_auto_20150723_0216'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'ordering': ('list_order',), 'verbose_name': '\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f', 'verbose_name_plural': '\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438'},
        ),
        migrations.AddField(
            model_name='category',
            name='list_order',
            field=models.PositiveIntegerField(default=0, verbose_name=b'\xd0\x9f\xd0\xbe\xd1\x80\xd1\x8f\xd0\xb4\xd0\xbe\xd0\xba'),
            preserve_default=True,
        ),
    ]
