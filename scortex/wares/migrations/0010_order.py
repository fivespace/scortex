# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('wares', '0009_category_is_on_home_page'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('phone', models.CharField(max_length=12)),
                ('email', models.EmailField(max_length=254, blank=True)),
                ('order_items', models.CharField(max_length=300, null=True, blank=True)),
                ('colors', models.CharField(max_length=300, null=True, blank=True)),
                ('sexes', models.CharField(max_length=300, null=True, blank=True)),
                ('sizes', models.CharField(max_length=300, null=True, blank=True)),
                ('order_qty', models.IntegerField(default=1, validators=[django.core.validators.MaxValueValidator(999999), django.core.validators.MinValueValidator(1)])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
