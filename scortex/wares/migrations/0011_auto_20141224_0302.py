# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('wares', '0010_order'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='order',
            options={'verbose_name': '\u0417\u0430\u043a\u0430\u0437', 'verbose_name_plural': '\u0417\u0430\u043a\u0430\u0437\u044b'},
        ),
        migrations.AddField(
            model_name='order',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 24, 0, 2, 2, 249454, tzinfo=utc), verbose_name=b'\xd0\x94\xd0\xb0\xd1\x82\xd0\xb0 \xd1\x81\xd0\xbe\xd0\xb7\xd0\xb4\xd0\xb0\xd0\xbd\xd0\xb8\xd1\x8f', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='order',
            name='updated_at',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 24, 0, 2, 20, 651186, tzinfo=utc), verbose_name=b'\xd0\x94\xd0\xb0\xd1\x82\xd0\xb0 \xd0\xbe\xd0\xb1\xd0\xbd\xd0\xbe\xd0\xb2\xd0\xbb\xd0\xb5\xd0\xbd\xd0\xb8\xd1\x8f', auto_now=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='order',
            name='colors',
            field=models.CharField(max_length=300, null=True, verbose_name=b'\xd0\xa6\xd0\xb2\xd0\xb5\xd1\x82', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='order',
            name='email',
            field=models.EmailField(max_length=254, verbose_name=b'Email', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='order',
            name='name',
            field=models.CharField(max_length=200, verbose_name=b'\xd0\x98\xd0\xbc\xd1\x8f'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='order',
            name='order_items',
            field=models.CharField(max_length=300, null=True, verbose_name=b'\xd0\xa7\xd1\x82\xd0\xbe', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='order',
            name='order_qty',
            field=models.IntegerField(default=1, verbose_name=b'\xd0\x9a\xd0\xbe\xd0\xbb\xd0\xb8\xd1\x87\xd0\xb5\xd1\x81\xd1\x82\xd0\xb2\xd0\xbe', validators=[django.core.validators.MaxValueValidator(999999), django.core.validators.MinValueValidator(1)]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='order',
            name='phone',
            field=models.CharField(max_length=17, verbose_name=b'\xd0\xa2\xd0\xb5\xd0\xbb\xd0\xb5\xd1\x84\xd0\xbe\xd0\xbd'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='order',
            name='sexes',
            field=models.CharField(max_length=300, null=True, verbose_name=b'\xd0\x9f\xd0\xbe\xd0\xbb', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='order',
            name='sizes',
            field=models.CharField(max_length=300, null=True, verbose_name=b'\xd0\xa0\xd0\xb0\xd0\xb7\xd0\xbc\xd0\xb5\xd1\x80', blank=True),
            preserve_default=True,
        ),
    ]
