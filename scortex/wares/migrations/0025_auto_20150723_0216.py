# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wares', '0024_auto_20150309_2033'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='description',
            field=models.TextField(null=True, verbose_name=b'\xd0\x9e\xd0\xbf\xd0\xb8\xd1\x81\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='html_description',
            field=models.CharField(default=b'Scortex', help_text=b'250 \xd1\x81\xd0\xb8\xd0\xbc\xd0\xb2\xd0\xbe\xd0\xbb\xd0\xbe\xd0\xb2 \xd0\xbc\xd0\xb0\xd0\xba\xd1\x81.', max_length=250, verbose_name=b'Description', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='html_title',
            field=models.CharField(default=b'Scortex', help_text=b'250 \xd1\x81\xd0\xb8\xd0\xbc\xd0\xb2\xd0\xbe\xd0\xbb\xd0\xbe\xd0\xb2 \xd0\xbc\xd0\xb0\xd0\xba\xd1\x81.', max_length=250, verbose_name=b'Title', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='ware',
            name='html_description',
            field=models.CharField(default=b'Scortex', help_text=b'250 \xd1\x81\xd0\xb8\xd0\xbc\xd0\xb2\xd0\xbe\xd0\xbb\xd0\xbe\xd0\xb2 \xd0\xbc\xd0\xb0\xd0\xba\xd1\x81.', max_length=250, verbose_name=b'Description', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='ware',
            name='html_title',
            field=models.CharField(default=b'Scortex', help_text=b'250 \xd1\x81\xd0\xb8\xd0\xbc\xd0\xb2\xd0\xbe\xd0\xbb\xd0\xbe\xd0\xb2 \xd0\xbc\xd0\xb0\xd0\xba\xd1\x81.', max_length=250, verbose_name=b'Title', blank=True),
            preserve_default=True,
        ),
    ]
