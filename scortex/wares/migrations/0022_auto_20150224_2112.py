# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('wares', '0021_auto_20150203_0335'),
    ]

    operations = [
        migrations.CreateModel(
            name='OrderNotify',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(max_length=254, verbose_name=b'Email')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name=b'\xd0\x94\xd0\xb0\xd1\x82\xd0\xb0 \xd1\x81\xd0\xbe\xd0\xb7\xd0\xb4\xd0\xb0\xd0\xbd\xd0\xb8\xd1\x8f')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name=b'\xd0\x94\xd0\xb0\xd1\x82\xd0\xb0 \xd0\xbe\xd0\xb1\xd0\xbd\xd0\xbe\xd0\xb2\xd0\xbb\xd0\xb5\xd0\xbd\xd0\xb8\xd1\x8f')),
            ],
            options={
                'verbose_name': '\u041e\u043f\u043e\u0432\u0435\u0449\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u041e\u043f\u043e\u0432\u0435\u0449\u0435\u043d\u0438\u044f',
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='_size_choices_model',
            name='title',
            field=models.CharField(max_length=3, verbose_name='\u0420\u0430\u0437\u043c\u0435\u0440', choices=[(b'1', b'XS'), (b'2', b'S'), (b'3', b'M'), (b'4', b'L'), (b'5', b'XL'), (b'6', b'XXL')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='description',
            field=ckeditor.fields.RichTextField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 (\u0432\u0435\u0440\u0445)', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='description_bottom',
            field=ckeditor.fields.RichTextField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 (\u043d\u0438\u0437)', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='is_on_home_page',
            field=models.BooleanField(default=False, verbose_name='\u041d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='is_public',
            field=models.BooleanField(default=False, verbose_name='\u041e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u043d\u043e'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='title',
            field=models.CharField(unique=True, max_length=100, verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='ware',
            name='application',
            field=models.CharField(max_length=200, null=True, verbose_name='\u041d\u0430\u043d\u0435\u0441\u0435\u043d\u0438\u0435', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='ware',
            name='brand',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='\u0411\u0440\u0435\u043d\u0434', choices=[(b'STAN', b'STAN'), (b'Trisar', b'Trisar'), (b'Scortex', b'Scortex')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='ware',
            name='category',
            field=models.ForeignKey(verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f', to='wares.Category'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='ware',
            name='consist',
            field=models.TextField(null=True, verbose_name='\u0421\u043e\u0441\u0442\u0430\u0432', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='ware',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='ware',
            name='extra_info',
            field=models.TextField(null=True, verbose_name='\u0414\u043e\u043f\u043e\u043b\u043d\u0438\u0442\u0435\u043b\u044c\u043d\u0430\u044f \u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='ware',
            name='fabric',
            field=models.CharField(max_length=200, null=True, verbose_name='\u0422\u043a\u0430\u043d\u044c', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='ware',
            name='is_public',
            field=models.BooleanField(default=False, verbose_name='\u041e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u043d\u043e'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='ware',
            name='order',
            field=models.IntegerField(null=True, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043a\u043e\u0432\u044b\u0439 \u043d\u043e\u043c\u0435\u0440', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='ware',
            name='price',
            field=models.IntegerField(null=True, verbose_name='\u0426\u0435\u043d\u0430', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='ware',
            name='title',
            field=models.CharField(help_text='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u0430 \u0434\u043e\u043b\u0436\u043d\u043e \u0431\u044b\u0442\u044c \u0443\u043d\u0438\u043a\u0430\u043b\u044c\u043d\u044b\u043c \u0432 \u0440\u0430\u043c\u043a\u0430\u0445 \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438', max_length=200, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='ware',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u043e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u0438\u044f'),
            preserve_default=True,
        ),
    ]
