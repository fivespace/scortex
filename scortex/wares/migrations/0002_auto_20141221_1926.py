# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wares', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='_sex_choices_model',
            name='title',
            field=models.CharField(max_length=7, choices=[('\u041c\u0443\u0436\u0441\u043a\u043e\u0439', '\u041c\u0443\u0436\u0441\u043a\u043e\u0439'), ('\u0416\u0435\u043d\u0441\u043a\u0438\u0439', '\u0416\u0435\u043d\u0441\u043a\u0438\u0439'), ('\u0423\u043d\u0438\u0441\u0435\u043a\u0441', '\u0423\u043d\u0438\u0441\u0435\u043a\u0441')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='_size_choices_model',
            name='title',
            field=models.CharField(max_length=3, choices=[(b'XS', b'XS'), (b'S', b'S'), (b'M', b'M'), (b'L', b'L'), (b'XL', b'XL'), (b'XXL', b'XXL')]),
            preserve_default=True,
        ),
    ]
