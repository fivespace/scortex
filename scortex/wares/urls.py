from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^/?(?P<req_category>[a-zA-Z0-9-_]+)?/?$', 'wares.views.list_view'),
    url(r'^/?(?P<req_category>[a-zA-Z0-9-_]+)/(?P<req_ware>[a-zA-Z0-9-_]+)/?$', 'wares.views.detail_view'),
)