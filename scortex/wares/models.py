# -*- coding: utf-8 -*-

import hashlib
import datetime
import random
import os

from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

from slugify import slugify
from PIL import Image
from ckeditor.fields import RichTextField

def file_hashify(name):
    file_ext = name.split('.')[1]
    name = slugify(name)
    sequence = (name, str(datetime.datetime.now()), str(random.random()))
    random_string = '-'.join(sequence)
    filename_hash = hashlib.md5(random_string).hexdigest()
    filename = '%s.%s' % (filename_hash, file_ext)
    return filename

def category_upload(instance, filename):
    filename = file_hashify(filename)
    return '/'.join(['categories', filename])

def ware_upload(instance, filename):
    filename = file_hashify(filename)
    return '/'.join(['wares', instance.category.slug, filename])

def ware_images_upload(instance, filename):
    filename = file_hashify(filename)
    return '/'.join(['wares', 'images', filename])

class Category(models.Model):
    title = models.CharField(max_length=100, verbose_name=u'Категория', unique=True)
    html_title = models.CharField(max_length=250, verbose_name='Title', blank=True, default="Scortex", help_text="250 символов макс.")
    html_description = models.CharField(max_length=250, verbose_name='Description', blank=True, default="Scortex", help_text="250 символов макс.")
    html_h1 = models.CharField(max_length=250, verbose_name='H1', blank=True, null=True, help_text="h1 тэг на странице")
    slug = models.CharField(max_length=200, blank=True, null=True)
    description = RichTextField(blank=True, null=True, verbose_name=u'Описание (верх)')
    description_bottom = RichTextField(blank=True, null=True, verbose_name=u'Описание (низ)')
    image = models.ImageField(upload_to=category_upload, verbose_name=u'Изображение', null=True)
    is_on_home_page = models.BooleanField(default=False, verbose_name=u'На главной')
    is_public = models.BooleanField(default=False, verbose_name=u'Опубликовано')
    list_order = models.PositiveIntegerField(default=0, blank=False, null=False, verbose_name='Порядок')

    def image_tag(self):
        return u'<img src="%s_thumbnail%s" />' % (os.path.splitext(self.image.url)[0], os.path.splitext(self.image.url)[1])

    image_tag.short_description = 'Превью'
    image_tag.allow_tags = True

    def __unicode__(self):
        return u'%s' % self.title

    def get_absolute_url(self):
        return u'/catalog/%s/' % self.slug

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title[:60].lower())

        if not self.html_title:
            self.html_title = self.title[:250]

        is_file_exists = os.path.exists(self.image.path)

        super(Category, self).save(*args, **kwargs) # Call the "real" save() method.

        if self.image and not is_file_exists:

            max_size = (370, 370)

            image = Image.open(self.image)
            (width, height) = image.size

            aspect_ratio = width / float(height)
            new_height = int( max_size[0] / aspect_ratio )

            if new_height < 370:
                final_height = new_height
                final_width = max_size[0]
            else:
                final_height = max_size[1]
                final_width = int(aspect_ratio * max_size[1])

            new_size = ( final_width, final_height )
            image = image.resize(new_size, Image.ANTIALIAS)
            image.save(self.image.path, quality=80)

            thumbnail = image.copy()
            thumbnail.thumbnail((100,100), Image.ANTIALIAS)
            thumbnail.save('%s_thumbnail%s' % (os.path.splitext(self.image.path)[0], os.path.splitext(self.image.path)[1]))

    class Meta:
        verbose_name=u'Категория'
        verbose_name_plural=u'Категории'
        ordering= ('list_order',)

class Ware(models.Model):
    BRAND_CHOICES = (
        ('STAN', 'STAN'),
        ('Trisar', 'Trisar'),
        ('Scortex', 'Scortex'),
    )

    title = models.CharField(max_length=200, verbose_name=u'Название', help_text=u'Название материала должно быть уникальным в рамках категории')
    slug = models.CharField(max_length=200, blank=True, null=True)
    html_title = models.CharField(max_length=250, verbose_name='Title', blank=True, default="Scortex", help_text="250 символов макс.")
    html_description = models.CharField(max_length=250, verbose_name='Description', blank=True, default="Scortex", help_text="250 символов макс.")
    html_h1 = models.CharField(max_length=250, verbose_name='H1', blank=True, null=True, help_text="h1 тэг на странице")
    category = models.ForeignKey(Category, verbose_name=u'Категория')
    order = models.IntegerField(blank=True, null=True, verbose_name=u'Порядковый номер')
    consist = models.TextField(blank=True, verbose_name=u'Состав', null=True)
    fabric = models.CharField(max_length=200, blank=True, null=True, verbose_name=u'Ткань')
    application = models.CharField(max_length=200, verbose_name=u'Нанесение', blank=True, null=True)
    extra_info = models.TextField(verbose_name=u'Дополнительная информация', blank=True, null=True)
    image = models.ImageField(upload_to=ware_upload, verbose_name=u'Изображение обложки', blank=True)
    brand = models.CharField(max_length=200, choices=BRAND_CHOICES, blank=True, null=True, verbose_name=u'Бренд')
    price = models.IntegerField(blank=True, null=True, verbose_name=u'Цена')
    is_public = models.BooleanField(default=False, verbose_name=u'Опубликовано')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name=u'Дата обновления')

    def image_tag(self):
        if self.image:
            return u'<img src="%s_thumbnail%s" />' % (os.path.splitext(self.image.url)[0], os.path.splitext(self.image.url)[1])

    image_tag.short_description = u'Превью'
    image_tag.allow_tags = True

    def __unicode__(self):
        return u'%s' % self.title

    def get_absolute_url(self):
        return u'/catalog/%s/%s' % (self.category.slug, self.slug)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title[:30].lower())
        self.html_title = self.title[:250]
        super(Ware, self).save(*args, **kwargs) # Call the "real" save() method.

        if self.image:
            max_size = (300, 290)
            image = Image.open(self.image)

            if image.mode not in ('L', 'RGB'):
                image = image.convert('RGB')

            image.thumbnail(max_size, Image.ANTIALIAS)
            image.save(self.image.path, quality=80)
            thumbnail = image.copy()
            thumbnail.thumbnail((100,100), Image.ANTIALIAS)
            thumbnail.save(u'%s_thumbnail%s' % (os.path.splitext(self.image.path)[0], os.path.splitext(self.image.path)[1]))

    class Meta:
        verbose_name=u'Материал'
        verbose_name_plural=u'Материалы'
        unique_together = ('title', 'category')

class _ware_images_model(models.Model):
    image = models.ImageField(upload_to=ware_images_upload, verbose_name=u'Изображения', help_text='560x570', blank=True)
    ware_foreign_k = models.ForeignKey(Ware)

    def __unicode__(self):
        return u'Изображение'

    def image_tag(self):
        if self.image:
            return u'<img src="%s_thumbnail%s" />' % (os.path.splitext(self.image.url)[0], os.path.splitext(self.image.url)[1])

    image_tag.short_description = u'Превью'
    image_tag.allow_tags = True

    class Meta:
        verbose_name=u'Изображение'
        verbose_name_plural=u'Изображения'

    def get_thumb_url(self):
        if not self.image:
            return
        return u'%s_thumbnail%s' % (os.path.splitext(self.image.url)[0], os.path.splitext(self.image.url)[1])

    def save(self, *args, **kwargs):
        super(_ware_images_model, self).save(*args, **kwargs) # Call the "real" save() method.

        if self.image:
            max_size = (560, 570)
            max_size_thumb = (125, 130)

            image = Image.open(self.image)
            thumbnail = image.copy()

            if thumbnail.mode not in ('L', 'RGB'):
                image = image.convert('RGB')
            if thumbnail.mode not in ('L', 'RGB'):
                thumbnail = thumbnail.convert('RGB')

            image.thumbnail(max_size, Image.ANTIALIAS)
            image.save(self.image.path, quality=80)
            thumbnail.thumbnail(max_size_thumb, Image.ANTIALIAS)
            thumbnail.save(u'%s_thumbnail%s' % (os.path.splitext(self.image.path)[0], os.path.splitext(self.image.path)[1]))


class _size_choices_model(models.Model):
    SIZE_CHOICES = (
        ('1', 'XS'),
        ('2', 'S'),
        ('3', 'M'),
        ('4', 'L'),
        ('5', 'XL'),
        ('6', 'XXL'),
    )
    title = models.CharField(max_length=3, choices=SIZE_CHOICES, verbose_name=u'Размер')
    ware_fk = models.ForeignKey(Ware)

    def __unicode__(self):
        return u'%s' % self.title

    class Meta:
        verbose_name=u'Размер'
        verbose_name_plural=u'Доступные размеры'
        ordering=['title']

class _sex_choices_model(models.Model):
    SEX_CHOICES = (
        (u'Мужской', u'Мужской'),
        (u'Женский', u'Женский'),
        (u'Унисекс', u'Унисекс'),
    )
    title = models.CharField(max_length=7, choices=SEX_CHOICES, verbose_name='Пол')
    ware_fk = models.ForeignKey(Ware)

    def __unicode__(self):
        return u'%s' % self.title

    class Meta:
        verbose_name='Пол'
        verbose_name_plural='Список моделей'

class _ware_colors_model(models.Model):
    title = models.CharField(max_length=20, verbose_name='Название')
    hex_code = models.CharField(max_length=7, verbose_name='HEX код цвета', help_text='Включая #')
    ware_fkey = models.ForeignKey(Ware)

    def __unicode__(self):
        return u'Цвет'

    def color_tag(self):
        return '<svg height="30" width="30"><circle cx="15" cy="15" r="15" fill="%s" /></svg>' % self.hex_code

    color_tag.short_description = 'Цвет'
    color_tag.allow_tags = True

    class Meta:
        verbose_name='Цвет'
        verbose_name_plural='Доступные цвета'

class OrderNotify(models.Model):
    email = models.EmailField(max_length=254, verbose_name='Email')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Дата обновления')

    def __unicode__(self):
        return u'%s' % self.email

    class Meta:
        verbose_name='Оповещение'
        verbose_name_plural='Оповещения'

class Order(models.Model):
    name = models.CharField(max_length=200, verbose_name='Имя')
    phone = models.CharField(max_length=18, verbose_name='Телефон')
    email = models.EmailField(max_length=254, blank=True, verbose_name='Email')
    order_items = models.CharField(max_length=300, blank=True, null=True, verbose_name='Что')
    colors = models.CharField(max_length=300, blank=True, null=True, verbose_name='Цвет')
    sexes = models.CharField(max_length=300, blank=True, null=True, verbose_name='Пол')
    sizes = models.CharField(max_length=300, blank=True, null=True, verbose_name='Размер')
    order_qty = models.CharField(max_length=200, default='1', verbose_name='Количество', blank=True, null=True)
    description = models.TextField(blank=True, null=True, verbose_name='Описание')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Дата обновления')

    def __unicode__(self):
        return u'%s' % self.name

    class Meta:
        verbose_name='Заказ'
        verbose_name_plural='Заказы'
