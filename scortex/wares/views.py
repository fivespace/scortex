# -*- coding: utf-8 -*-
import json

from django.shortcuts import render, get_list_or_404
from django.http import HttpResponse, HttpResponseBadRequest
from django.core.mail import EmailMultiAlternatives

from .models import Category, Ware, OrderNotify
from .forms import OrderForm
from site_content.views import get_content
from site_content.models import Page

def _send_email(content):

    notify_to = [item.email for item in OrderNotify.objects.all()]

    if len(notify_to) == 0:
        return

    msg = EmailMultiAlternatives(
        subject = u'Новый заказ',
        body = u'Содержание заказа',
        from_email = 'Scortex <good_robot@scortex.ru>',
        to = notify_to
    )

    msg.attach_alternative(content, 'text/html')
    msg.tags = ['new order']
    msg.send()

    return msg.mandrill_response[0]

def get_meta(alias):
    page = Page.objects.get(alias=alias)
    meta = {}
    meta['title'] = page.html_title
    meta['description'] = page.html_description

    return meta

def list_view(req, req_category):
    categories = get_list_or_404(Category, is_public=True)

    if not req_category:
        category = categories[0]
    else:
        category = Category.objects.get(slug=req_category)

    category.wares = category.ware_set.all().filter(is_public=True).order_by('order')

    content = get_content('Каталог')
    meta = get_meta('Каталог')

    breadcrumbs = [];
    breadcrumbs.append(u'Каталог');
    breadcrumbs.append(category);

    if not category.html_title == 'Scortex':
        meta['title'] = '%s > %s' % (category.html_title, meta['title'])
    else:
        meta['title'] = '%s > %s' % (category.title, meta['title'])

    return render(req, 'wares/list.html', {
        'categories': categories,
        'category': category,
        'catalog': True,
        'content': content,
        'meta': meta,
        'breadcrumbs': breadcrumbs
    })

def detail_view(req, req_category, req_ware):
    categories = Category.objects.all().filter(is_public=True)
    ware = Ware.objects.get(category__slug=req_category, slug=req_ware)

    ware.sexes_choices = ware._sex_choices_model_set.all()
    ware.sizes_choices = ware._size_choices_model_set.all()
    ware.colors_choices = ware._ware_colors_model_set.all()
    ware.images = ware._ware_images_model_set.all()

    content = get_content()
    meta = {}

    breadcrumbs = [];
    breadcrumbs.append(u'Каталог');
    breadcrumbs.append(ware.category);
    breadcrumbs.append(ware);

    if not ware.html_title == 'Scortex':
        meta['title'] = '%s > %s' % (ware.html_title, ware.category.title)
    else:
        meta['title'] = '%s > %s' % (ware.title, ware.category.title)

    meta['description'] = ware.html_description

    return render(req, 'wares/detail.html', {
        'categories': categories,
        'ware': ware,
        'catalog': True,
        'content': content,
        'meta': meta,
        'breadcrumbs': breadcrumbs
    })

def create_order(req):
    if req.method == "POST" and req.is_ajax():
        form = OrderForm(req.POST)

        if form.is_valid():

            post = dict(req.POST.iterlists())

            for key, val in post.iteritems():
                if len(val[0]) == 0:
                    post[key][0] = u'н/а'

            message_content = u'<h2>Данные заказа</h2><ul><li> <strong> Имя: </strong> %s </li> <li> <strong> Телефон: </strong> %s </li> <li> <strong> Email: </strong> %s </li> <li> <strong>Описание: </strong> %s </li> </ul>' % (post['name'][0], post['phone'][0], post['email'][0], post['description'][0])

            print _send_email(message_content)

            form.save()
            return HttpResponse('OK')
        else:
            errors = {}
            if form.errors:
                errors = form.errors.as_json()
            return HttpResponseBadRequest(json.dumps(errors), content_type="application/json")
    else:
        return HttpResponseBadRequest()
