# -*- coding: utf-8 -*-

from django.contrib import admin
from wares.models import Category, Ware, Order, _sex_choices_model, _size_choices_model, _ware_colors_model, _ware_images_model, OrderNotify
from adminsortable2.admin import SortableAdminMixin, SortableInlineAdminMixin

@admin.register(Category)
class CategoryAdmin(SortableAdminMixin, admin.ModelAdmin):
    search_fields = ('title',)
    list_display = ['__unicode__', 'image_tag', 'is_public', 'is_on_home_page']
    readonly_fields = ('image_tag',)
    exclude = ['slug',]
    search_fields = ('title',)
    list_filter = ('is_public', 'is_on_home_page')
    ordering = ('list_order',)

class SexChoicesInline(admin.TabularInline):
    model = _sex_choices_model
    extra = 1

class SizeChoicesInline(admin.TabularInline):
    model = _size_choices_model
    extra = 1

class ColorChoicesAdmin(admin.TabularInline):
    model = _ware_colors_model
    extra = 1
    readonly_fields = ('color_tag',)
    list_display = ('title', 'hex_code', 'color_tag',)

class WareImagesModelInline(admin.TabularInline):
    model = _ware_images_model
    extra = 1
    readonly_fields = ('image_tag',)
    list_display = ('image', 'image_tag',)

@admin.register(Ware)
class WareAdmin(admin.ModelAdmin):

    inlines = [
        SexChoicesInline,
        SizeChoicesInline,
        ColorChoicesAdmin,
        WareImagesModelInline,
    ]

    search_fields = ('title',)
    list_filter = ('category__title',)
    list_display = ('__unicode__', 'category', 'image_tag', 'is_public', 'created_at',)
    readonly_fields = ('created_at', 'updated_at', 'slug', 'image_tag',)
    exclude = ('slug',)
    save_as = True

    fieldsets = (
        ('Главное', {
            'fields': ('title', 'category', 'brand', 'fabric', 'consist', 'application', 'price', 'image', 'extra_info', 'html_h1', 'is_public')
        }),
        ('Прочее', {
            'classes': ['collapse'],
            'fields': ('created_at', 'updated_at')
        })
    )

class SexesListFiler(admin.SimpleListFilter):
    title = 'Пол'
    parameter_name = 'sexes'

    def lookups(self, request, model_admin):
        return (
            (u'мужской', u'мужской'),
            (u'женский', u'женский'),
            (u'унисекс', u'унисекс'),
        )

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(sexes__icontains=self.value())
        else:
            return  queryset

class SizesListFilter(admin.SimpleListFilter):
    title = 'Размер'
    parameter_name = 'sizes'

    def lookups(self, request, model_admin):
        return (
            (u'XS', u'XS'),
            (u'S', u'S'),
            (u'M', u'M'),
            (u'L', u'L'),
            (u'XL', u'XL'),
            (u'XXL', u'XXL'),
        )

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(sizes__icontains=self.value())
        else:
            return  queryset

@admin.register(OrderNotify)
class OrderNotifyAdmin(admin.ModelAdmin):
    list_display = ('__unicode__',)
    readonly_fields = ('created_at', 'updated_at',)

@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'email', 'phone', 'created_at')
    readonly_fields = ('created_at', 'updated_at',)
    search_fields = ('name', 'email', 'phone')
    list_filter = ('created_at', SexesListFiler, SizesListFilter)
    ordering = ['-created_at']

    fieldsets = (
        ('Персональные данные', {
            'fields': ('name', 'phone', 'email')
        }),
        ('Заказ', {
            'fields': ('order_items', 'colors', 'sexes', 'sizes', 'order_qty', 'created_at', 'updated_at')
        })
    )
