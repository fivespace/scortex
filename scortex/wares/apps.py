# -*- coding: utf-8 -*-

from django.apps import AppConfig

class WaresConfig(AppConfig):
    name='wares'
    verbose_name=u'Каталог'