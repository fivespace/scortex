/* Notes:
   - gulp/tasks/browserify.js handles js recompiling with watchify
   - gulp/tasks/browserSync.js watches and reloads compiled files
*/

var gulp  = require('gulp');
var config = require('../config');
var livereload = require('gulp-livereload');

gulp.task('watch', ['setWatch', 'browserify'], function() {
    livereload.listen();
    gulp.watch(config.scss.src, ['scss']);
    gulp.watch(config.images.src, ['images']);
    gulp.watch('./templates/**/*.html').on('change', livereload.changed);
});
