var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    prefix = require('gulp-autoprefixer'),
    handleErrors = require('../util/handleErrors'),
    config = require('../config').scss,
    header = require('gulp-header'),
    livereload = require('gulp-livereload'),
    banner = '/* =============== Made by 5_ | 2014 (fivespace.ru) =============== */';

gulp.task('scss', ['images'], function () {
  return gulp.src(config.src)
    .pipe(sass())
    .on('error', handleErrors)
    .pipe(prefix('last 2 version'))
    .pipe(header(banner))
    .pipe(gulp.dest(config.dest))
    .pipe(livereload({ auto: false }));
});
