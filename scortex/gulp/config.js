var dest = './assets';
var src = './source';

module.exports = {
  scss: {
    src: src + '/scss/**/*.scss',
    dest: dest + '/css'
  },
  images: {
    src: src + '/img/**/**',
    dest: dest + '/img'
  },
  dist: {
    root: dest
  },
  browserify: {
    // Enable source maps
    debug: true,
    // Additional file extentions to make optional
    extensions: ['.js'],
    // A separate bundle will be generated for each
    // bundle config in the list below
    bundleConfigs: [{
      entries: src + '/js/app.js',
      dest: dest + '/js',
      outputName: 'app.js'
    }]
  }
};
