PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE "django_migrations" ("id" serial NOT NULL PRIMARY KEY, "app" text NOT NULL, "name" text NOT NULL, "applied" timestamp NOT NULL);
INSERT INTO "django_migrations" VALUES(1,'contenttypes','0001_initial','2014-12-21 16:10:50.069497');
INSERT INTO "django_migrations" VALUES(2,'auth','0001_initial','2014-12-21 16:10:50.172790');
INSERT INTO "django_migrations" VALUES(3,'admin','0001_initial','2014-12-21 16:10:50.259086');
INSERT INTO "django_migrations" VALUES(4,'sessions','0001_initial','2014-12-21 16:10:50.305234');
INSERT INTO "django_migrations" VALUES(5,'fabrics','0001_initial','2014-12-21 16:20:32.512003');
INSERT INTO "django_migrations" VALUES(6,'wares','0001_initial','2014-12-21 16:20:32.534375');
INSERT INTO "django_migrations" VALUES(7,'orders','0001_initial','2014-12-21 16:20:32.562491');
INSERT INTO "django_migrations" VALUES(8,'site_content','0001_initial','2014-12-21 16:20:32.566228');
INSERT INTO "django_migrations" VALUES(9,'testimonials','0001_initial','2014-12-21 16:20:32.570244');
INSERT INTO "django_migrations" VALUES(10,'wares','0002_auto_20141221_1926','2014-12-21 16:27:06.740425');
INSERT INTO "django_migrations" VALUES(11,'wares','0003_auto_20141221_1930','2014-12-21 16:31:16.485540');
INSERT INTO "django_migrations" VALUES(12,'wares','0004_ware__has_thumb','2014-12-21 16:31:16.572536');
INSERT INTO "django_migrations" VALUES(13,'wares','0005_auto_20141221_1942','2014-12-21 16:42:35.356625');
INSERT INTO "django_migrations" VALUES(14,'wares','0006_remove_ware_moderator','2014-12-21 16:43:59.002257');
INSERT INTO "django_migrations" VALUES(15,'wares','0007_auto_20141221_2032','2014-12-21 17:33:00.173678');
INSERT INTO "django_migrations" VALUES(16,'site_content','0002_indexslider_is_public','2014-12-22 16:00:16.120062');
INSERT INTO "django_migrations" VALUES(17,'testimonials','0002_auto_20141222_1931','2014-12-22 16:31:44.860685');
INSERT INTO "django_migrations" VALUES(18,'wares','0008_auto_20141222_1937','2014-12-22 16:37:23.329667');
INSERT INTO "django_migrations" VALUES(19,'site_content','0003_texts','2014-12-22 21:39:41.540865');
INSERT INTO "django_migrations" VALUES(20,'site_content','0004_auto_20141223_0044','2014-12-22 21:44:19.642360');
INSERT INTO "django_migrations" VALUES(21,'site_content','0005_auto_20141223_0045','2014-12-22 21:45:02.828172');
INSERT INTO "django_migrations" VALUES(22,'site_content','0006_page','2014-12-22 22:00:08.339289');
INSERT INTO "django_migrations" VALUES(23,'site_content','0007_text_page','2014-12-22 22:02:40.720998');
INSERT INTO "django_migrations" VALUES(24,'site_content','0008_auto_20141223_0102','2014-12-22 22:02:40.749628');
INSERT INTO "django_migrations" VALUES(25,'site_content','0009_auto_20141223_0105','2014-12-22 22:05:22.445666');
INSERT INTO "django_migrations" VALUES(26,'orders','0002_auto_20141223_1659','2014-12-23 13:59:54.485806');
INSERT INTO "django_migrations" VALUES(27,'orders','0003_auto_20141223_1708','2014-12-23 14:08:42.445162');
INSERT INTO "django_migrations" VALUES(28,'orders','0004_remove_order_order_items','2014-12-23 15:14:52.032803');
INSERT INTO "django_migrations" VALUES(29,'orders','0005_order_order_items','2014-12-23 15:15:06.323224');
INSERT INTO "django_migrations" VALUES(30,'orders','0006_auto_20141223_1815','2014-12-23 15:15:52.772164');
INSERT INTO "django_migrations" VALUES(31,'wares','0009_category_is_on_home_page','2014-12-23 22:40:54.815854');
INSERT INTO "django_migrations" VALUES(32,'wares','0010_order','2014-12-23 23:25:01.442443');
INSERT INTO "django_migrations" VALUES(33,'wares','0011_auto_20141224_0302','2014-12-24 00:02:31.026729');
INSERT INTO "django_migrations" VALUES(34,'site_content','0010_auto_20141224_2216','2014-12-24 19:16:34.132516');
INSERT INTO "django_migrations" VALUES(35,'wares','0012_auto_20141224_2216','2014-12-24 19:16:34.464301');
INSERT INTO "django_migrations" VALUES(36,'fabrics','0002_auto_20150112_1941','2015-01-12 16:42:06.905051');
INSERT INTO "django_migrations" VALUES(37,'site_content','0011_text_attachement','2015-01-12 16:42:06.949207');
INSERT INTO "django_migrations" VALUES(38,'wares','0013_auto_20150113_0008','2015-01-12 21:08:33.790821');
CREATE TABLE "wares_ware_colors_choices" (
    "id" serial NOT NULL PRIMARY KEY,
    "ware_id" integer NOT NULL,
    "_ware_colors_model_id" integer NOT NULL REFERENCES "wares__ware_colors_model" ("id"),
    UNIQUE ("ware_id", "_ware_colors_model_id")
);
CREATE TABLE "wares_ware_sexes_choices" (
    "id" serial NOT NULL PRIMARY KEY,
    "ware_id" integer NOT NULL,
    "_sex_choices_model_id" integer NOT NULL REFERENCES "wares__sex_choices_model" ("id"),
    UNIQUE ("ware_id", "_sex_choices_model_id")
);
CREATE TABLE "wares_ware_sizes_choices" (
    "id" serial NOT NULL PRIMARY KEY,
    "ware_id" integer NOT NULL,
    "_size_choices_model_id" integer NOT NULL REFERENCES "wares__size_choices_model" ("id"),
    UNIQUE ("ware_id", "_size_choices_model_id")
);
CREATE TABLE "fabrics_section" (
    "id" serial NOT NULL PRIMARY KEY,
    "title" text NOT NULL UNIQUE,
    "slug" text,
    "is_public" bool NOT NULL
);
INSERT INTO "fabrics_section" VALUES(1,'Хлопчатобумажные ткани. 100% хлопок','khlopchatobumazhnye-tkani-100-kh',1);
INSERT INTO "fabrics_section" VALUES(2,'Синтетические ткани. 100% полиэстер','sinteticheskie-tkani-100-poli',1);
CREATE TABLE "fabrics_category" (
    "id" serial NOT NULL PRIMARY KEY,
    "title" text NOT NULL UNIQUE,
    "section_id" integer REFERENCES "fabrics_section" ("id"),
    "slug" text,
    "is_public" bool NOT NULL
);
INSERT INTO "fabrics_category" VALUES(1,'Трикотажные изделия',1,'trikotazhnye-izdeliia',1);
INSERT INTO "fabrics_category" VALUES(2,'Эко-сумки и фартуки',1,'eko-sumki-i-fartuki',1);
INSERT INTO "fabrics_category" VALUES(3,'Халаты и полотенца',1,'khalaty-i-polotentsa',1);
INSERT INTO "fabrics_category" VALUES(4,'Платки',2,'platki',1);
INSERT INTO "fabrics_category" VALUES(5,'Подушки',2,'podushki',1);
INSERT INTO "fabrics_category" VALUES(6,'Куртки и ветровки',2,'kurtki-i-vetrovki',1);
INSERT INTO "fabrics_category" VALUES(7,'Спортивная атрибутика',2,'sportivnaia-atributika',1);
CREATE TABLE "django_content_type" ("id" serial NOT NULL PRIMARY KEY, "name" text NOT NULL, "app_label" text NOT NULL, "model" text NOT NULL, UNIQUE ("app_label", "model"));
INSERT INTO "django_content_type" VALUES(1,'log entry','admin','logentry');
INSERT INTO "django_content_type" VALUES(2,'permission','auth','permission');
INSERT INTO "django_content_type" VALUES(3,'group','auth','group');
INSERT INTO "django_content_type" VALUES(4,'user','auth','user');
INSERT INTO "django_content_type" VALUES(5,'content type','contenttypes','contenttype');
INSERT INTO "django_content_type" VALUES(6,'session','sessions','session');
INSERT INTO "django_content_type" VALUES(7,'Слайд','site_content','indexslider');
INSERT INTO "django_content_type" VALUES(8,'Отзыв','testimonials','testimonial');
INSERT INTO "django_content_type" VALUES(9,'Категория','wares','category');
INSERT INTO "django_content_type" VALUES(10,'_size_choices_model','wares','_size_choices_model');
INSERT INTO "django_content_type" VALUES(11,'_sex_choices_model','wares','_sex_choices_model');
INSERT INTO "django_content_type" VALUES(12,'_ware_colors_model','wares','_ware_colors_model');
INSERT INTO "django_content_type" VALUES(13,'Материал','wares','ware');
INSERT INTO "django_content_type" VALUES(15,'Раздел','fabrics','section');
INSERT INTO "django_content_type" VALUES(16,'Категория','fabrics','category');
INSERT INTO "django_content_type" VALUES(17,'Ткань','fabrics','fabric');
INSERT INTO "django_content_type" VALUES(18,'order','orders','order');
INSERT INTO "django_content_type" VALUES(19,'_ware_images_model','wares','_ware_images_model');
INSERT INTO "django_content_type" VALUES(21,'Тестовый блок','site_content','text');
INSERT INTO "django_content_type" VALUES(22,'Страница','site_content','page');
INSERT INTO "django_content_type" VALUES(23,'order','wares','order');
CREATE TABLE "auth_permission" ("id" serial NOT NULL PRIMARY KEY, "name" text NOT NULL, "content_type_id" integer NOT NULL REFERENCES "django_content_type" ("id"), "codename" text NOT NULL, UNIQUE ("content_type_id", "codename"));
INSERT INTO "auth_permission" VALUES(1,'Can add log entry',1,'add_logentry');
INSERT INTO "auth_permission" VALUES(2,'Can change log entry',1,'change_logentry');
INSERT INTO "auth_permission" VALUES(3,'Can delete log entry',1,'delete_logentry');
INSERT INTO "auth_permission" VALUES(4,'Can add permission',2,'add_permission');
INSERT INTO "auth_permission" VALUES(5,'Can change permission',2,'change_permission');
INSERT INTO "auth_permission" VALUES(6,'Can delete permission',2,'delete_permission');
INSERT INTO "auth_permission" VALUES(7,'Can add group',3,'add_group');
INSERT INTO "auth_permission" VALUES(8,'Can change group',3,'change_group');
INSERT INTO "auth_permission" VALUES(9,'Can delete group',3,'delete_group');
INSERT INTO "auth_permission" VALUES(10,'Can add user',4,'add_user');
INSERT INTO "auth_permission" VALUES(11,'Can change user',4,'change_user');
INSERT INTO "auth_permission" VALUES(12,'Can delete user',4,'delete_user');
INSERT INTO "auth_permission" VALUES(13,'Can add content type',5,'add_contenttype');
INSERT INTO "auth_permission" VALUES(14,'Can change content type',5,'change_contenttype');
INSERT INTO "auth_permission" VALUES(15,'Can delete content type',5,'delete_contenttype');
INSERT INTO "auth_permission" VALUES(16,'Can add session',6,'add_session');
INSERT INTO "auth_permission" VALUES(17,'Can change session',6,'change_session');
INSERT INTO "auth_permission" VALUES(18,'Can delete session',6,'delete_session');
INSERT INTO "auth_permission" VALUES(19,'Can add Слайд',7,'add_indexslider');
INSERT INTO "auth_permission" VALUES(20,'Can change Слайд',7,'change_indexslider');
INSERT INTO "auth_permission" VALUES(21,'Can delete Слайд',7,'delete_indexslider');
INSERT INTO "auth_permission" VALUES(22,'Can add Отзыв',8,'add_testimonial');
INSERT INTO "auth_permission" VALUES(23,'Can change Отзыв',8,'change_testimonial');
INSERT INTO "auth_permission" VALUES(24,'Can delete Отзыв',8,'delete_testimonial');
INSERT INTO "auth_permission" VALUES(25,'Can add Категория',9,'add_category');
INSERT INTO "auth_permission" VALUES(26,'Can change Категория',9,'change_category');
INSERT INTO "auth_permission" VALUES(27,'Can delete Категория',9,'delete_category');
INSERT INTO "auth_permission" VALUES(28,'Can add _size_choices_model',10,'add__size_choices_model');
INSERT INTO "auth_permission" VALUES(29,'Can change _size_choices_model',10,'change__size_choices_model');
INSERT INTO "auth_permission" VALUES(30,'Can delete _size_choices_model',10,'delete__size_choices_model');
INSERT INTO "auth_permission" VALUES(31,'Can add _sex_choices_model',11,'add__sex_choices_model');
INSERT INTO "auth_permission" VALUES(32,'Can change _sex_choices_model',11,'change__sex_choices_model');
INSERT INTO "auth_permission" VALUES(33,'Can delete _sex_choices_model',11,'delete__sex_choices_model');
INSERT INTO "auth_permission" VALUES(34,'Can add _ware_colors_model',12,'add__ware_colors_model');
INSERT INTO "auth_permission" VALUES(35,'Can change _ware_colors_model',12,'change__ware_colors_model');
INSERT INTO "auth_permission" VALUES(36,'Can delete _ware_colors_model',12,'delete__ware_colors_model');
INSERT INTO "auth_permission" VALUES(37,'Can add Материал',13,'add_ware');
INSERT INTO "auth_permission" VALUES(38,'Can change Материал',13,'change_ware');
INSERT INTO "auth_permission" VALUES(39,'Can delete Материал',13,'delete_ware');
INSERT INTO "auth_permission" VALUES(43,'Can add Раздел',15,'add_section');
INSERT INTO "auth_permission" VALUES(44,'Can change Раздел',15,'change_section');
INSERT INTO "auth_permission" VALUES(45,'Can delete Раздел',15,'delete_section');
INSERT INTO "auth_permission" VALUES(46,'Can add Категория',16,'add_category');
INSERT INTO "auth_permission" VALUES(47,'Can change Категория',16,'change_category');
INSERT INTO "auth_permission" VALUES(48,'Can delete Категория',16,'delete_category');
INSERT INTO "auth_permission" VALUES(49,'Can add Ткань',17,'add_fabric');
INSERT INTO "auth_permission" VALUES(50,'Can change Ткань',17,'change_fabric');
INSERT INTO "auth_permission" VALUES(51,'Can delete Ткань',17,'delete_fabric');
INSERT INTO "auth_permission" VALUES(52,'Can add order',18,'add_order');
INSERT INTO "auth_permission" VALUES(53,'Can change order',18,'change_order');
INSERT INTO "auth_permission" VALUES(54,'Can delete order',18,'delete_order');
INSERT INTO "auth_permission" VALUES(55,'Can add _ware_images_model',19,'add__ware_images_model');
INSERT INTO "auth_permission" VALUES(56,'Can change _ware_images_model',19,'change__ware_images_model');
INSERT INTO "auth_permission" VALUES(57,'Can delete _ware_images_model',19,'delete__ware_images_model');
INSERT INTO "auth_permission" VALUES(61,'Can add Тестовый блок',21,'add_text');
INSERT INTO "auth_permission" VALUES(62,'Can change Тестовый блок',21,'change_text');
INSERT INTO "auth_permission" VALUES(63,'Can delete Тестовый блок',21,'delete_text');
INSERT INTO "auth_permission" VALUES(64,'Can add Страница',22,'add_page');
INSERT INTO "auth_permission" VALUES(65,'Can change Страница',22,'change_page');
INSERT INTO "auth_permission" VALUES(66,'Can delete Страница',22,'delete_page');
INSERT INTO "auth_permission" VALUES(67,'Can add order',23,'add_order');
INSERT INTO "auth_permission" VALUES(68,'Can change order',23,'change_order');
INSERT INTO "auth_permission" VALUES(69,'Can delete order',23,'delete_order');
CREATE TABLE "auth_group" ("id" serial NOT NULL PRIMARY KEY, "name" text NOT NULL UNIQUE);
CREATE TABLE "auth_group_permissions" ("id" serial NOT NULL PRIMARY KEY, "group_id" integer NOT NULL REFERENCES "auth_group" ("id"), "permission_id" integer NOT NULL REFERENCES "auth_permission" ("id"), UNIQUE ("group_id", "permission_id"));
CREATE TABLE "auth_user" ("id" serial NOT NULL PRIMARY KEY, "password" text NOT NULL, "last_login" timestamp NOT NULL, "is_superuser" bool NOT NULL, "username" text NOT NULL UNIQUE, "first_name" text NOT NULL, "last_name" text NOT NULL, "email" text NOT NULL, "is_staff" bool NOT NULL, "is_active" bool NOT NULL, "date_joined" timestamp NOT NULL);
INSERT INTO "auth_user" VALUES(1,'pbkdf2_sha256$12000$cCGEGL2H9Msa$eWCYGtDKlwKJmQhqECBf5jkgv2ueTNUZHq1xYlRTgNk=','2015-01-12 14:32:26.123275',1,'sergey','','','',1,1,'2014-12-21 16:11:10.105910');
CREATE TABLE "auth_user_groups" ("id" serial NOT NULL PRIMARY KEY, "user_id" integer NOT NULL REFERENCES "auth_user" ("id"), "group_id" integer NOT NULL REFERENCES "auth_group" ("id"), UNIQUE ("user_id", "group_id"));
CREATE TABLE "auth_user_user_permissions" ("id" serial NOT NULL PRIMARY KEY, "user_id" integer NOT NULL REFERENCES "auth_user" ("id"), "permission_id" integer NOT NULL REFERENCES "auth_permission" ("id"), UNIQUE ("user_id", "permission_id"));
CREATE TABLE "django_admin_log" ("id" serial NOT NULL PRIMARY KEY, "action_time" timestamp NOT NULL, "object_id" text NULL, "object_repr" text NOT NULL, "action_flag" smallint unsigned NOT NULL, "change_message" text NOT NULL, "content_type_id" integer NULL REFERENCES "django_content_type" ("id"), "user_id" integer NOT NULL REFERENCES "auth_user" ("id"));
INSERT INTO "django_admin_log" VALUES(1,'2014-12-21 16:13:29.352588','1','Футболки',1,'',9,1);
INSERT INTO "django_admin_log" VALUES(2,'2014-12-21 16:34:24.497096','1','Футболка с коротким рукавом из кулироной глади с круглым воротом',1,'',13,1);
INSERT INTO "django_admin_log" VALUES(3,'2014-12-21 16:40:27.516733','2','123',1,'',13,1);
INSERT INTO "django_admin_log" VALUES(4,'2014-12-21 16:42:58.412082','1','Футболка с коротким рукавом из кулироной глади с круглым воротом',2,'Добавлен _ware_colors_model "красный".',13,1);
INSERT INTO "django_admin_log" VALUES(5,'2014-12-21 16:45:06.128573','3','Футболка с коротким рукавом из кулироной глади с круглым воротом 2',1,'',13,1);
INSERT INTO "django_admin_log" VALUES(6,'2014-12-21 16:45:23.685652','2','123',3,'',13,1);
INSERT INTO "django_admin_log" VALUES(7,'2014-12-21 16:45:23.687694','1','Футболка с коротким рукавом из кулироной глади с круглым воротом',3,'',13,1);
INSERT INTO "django_admin_log" VALUES(8,'2014-12-21 16:45:55.266689','3','Футболка с коротким рукавом из кулироной глади с круглым воротом 2',2,'Добавлен _sex_choices_model "Женский". Добавлен _sex_choices_model "Унисекс". Добавлен _size_choices_model "S". Добавлен _size_choices_model "M". Добавлен _size_choices_model "L". Добавлен _size_choices_model "XL". Добавлен _size_choices_model "XXL".',13,1);
INSERT INTO "django_admin_log" VALUES(9,'2014-12-21 16:46:12.977314','3','Футболка с коротким рукавом из кулироной глади с круглым воротом',2,'Изменен title.',13,1);
INSERT INTO "django_admin_log" VALUES(10,'2014-12-21 16:53:58.136639','3','Футболка с коротким рукавом из кулироной глади с круглым воротом',2,'Изменен image.',13,1);
INSERT INTO "django_admin_log" VALUES(11,'2014-12-21 17:11:48.085133','3','Футболка с коротким рукавом из кулироной глади с круглым воротом',2,'Добавлен ware images model "WareImagesModel object".',13,1);
INSERT INTO "django_admin_log" VALUES(12,'2014-12-21 17:52:01.733200','3','Футболка с коротким рукавом из кулироной глади с круглым воротом',2,'Добавлен _ware_images_model "_ware_images_model object".',13,1);
INSERT INTO "django_admin_log" VALUES(13,'2014-12-21 17:52:07.538233','3','Футболка с коротким рукавом из кулироной глади с круглым воротом',2,'Удален _ware_images_model "_ware_images_model object".',13,1);
INSERT INTO "django_admin_log" VALUES(14,'2014-12-21 17:52:15.203652','3','Футболка с коротким рукавом из кулироной глади с круглым воротом',2,'Добавлен _ware_images_model "_ware_images_model object".',13,1);
INSERT INTO "django_admin_log" VALUES(15,'2014-12-21 18:31:23.339706','3','Футболка с коротким рукавом из кулироной глади с круглым воротом',2,'Добавлен _ware_colors_model "оранжевый".',13,1);
INSERT INTO "django_admin_log" VALUES(16,'2014-12-21 18:33:06.934982','3','Футболка с коротким рукавом из кулироной глади с круглым воротом',2,'Добавлен _ware_colors_model "зеленый".',13,1);
INSERT INTO "django_admin_log" VALUES(17,'2014-12-22 10:36:22.648725','1','Хлопчатобумажные ткани. 100% хлопок',1,'',15,1);
INSERT INTO "django_admin_log" VALUES(18,'2014-12-22 10:36:34.961053','2','Синтетические ткани. 100% полиэстер',1,'',15,1);
INSERT INTO "django_admin_log" VALUES(19,'2014-12-22 10:37:34.722840','1','Трикотажные изделия',1,'',16,1);
INSERT INTO "django_admin_log" VALUES(20,'2014-12-22 10:37:50.544883','2','Эко-сумки и фартуки',1,'',16,1);
INSERT INTO "django_admin_log" VALUES(21,'2014-12-22 10:38:04.697897','3','Халаты и полотенца',1,'',16,1);
INSERT INTO "django_admin_log" VALUES(22,'2014-12-22 10:38:38.837587','1','Кулирка',1,'',17,1);
INSERT INTO "django_admin_log" VALUES(23,'2014-12-22 10:38:49.368792','2','Интерлок',1,'',17,1);
INSERT INTO "django_admin_log" VALUES(24,'2014-12-22 10:39:00.068381','3','Футер',1,'',17,1);
INSERT INTO "django_admin_log" VALUES(25,'2014-12-22 10:39:13.617459','4','Бязь',1,'',17,1);
INSERT INTO "django_admin_log" VALUES(26,'2014-12-22 10:39:21.578319','5','Двунитка',1,'',17,1);
INSERT INTO "django_admin_log" VALUES(27,'2014-12-22 10:39:28.702251','6','Саржа',1,'',17,1);
INSERT INTO "django_admin_log" VALUES(28,'2014-12-22 10:39:41.952894','7','Вафельное полотно',1,'',17,1);
INSERT INTO "django_admin_log" VALUES(29,'2014-12-22 10:39:53.754232','8','Махровое полотно',1,'',17,1);
INSERT INTO "django_admin_log" VALUES(30,'2014-12-22 10:42:39.040412','4','Плат',1,'',16,1);
INSERT INTO "django_admin_log" VALUES(31,'2014-12-22 10:42:54.423588','5','Подушки',1,'',16,1);
INSERT INTO "django_admin_log" VALUES(32,'2014-12-22 10:43:07.939573','6','Куртки и ветровки',1,'',16,1);
INSERT INTO "django_admin_log" VALUES(33,'2014-12-22 10:43:15.966562','4','Платки',2,'Изменен title.',16,1);
INSERT INTO "django_admin_log" VALUES(34,'2014-12-22 10:43:46.954131','9','Шифон',1,'',17,1);
INSERT INTO "django_admin_log" VALUES(35,'2014-12-22 10:43:56.608718','10','Шармус',1,'',17,1);
INSERT INTO "django_admin_log" VALUES(36,'2014-12-22 10:44:11.552310','11','Мокрый шелк',1,'',17,1);
INSERT INTO "django_admin_log" VALUES(37,'2014-12-22 10:44:24.237892','12','Дешайн',1,'',17,1);
INSERT INTO "django_admin_log" VALUES(38,'2014-12-22 10:44:36.554626','13','Атлас',1,'',17,1);
INSERT INTO "django_admin_log" VALUES(39,'2014-12-22 10:44:45.175490','14','Сатен',1,'',17,1);
INSERT INTO "django_admin_log" VALUES(40,'2014-12-22 10:44:58.582627','15','Микрофибра',1,'',17,1);
INSERT INTO "django_admin_log" VALUES(41,'2014-12-22 10:45:07.466930','16','Габардин',1,'',17,1);
INSERT INTO "django_admin_log" VALUES(42,'2014-12-22 10:45:23.852087','17','Оксфорд',1,'',17,1);
INSERT INTO "django_admin_log" VALUES(43,'2014-12-22 10:45:32.821824','18','Дьюспа',1,'',17,1);
INSERT INTO "django_admin_log" VALUES(44,'2014-12-22 10:45:45.122808','19','Тафета',1,'',17,1);
INSERT INTO "django_admin_log" VALUES(45,'2014-12-22 10:46:03.933742','7','Спортивная атрибутика',1,'',16,1);
INSERT INTO "django_admin_log" VALUES(46,'2014-12-22 10:46:24.107974','20','Ложная сетка',1,'',17,1);
INSERT INTO "django_admin_log" VALUES(47,'2014-12-22 10:46:31.718086','21','Прима',1,'',17,1);
INSERT INTO "django_admin_log" VALUES(48,'2014-12-22 10:50:23.067478','2','Рубашки-поло',1,'',9,1);
INSERT INTO "django_admin_log" VALUES(49,'2014-12-22 10:50:35.032750','3','Толстовки',1,'',9,1);
INSERT INTO "django_admin_log" VALUES(50,'2014-12-22 10:50:45.670188','4','Бейсболки',1,'',9,1);
INSERT INTO "django_admin_log" VALUES(51,'2014-12-22 15:57:56.273544','1','Слайд №1',1,'',7,1);
INSERT INTO "django_admin_log" VALUES(52,'2014-12-22 15:58:05.483247','2','Слайд №2',1,'',7,1);
INSERT INTO "django_admin_log" VALUES(53,'2014-12-22 16:00:39.961694','1','Слайд №1',2,'Изменен is_public.',7,1);
INSERT INTO "django_admin_log" VALUES(54,'2014-12-22 16:00:43.886568','2','Слайд №2',2,'Изменен is_public.',7,1);
INSERT INTO "django_admin_log" VALUES(55,'2014-12-22 16:41:31.532574','17','Оксфорд',2,'Изменен category.',17,1);
INSERT INTO "django_admin_log" VALUES(56,'2014-12-22 17:37:53.511440','1','Кулирка',2,'Изменен description.',17,1);
INSERT INTO "django_admin_log" VALUES(57,'2014-12-22 21:47:08.536557','1','Добро пожаловать на наш сайт',1,'',21,1);
INSERT INTO "django_admin_log" VALUES(58,'2014-12-22 22:05:39.066663','1','Домашняя',1,'',22,1);
INSERT INTO "django_admin_log" VALUES(59,'2014-12-22 22:06:37.702045','1','Домашняя',2,'Ни одно поле не изменено.',22,1);
INSERT INTO "django_admin_log" VALUES(60,'2014-12-22 22:08:21.486747','1','Домашняя',2,'Добавлен Тестовый блок "Добро пожаловать на наш сайт".',22,1);
INSERT INTO "django_admin_log" VALUES(61,'2014-12-23 11:21:32.630519','1','Домашняя',2,'Изменены content для Тестовый блок "Добро пожаловать на наш сайт".',22,1);
INSERT INTO "django_admin_log" VALUES(62,'2014-12-23 11:22:31.436710','1','Домашняя',2,'Добавлен Тестовый блок "Уникальное торговое предложение". Изменены content для Тестовый блок "Добро пожаловать на наш сайт".',22,1);
INSERT INTO "django_admin_log" VALUES(63,'2014-12-23 11:26:55.669106','1','Домашняя',2,'Добавлен Тестовый блок "Слоган". Изменены content для Тестовый блок "Уникальное торговое предложение".',22,1);
INSERT INTO "django_admin_log" VALUES(64,'2014-12-23 11:30:42.650185','2','Ткани',1,'',22,1);
INSERT INTO "django_admin_log" VALUES(65,'2014-12-23 11:56:08.572626','3','Оплата',1,'',22,1);
INSERT INTO "django_admin_log" VALUES(66,'2014-12-23 12:05:44.818871','4','Доставка',1,'',22,1);
INSERT INTO "django_admin_log" VALUES(67,'2014-12-23 12:14:52.140183','5','Контакты',1,'',22,1);
INSERT INTO "django_admin_log" VALUES(68,'2014-12-23 12:21:11.909777','5','Контакты',2,'Добавлен Тестовый блок "Хедер". Добавлен Тестовый блок "Футер".',22,1);
INSERT INTO "django_admin_log" VALUES(69,'2014-12-23 13:06:56.700197','1','Домашняя',2,'Добавлен Тестовый блок "довольных клиентов".',22,1);
INSERT INTO "django_admin_log" VALUES(70,'2014-12-23 13:06:56.758164','1','Домашняя',2,'Добавлен Тестовый блок "довольных клиентов".',22,1);
INSERT INTO "django_admin_log" VALUES(71,'2014-12-23 13:07:16.247715','1','Домашняя',2,'Удален Тестовый блок "довольных клиентов".',22,1);
INSERT INTO "django_admin_log" VALUES(72,'2014-12-23 13:20:42.754049','1','Домашняя',2,'Изменены content для Тестовый блок "довольных клиентов".',22,1);
INSERT INTO "django_admin_log" VALUES(73,'2014-12-23 21:56:24.792471','6','Куртки и ветровки',2,'Ни одно поле не изменено.',16,1);
INSERT INTO "django_admin_log" VALUES(74,'2014-12-23 22:32:27.468826','11','Labore sit consequatur aspernatur voluptatum.',2,'Изменен description, image и is_public.',9,1);
INSERT INTO "django_admin_log" VALUES(75,'2014-12-23 22:32:46.438903','17','Dolorem velit amet eligendi doloribus unde quisquam et.',2,'Изменен description и image.',9,1);
INSERT INTO "django_admin_log" VALUES(76,'2014-12-23 22:33:05.765598','16','Perspiciatis fuga omnis voluptatem tenetur dolorem est.',3,'',9,1);
INSERT INTO "django_admin_log" VALUES(77,'2014-12-23 22:33:05.769005','14','Eos qui voluptate asperiores similique.',3,'',9,1);
INSERT INTO "django_admin_log" VALUES(78,'2014-12-23 22:33:05.770955','10','Autem fugiat vitae cumque unde aliquid quis.',3,'',9,1);
INSERT INTO "django_admin_log" VALUES(79,'2014-12-23 22:33:05.773311','9','Tempora quo eaque ex quis.',3,'',9,1);
INSERT INTO "django_admin_log" VALUES(80,'2014-12-23 22:33:05.775439','8','Est architecto eveniet reprehenderit rerum.',3,'',9,1);
INSERT INTO "django_admin_log" VALUES(81,'2014-12-23 22:33:16.965672','5','Eos debitis aut natus velit.',3,'',9,1);
INSERT INTO "django_admin_log" VALUES(82,'2014-12-23 22:33:49.789755','15','Voluptate nisi perferendis saepe dolore qui perferendis.',2,'Изменен description и image.',9,1);
INSERT INTO "django_admin_log" VALUES(83,'2014-12-23 22:34:00.241295','13','In a eius culpa dolorum quos.',2,'Изменен description и image.',9,1);
INSERT INTO "django_admin_log" VALUES(84,'2014-12-23 22:34:11.369908','12','Eveniet est aliquid sapiente recusandae esse porro vel reprehenderit.',2,'Изменен description и image.',9,1);
INSERT INTO "django_admin_log" VALUES(85,'2014-12-23 22:34:28.467282','7','Velit laudantium similique nulla repellendus.',2,'Изменен description и image.',9,1);
INSERT INTO "django_admin_log" VALUES(86,'2014-12-23 22:34:38.305226','6','Omnis cum inventore suscipit eligendi pariatur.',2,'Изменен description и image.',9,1);
INSERT INTO "django_admin_log" VALUES(87,'2014-12-23 22:41:50.696891','1','Футболки',2,'Изменен is_on_home_page.',9,1);
INSERT INTO "django_admin_log" VALUES(88,'2014-12-23 22:41:58.315542','2','Рубашки-поло',2,'Изменен is_on_home_page.',9,1);
INSERT INTO "django_admin_log" VALUES(89,'2014-12-23 22:42:08.907255','3','Толстовки',2,'Изменен is_on_home_page.',9,1);
INSERT INTO "django_admin_log" VALUES(90,'2014-12-23 22:42:17.549310','4','Бейсболки',2,'Изменен is_on_home_page.',9,1);
INSERT INTO "django_admin_log" VALUES(91,'2014-12-23 22:42:30.518595','6','Omnis cum inventore suscipit eligendi pariatur.',2,'Изменен is_on_home_page.',9,1);
INSERT INTO "django_admin_log" VALUES(92,'2014-12-23 22:42:37.369051','7','Velit laudantium similique nulla repellendus.',2,'Изменен is_on_home_page.',9,1);
INSERT INTO "django_admin_log" VALUES(93,'2014-12-23 22:44:50.821605','3','Футболка с коротким рукавом из кулироной глади с круглым воротом',2,'Добавлен _ware_images_model "_ware_images_model object". Добавлен _ware_images_model "_ware_images_model object". Добавлен _ware_images_model "_ware_images_model object".',13,1);
INSERT INTO "django_admin_log" VALUES(94,'2014-12-23 23:02:20.105989','1','Футболки',2,'Изменен description.',9,1);
INSERT INTO "django_admin_log" VALUES(95,'2014-12-23 23:33:48.780888','20','Molestias rerum in quia magnam perspiciatis mollitia a. Quia numquam eum voluptatem sint iusto. Ut ab cupiditate et eligendi dolorum aspernatur.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(96,'2014-12-23 23:33:48.786829','19','Sit fuga corporis labore et. Omnis et sapiente reiciendis.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(97,'2014-12-23 23:33:48.789800','18','Exercitationem sed vero possimus delectus dolor aut. Nihil aut iste libero expedita aperiam. Ad nemo ea doloremque. Quis nobis voluptatem architecto earum perferendis.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(98,'2014-12-23 23:33:48.791797','17','Sit eligendi sint accusantium quaerat beatae nemo. Et veritatis facilis et vero et fugit nulla laborum. Quo velit ipsam odit et et provident. Distinctio optio quia qui ea.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(99,'2014-12-23 23:33:48.794438','16','Eum dolores et omnis animi omnis dolorem. Labore quo et et natus aut ut impedit recusandae. Cum in asperiores in rerum. Ut voluptatem nihil doloribus.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(100,'2014-12-23 23:33:48.796123','15','Sequi inventore molestiae non placeat commodi quibusdam qui. Fugit unde sit labore. Ea velit assumenda harum commodi sit soluta voluptatem. Veritatis officiis qui quis ipsa.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(101,'2014-12-23 23:33:48.798034','14','Omnis dolorem sit ea tempore totam dolor. Laborum enim dicta dignissimos dolor mollitia non. Repellat hic consectetur neque. Quae illum nihil quibusdam eius ea alias ut.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(102,'2014-12-23 23:33:48.800064','13','Atque id et dolores necessitatibus quis consectetur ea. Velit iure dignissimos sit excepturi reprehenderit. Id et mollitia sapiente excepturi.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(103,'2014-12-23 23:33:48.802213','12','Dolorem optio nulla molestias ducimus non tempore. Iste et ex iste voluptatem. Et atque voluptatum aliquam omnis quas reprehenderit. Esse omnis provident at modi nemo nesciunt dolor.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(104,'2014-12-23 23:33:48.804600','11','Ratione officia rerum nihil recusandae nesciunt voluptatum. Consequatur error quaerat quia aut. Consectetur unde in non quo. Sint veritatis repellendus numquam laudantium consequatur.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(105,'2014-12-23 23:33:48.807007','10','Ipsum totam nisi reiciendis aperiam est natus. Magni maxime aut rerum.
Similique nulla velit unde ipsam quia ut. Dolore earum eos rem possimus quia placeat.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(106,'2014-12-23 23:33:48.808917','9','Est libero mollitia error ex. Est aut voluptatibus unde dicta aperiam. Magnam repellat eos sunt. Et nulla nemo explicabo sit.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(107,'2014-12-23 23:33:48.810762','8','Laborum dolorum maxime beatae. Ut labore officiis eveniet ducimus recusandae temporibus. Quae rerum non cumque delectus enim officiis assumenda.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(108,'2014-12-23 23:33:48.812576','7','Nobis tempora rerum et perspiciatis eligendi aspernatur. Molestiae ipsum omnis quia explicabo eveniet sit velit.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(109,'2014-12-23 23:33:48.814988','6','Alias inventore a sit dolor est voluptas est eaque. Non temporibus deserunt deleniti est perspiciatis corporis voluptatem minima. Ratione iste ducimus laudantium sit. Nulla qui id delectus.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(110,'2014-12-23 23:33:48.817392','5','Modi ipsum non officia ducimus eveniet. Quidem illum velit omnis voluptas dolorem in. Quia quos fugiat velit. Sit non neque est ea.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(111,'2014-12-23 23:33:48.819241','4','Sunt est eius ducimus et et. Possimus animi quidem quam deserunt illum earum. Voluptates exercitationem dicta minima molestias.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(112,'2014-12-23 23:33:48.821075','3','Libero possimus non id. Expedita harum magni vel. Natus voluptas odit est.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(113,'2014-12-23 23:33:48.822907','2','Quo dolorem eligendi at a dolor vitae odit. Est neque illum quia aut exercitationem necessitatibus quis architecto. Labore vel esse laudantium reprehenderit quas soluta ut.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(114,'2014-12-23 23:33:48.825137','1','Animi quis quis facere ducimus. Occaecati qui similique molestiae soluta officiis sit et. Repellendus officia similique corporis eius.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(115,'2014-12-23 23:54:04.496155','94','Accusantium rerum minima voluptatem veritatis. Dolorem ut provident blanditiis sed eos similique. Voluptatum accusantium delectus error assumenda.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(116,'2014-12-23 23:54:04.500688','93','Quod est rerum nulla ipsa corrupti. Ex corrupti voluptate eum sint. Odio porro porro reiciendis exercitationem quo sunt dolore. Temporibus deleniti minima facere.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(117,'2014-12-23 23:54:04.503065','92','Eaque facere officiis ipsam atque tempore voluptatem assumenda. Tenetur et eum necessitatibus doloremque animi sed. Rerum eos eos voluptatem neque quia laudantium vel.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(118,'2014-12-23 23:54:04.504815','91','Adipisci eum suscipit ab reiciendis nostrum. Non voluptas totam provident ea ullam. Et dolores quas in esse.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(119,'2014-12-23 23:54:04.507029','90','Inventore autem quae ad quaerat debitis quae eos. Asperiores vel enim nobis eos ullam id harum. Rerum quaerat culpa aliquam quibusdam.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(120,'2014-12-23 23:54:04.509498','89','Nemo non ullam eius voluptatem. Totam quam ut qui cupiditate itaque laborum. Sed sit corporis a harum nam deleniti. Eaque sunt deserunt voluptas cumque tempore aut repudiandae.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(121,'2014-12-23 23:54:04.511247','88','Sed doloremque perferendis officia vel. Vitae est consequatur temporibus. Officia perspiciatis sapiente consequatur. Praesentium officiis reprehenderit velit quis ea.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(122,'2014-12-23 23:54:04.513365','87','Quidem quod omnis eos sunt. Sit esse iure at et. Voluptatem nemo delectus odit et nisi quia consectetur. Natus repudiandae rerum voluptates et cupiditate labore.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(123,'2014-12-23 23:54:04.515686','86','Est hic dolor sint quia. Velit illo voluptatem voluptas molestias. Alias dolor et exercitationem. Unde voluptatum et id sapiente est nemo ipsa.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(124,'2014-12-23 23:54:04.517646','85','Voluptatem inventore quis est sed quo voluptas quia. Ipsa molestias ad debitis ut placeat. Et enim ipsam doloremque deleniti aut. Eos dolor facere exercitationem rerum in reprehenderit ea sed.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(125,'2014-12-23 23:54:04.519658','84','Iusto autem accusamus laborum et quaerat sed vitae. Voluptatibus rerum enim a enim. Ut eos vel veritatis repudiandae saepe. Dolores enim eos corporis aliquid beatae repudiandae explicabo.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(126,'2014-12-23 23:54:04.522062','83','Natus sed officia nihil tempora facilis adipisci. Labore similique et alias vel esse rem optio. Eveniet aliquid eos rerum ea.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(127,'2014-12-23 23:54:04.523886','82','Et et ducimus vitae ducimus. Quae voluptatem quos id velit. Enim sit in quos.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(128,'2014-12-23 23:54:04.526064','81','Quia officia quo iusto dolor aut delectus velit. Maiores quos voluptatum enim aperiam odit qui. Officia autem aut quae eum aliquid natus odio.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(129,'2014-12-23 23:54:04.528303','80','Illum id occaecati voluptatem commodi aut et. Excepturi voluptas non non reiciendis ea quasi.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(130,'2014-12-23 23:54:04.530564','79','Ipsum atque nostrum aut est. Optio non eaque voluptatum sit odio voluptate.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(131,'2014-12-23 23:54:04.532302','78','Voluptatem reprehenderit et nostrum blanditiis recusandae aliquam aut est. Sint ex delectus et molestiae et. Nostrum dolor dolorem ipsa consequatur amet illo voluptatem blanditiis.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(132,'2014-12-23 23:54:04.534149','77','Fuga enim quis autem dignissimos in deleniti cupiditate. Commodi reiciendis illum maiores tempora id qui.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(133,'2014-12-23 23:54:04.535975','76','Sapiente voluptatem omnis et perferendis. Rerum et et officia voluptatem deleniti est voluptatem.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(134,'2014-12-23 23:54:04.537753','75','Rerum porro debitis molestias quo accusantium temporibus. Eaque ad consectetur corrupti nemo. Adipisci optio blanditiis delectus impedit neque et nulla dolores.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(135,'2014-12-23 23:54:04.540134','74','Laboriosam illo explicabo id laudantium. Sit iure ipsa at perspiciatis. In commodi non reiciendis ipsa necessitatibus animi.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(136,'2014-12-23 23:54:04.542135','73','Consectetur qui ex ipsum est fuga vitae voluptatum. Aut accusantium nisi necessitatibus enim reprehenderit. Magni doloremque deserunt reprehenderit est.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(137,'2014-12-23 23:54:04.544010','72','Ut dolores in adipisci aperiam tempora. Earum sit et magnam rem natus sit omnis. Voluptatem et id rerum qui quo tempore minus laudantium.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(138,'2014-12-23 23:54:04.546134','71','Facilis expedita quis assumenda quia sit quo. Non aspernatur necessitatibus in. Voluptas aut consectetur qui adipisci. Tempore unde molestias numquam iste amet laborum necessitatibus sed.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(139,'2014-12-23 23:54:04.548181','70','Dolores ea aut dolorum. Magni possimus ad voluptas eum quis quibusdam quia vero.
Ipsam ad nam est rerum. Cum quis soluta non.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(140,'2014-12-23 23:54:04.550179','69','Quia temporibus sit in non et ullam corporis. Tenetur voluptatem fugiat et quo quia enim ea. Fugit itaque esse voluptatem quos expedita eos. Illum impedit sint libero omnis optio autem.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(141,'2014-12-23 23:54:04.552242','68','Tempora accusantium reiciendis voluptate qui. Dolorum itaque perspiciatis exercitationem facilis. Eligendi blanditiis quo sint.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(142,'2014-12-23 23:54:04.554157','67','Unde qui reiciendis aperiam qui veritatis expedita. Laudantium eaque et ex modi et iusto deleniti. Eius numquam iusto et nulla id distinctio voluptatem aut.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(143,'2014-12-23 23:54:04.556047','66','Quam reiciendis voluptates sint consequatur. Voluptas qui modi in cupiditate voluptatem ut doloremque. Fuga porro distinctio quo molestias quo.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(144,'2014-12-23 23:54:04.558365','65','Quia dolor earum ut tempore quia eum sed. Quaerat alias nobis eos et qui sed animi. Cumque eos dolores nostrum ducimus.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(145,'2014-12-23 23:54:04.560643','64','Dolore quasi quia quo. Laudantium natus iure commodi voluptate ratione officiis. Vitae libero debitis fugiat eaque voluptas voluptatem harum. Qui aut numquam repellendus et tempora nihil quia.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(146,'2014-12-23 23:54:04.562890','63','Vel ad voluptas labore sed. Voluptas commodi iusto eaque est suscipit molestiae.
Est qui et modi quos cupiditate a quas. Eum sed debitis ratione qui a qui. Rem dicta et officia labore.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(147,'2014-12-23 23:54:04.565303','62','Rerum porro perspiciatis voluptatem consequatur voluptatum aut ea. Ut voluptatem voluptas expedita perspiciatis ipsum.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(148,'2014-12-23 23:54:04.567362','61','Dolor accusamus sit accusantium atque et eligendi debitis. Accusantium ut ducimus cumque adipisci officia. Cumque pariatur repudiandae et error. Molestiae sint eius non minima accusamus est aut.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(149,'2014-12-23 23:54:04.569379','60','Delectus expedita voluptas quos vero nam autem ab. Veniam ut voluptatum architecto fuga ullam. Corporis eveniet atque velit nihil autem temporibus architecto.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(150,'2014-12-23 23:54:04.571310','59','Animi ducimus recusandae libero in vel cumque et. Expedita corrupti ut dolore labore.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(151,'2014-12-23 23:54:04.573500','58','Dolores recusandae non possimus ipsam. Nulla iste et in cum. Ducimus optio quo delectus autem expedita iusto.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(152,'2014-12-23 23:54:04.575338','57','Aut id qui facilis nisi est blanditiis inventore. Dolores nisi harum impedit eaque qui aliquid. Sapiente expedita dolorem harum et autem et. Qui eum unde dolore enim nihil.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(153,'2014-12-23 23:54:04.576983','56','Veritatis sequi exercitationem rem voluptas nesciunt est voluptas. Laborum aut corporis id dolor enim. Ipsum illum voluptas aut at.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(154,'2014-12-23 23:54:04.578755','55','Eaque aut perspiciatis corrupti neque vel. In voluptate vel molestias blanditiis. Nam velit laborum ipsum et.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(155,'2014-12-23 23:54:04.581310','54','Quidem ipsam doloremque tenetur vitae quidem. Et delectus adipisci ut laborum ut enim voluptas. Deserunt aut vel totam et aut nemo dolore. Incidunt nobis voluptatem eveniet est nihil.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(156,'2014-12-23 23:54:04.583487','53','Suscipit blanditiis porro quod non iure omnis sunt. Vel eveniet voluptates voluptatem itaque.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(157,'2014-12-23 23:54:04.585698','52','Dolore delectus aut expedita ullam. Necessitatibus ratione doloremque voluptatem et omnis reprehenderit. Ut quis deleniti laudantium laboriosam perferendis.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(158,'2014-12-23 23:54:04.588156','51','Dolores qui vel dignissimos non. Nesciunt ducimus quia in sed occaecati. Error libero nihil totam rerum voluptas.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(159,'2014-12-23 23:54:04.590299','50','Consequatur accusamus veniam autem est accusamus enim ad. Atque vel voluptatibus eos sed quia. Corrupti et error deserunt quos. Saepe commodi voluptatem aut assumenda aliquam reprehenderit.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(160,'2014-12-23 23:54:04.592563','49','Quia odit sint est earum ut. Officia veniam sequi non recusandae consectetur tenetur. Aperiam voluptatem ea aliquid qui quae.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(161,'2014-12-23 23:54:04.595001','48','Exercitationem rerum autem nam culpa saepe culpa officia molestias. Suscipit amet officia alias qui incidunt. Ipsa maxime maiores nostrum tempora non sed. Quis officiis quasi corrupti in quo.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(162,'2014-12-23 23:54:04.597409','47','Enim cumque totam aut quia rem. Sed minima iure consequatur a et. Tempora dolor sequi tempora aut. Beatae nihil ab harum culpa ex occaecati. Sit aut ea ex omnis laborum assumenda.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(163,'2014-12-23 23:54:04.600074','46','Quaerat tenetur et omnis doloribus consequatur accusantium temporibus. Impedit aperiam aut repellendus.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(164,'2014-12-23 23:54:04.602725','45','Dignissimos aut dolor fugit enim id totam. Impedit error tempore sed molestias ipsum.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(165,'2014-12-23 23:54:04.605306','44','Et beatae fugit officiis quia ea. Modi non delectus molestiae explicabo molestias. Aut est sed deserunt rerum.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(166,'2014-12-23 23:54:04.617341','43','Ipsum ea sit qui dolore et iste. Fugiat ipsum et quas ut.
Perferendis ea quis est maiores dicta dolores. Asperiores qui similique id vel ipsum. Quasi veritatis facere et aut reiciendis.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(167,'2014-12-23 23:54:04.625302','42','Recusandae tempore odio qui id animi qui dolor. Totam quia libero repellat quo. Cupiditate ut velit eum dolores asperiores cumque aspernatur. Possimus quis nihil modi sint.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(168,'2014-12-23 23:54:04.627754','41','Iure aut corporis sed aut. Exercitationem voluptatem vel est. Voluptates voluptas fugit est dignissimos architecto sint.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(169,'2014-12-23 23:54:04.629842','40','Ut consequatur sapiente asperiores qui consequuntur eum quas. Qui sunt excepturi repellendus id. Aut a nihil dolorem soluta explicabo. Vero dolorem soluta corrupti ut voluptate unde et.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(170,'2014-12-23 23:54:04.631998','39','Et voluptatem facilis quibusdam est est nulla. Harum corporis a ipsa soluta at hic rerum incidunt.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(171,'2014-12-23 23:54:04.634128','38','Suscipit illum omnis quod alias. Repellendus maiores consequuntur dolorem maxime enim corporis dolor.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(172,'2014-12-23 23:54:04.636270','37','Vel illo doloremque non et quo. Ut quia voluptatem aliquam. Vel odio nobis rerum maiores. Tenetur sequi voluptas animi consequatur. Ea aut necessitatibus qui sunt.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(173,'2014-12-23 23:54:04.638739','36','Sint repellat deleniti provident quis nulla. Rerum vel itaque fugit. Dolores quia facere et eos cumque culpa.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(174,'2014-12-23 23:54:04.640683','35','A aut dolores necessitatibus voluptatem. Et sint dolor voluptas voluptatem. Doloremque dolores iusto et tempora. Odio quos odio voluptatibus esse minus. Et corrupti dolores iste cum libero magnam.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(175,'2014-12-23 23:54:04.642943','34','Accusamus at aut possimus sequi. Quidem id sit itaque est soluta quia doloribus. Vel vel et assumenda eum aut.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(176,'2014-12-23 23:54:04.645304','33','Soluta delectus accusantium veritatis. Occaecati reiciendis consectetur enim nisi porro dolores ut. Recusandae sint aut assumenda nam.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(177,'2014-12-23 23:54:04.647174','32','Blanditiis aut nesciunt saepe quod. Ea omnis voluptatibus alias repellendus voluptate voluptatem magnam. Voluptas facilis cumque illo.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(178,'2014-12-23 23:54:04.648922','31','Et repudiandae asperiores est. Eveniet veniam facere rem tempora voluptatibus.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(179,'2014-12-23 23:54:04.650586','30','Dolorum voluptas voluptatibus quibusdam asperiores quo est. Autem natus nostrum sed ut. Laborum id dolores blanditiis aut et incidunt laudantium. Iure quos porro unde id repudiandae.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(180,'2014-12-23 23:54:04.652340','29','Quia sed ab quia sed aut et. Error unde et dolorum asperiores. Qui modi mollitia consequatur perferendis vitae sed aut. Similique magnam et molestiae sint molestiae inventore.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(181,'2014-12-23 23:54:04.654274','28','Animi commodi perferendis aut dicta. Sit consequuntur esse id. Eius vero nulla dolores vitae non consequatur libero.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(182,'2014-12-23 23:54:04.656164','27','Fugit quisquam dolorum ullam fuga eum. Quaerat earum vero velit dolores eos est perferendis. Sint nemo aspernatur amet porro. Enim in rerum eos amet officia possimus atque.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(183,'2014-12-23 23:54:04.658050','26','Et alias voluptatem quia rerum cum sed voluptatem. Voluptatem debitis omnis et praesentium et voluptatum et. Enim nam molestias eos voluptas non est.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(184,'2014-12-23 23:54:04.659927','25','Ex tempore ipsum libero necessitatibus. Eaque aut sed voluptatibus ducimus aut. Autem iure consequatur iure sint corrupti atque quisquam. Soluta in qui enim omnis.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(185,'2014-12-23 23:54:04.661805','24','Quia labore debitis animi eos expedita dolor. Et consequuntur aut animi hic. Nihil assumenda ea quia deserunt. Facilis tempora ratione libero facere.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(186,'2014-12-23 23:54:04.663865','23','Tenetur tempora quas eum voluptatem. Quaerat sit et repellendus odio. Officiis dolore in pariatur aut magnam.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(187,'2014-12-23 23:54:04.665548','22','Non rerum id ex iste. Laboriosam rerum sed voluptatem vitae. Expedita eaque harum consequatur explicabo voluptate. Unde ut omnis dolorum ut sapiente incidunt quia.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(188,'2014-12-23 23:54:04.667272','21','Tempore tenetur inventore qui eum at. Magni laborum totam eum. Error vero provident voluptas voluptatem repellendus placeat rerum. Porro quos libero cumque non est enim odio.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(189,'2014-12-23 23:55:15.965621','100','Иванов Петр',2,'Изменен name, phone, colors, sexes, sizes и order_qty.',23,1);
INSERT INTO "django_admin_log" VALUES(190,'2014-12-23 23:55:56.485624','99','Вася Кузьмин',2,'Изменен name, phone, order_items, colors, sexes, sizes и order_qty.',23,1);
INSERT INTO "django_admin_log" VALUES(191,'2014-12-23 23:56:53.825218','98','Маша Иванова',2,'Изменен name, phone, colors, sexes, sizes и order_qty.',23,1);
INSERT INTO "django_admin_log" VALUES(192,'2014-12-24 00:26:25.957614','96','Ullam maiores quasi dolores voluptatem pariatur dolorem. Voluptatem in sed fugiat rerum. Non error necessitatibus nobis temporibus facilis a sequi.',2,'Изменен sexes, sizes и order_qty.',23,1);
INSERT INTO "django_admin_log" VALUES(193,'2014-12-24 00:26:39.172890','97','Omnis doloribus saepe sit consequatur qui neque. Rerum voluptatum omnis officia. Nulla vitae non id dolores hic voluptate ipsa.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(194,'2014-12-24 00:26:39.174697','96','Ullam maiores quasi dolores voluptatem pariatur dolorem. Voluptatem in sed fugiat rerum. Non error necessitatibus nobis temporibus facilis a sequi.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(195,'2014-12-24 00:26:39.176759','95','Maxime distinctio est est. Sit aut est quis et. Itaque et et quasi officia velit.
Nihil pariatur consequatur esse aut eius maiores. Asperiores temporibus quos voluptatem nemo illo praesentium.',3,'',23,1);
INSERT INTO "django_admin_log" VALUES(196,'2014-12-24 18:22:33.038622','1','Слайд №1',2,'Изменен image.',7,1);
INSERT INTO "django_admin_log" VALUES(197,'2014-12-24 18:22:46.585269','2','Слайд №2',2,'Изменен image.',7,1);
INSERT INTO "django_admin_log" VALUES(198,'2014-12-24 19:01:35.819612','3','Футболка с коротким рукавом из кулироной глади с круглым воротом',2,'Изменен image.',13,1);
INSERT INTO "django_admin_log" VALUES(199,'2014-12-24 19:02:02.947696','3','Футболка с коротким рукавом из кулироной глади с круглым воротом',2,'Изменен image.',13,1);
INSERT INTO "django_admin_log" VALUES(200,'2014-12-24 19:21:50.571262','1','Футболки',2,'Изменен image.',9,1);
INSERT INTO "django_admin_log" VALUES(201,'2014-12-26 23:15:51.282489','1','Футболки',2,'Изменен image.',9,1);
INSERT INTO "django_admin_log" VALUES(202,'2014-12-26 23:16:30.406656','2','Рубашки-поло',2,'Изменен image.',9,1);
INSERT INTO "django_admin_log" VALUES(203,'2014-12-26 23:16:43.991721','3','Толстовки',2,'Изменен image.',9,1);
INSERT INTO "django_admin_log" VALUES(204,'2015-01-12 14:32:42.535855','6','Доставка',1,'',22,1);
INSERT INTO "django_admin_log" VALUES(205,'2015-01-12 14:37:05.825348','6','Доставка',2,'Добавлен Тестовый блок "О нас".',22,1);
INSERT INTO "django_admin_log" VALUES(206,'2015-01-12 15:43:09.351575','6','О компании',2,'Изменен alias.',22,1);
INSERT INTO "django_admin_log" VALUES(207,'2015-01-12 15:44:18.648109','6','О компании',2,'Изменены content для Тестовый блок "".',22,1);
INSERT INTO "django_admin_log" VALUES(208,'2015-01-12 15:44:35.871381','6','О компании',2,'Изменены content для Тестовый блок "".',22,1);
INSERT INTO "django_admin_log" VALUES(209,'2015-01-12 16:09:15.147760','7','Документы',1,'',22,1);
INSERT INTO "django_admin_log" VALUES(210,'2015-01-12 17:31:23.685896','7','Документы',2,'Добавлен Тестовый блок "".',22,1);
INSERT INTO "django_admin_log" VALUES(211,'2015-01-12 17:32:46.859832','7','Документы',2,'Изменены attachement для Тестовый блок "".',22,1);
INSERT INTO "django_admin_log" VALUES(212,'2015-01-12 17:32:58.929824','7','Документы',2,'Изменены attachement для Тестовый блок "".',22,1);
INSERT INTO "django_admin_log" VALUES(213,'2015-01-12 17:35:14.741032','7','Документы',2,'Изменены attachement для Тестовый блок "".',22,1);
INSERT INTO "django_admin_log" VALUES(214,'2015-01-12 17:35:42.926082','7','Документы',2,'Изменены attachement для Тестовый блок "".',22,1);
INSERT INTO "django_admin_log" VALUES(215,'2015-01-12 17:36:22.217057','7','Документы',2,'Добавлен Тестовый блок "".',22,1);
INSERT INTO "django_admin_log" VALUES(216,'2015-01-12 17:36:40.427592','7','Документы',2,'Добавлен Тестовый блок "".',22,1);
INSERT INTO "django_admin_log" VALUES(217,'2015-01-12 18:32:25.668069','8','Макеты',1,'',22,1);
INSERT INTO "django_admin_log" VALUES(218,'2015-01-12 18:36:54.993078','8','Макеты',2,'Добавлен Тестовый блок "".',22,1);
INSERT INTO "django_admin_log" VALUES(219,'2015-01-12 18:45:38.168376','9','Нанесение',1,'',22,1);
INSERT INTO "django_admin_log" VALUES(220,'2015-01-12 18:51:23.372288','9','Нанесение',2,'Добавлен Тестовый блок "". Добавлен Тестовый блок "". Добавлен Тестовый блок "". Добавлен Тестовый блок "". Изменены content для Тестовый блок "".',22,1);
INSERT INTO "django_admin_log" VALUES(221,'2015-01-12 19:03:24.683597','9','Нанесение',2,'Изменены content для Тестовый блок "". Изменены content для Тестовый блок "". Изменены content для Тестовый блок "".',22,1);
INSERT INTO "django_admin_log" VALUES(222,'2015-01-12 19:10:10.215832','9','Нанесение',2,'Изменены content для Тестовый блок "".',22,1);
INSERT INTO "django_admin_log" VALUES(223,'2015-01-12 19:11:36.346367','9','Нанесение',2,'Изменены content для Тестовый блок "".',22,1);
INSERT INTO "django_admin_log" VALUES(224,'2015-01-12 19:25:16.715513','9','Нанесение',2,'Изменены content для Тестовый блок "".',22,1);
INSERT INTO "django_admin_log" VALUES(225,'2015-01-12 19:32:03.970829','9','Нанесение',2,'Изменены content для Тестовый блок "". Изменены content для Тестовый блок "".',22,1);
INSERT INTO "django_admin_log" VALUES(226,'2015-01-12 19:32:26.022038','9','Нанесение',2,'Изменены content для Тестовый блок "". Изменены content для Тестовый блок "".',22,1);
INSERT INTO "django_admin_log" VALUES(227,'2015-01-12 19:34:20.364659','9','Нанесение',2,'Изменены content для Тестовый блок "".',22,1);
INSERT INTO "django_admin_log" VALUES(228,'2015-01-12 19:34:34.640044','9','Нанесение',2,'Изменены content для Тестовый блок "".',22,1);
INSERT INTO "django_admin_log" VALUES(229,'2015-01-12 19:35:17.803196','9','Нанесение',2,'Изменены content для Тестовый блок "".',22,1);
INSERT INTO "django_admin_log" VALUES(230,'2015-01-12 19:35:44.204188','9','Нанесение',2,'Изменены content для Тестовый блок "".',22,1);
INSERT INTO "django_admin_log" VALUES(231,'2015-01-12 19:36:52.529555','9','Нанесение',2,'Изменены content для Тестовый блок "".',22,1);
INSERT INTO "django_admin_log" VALUES(232,'2015-01-12 19:50:19.701926','9','Нанесение',2,'Изменены content для Тестовый блок "". Изменены content для Тестовый блок "".',22,1);
CREATE TABLE "django_session" ("session_key" text NOT NULL PRIMARY KEY, "session_data" text NOT NULL, "expire_date" timestamp NOT NULL);
INSERT INTO "django_session" VALUES('ubi6rx7m6m5vfb3zvmbpa70x7x58jiwv','MmUzOGJhZWY1Y2QwNDZjNzA5NjYyYjYyOWQ3ZjgzOWMwMDU4YmIyNTp7Il9hdXRoX3VzZXJfaGFzaCI6ImI2MzY5OTM1ODFiZjc3NDMxNzY1YmM2ZTlkNjczOGJlMzBkYTIyMGMiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2015-01-04 16:13:04.455308');
INSERT INTO "django_session" VALUES('zd7r219bvi28y0o045pvlb794mrpcvdm','MmUzOGJhZWY1Y2QwNDZjNzA5NjYyYjYyOWQ3ZjgzOWMwMDU4YmIyNTp7Il9hdXRoX3VzZXJfaGFzaCI6ImI2MzY5OTM1ODFiZjc3NDMxNzY1YmM2ZTlkNjczOGJlMzBkYTIyMGMiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2015-01-09 23:09:56.613776');
INSERT INTO "django_session" VALUES('93lwzwissqf9ah9qxiln6hhceivjk7mu','MmUzOGJhZWY1Y2QwNDZjNzA5NjYyYjYyOWQ3ZjgzOWMwMDU4YmIyNTp7Il9hdXRoX3VzZXJfaGFzaCI6ImI2MzY5OTM1ODFiZjc3NDMxNzY1YmM2ZTlkNjczOGJlMzBkYTIyMGMiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2015-01-26 14:32:26.125502');
CREATE TABLE "wares__ware_colors_model" ("id" serial NOT NULL PRIMARY KEY, "title" text NOT NULL, "hex_code" text NOT NULL, "ware_fkey_id" integer NOT NULL REFERENCES "wares_ware" ("id"));
INSERT INTO "wares__ware_colors_model" VALUES(2,'красный','ff0000',3);
INSERT INTO "wares__ware_colors_model" VALUES(3,'оранжевый','ffba00',3);
INSERT INTO "wares__ware_colors_model" VALUES(4,'зеленый','35e387',3);
CREATE TABLE "site_content_indexslider" ("id" serial NOT NULL PRIMARY KEY, "image" text NOT NULL, "created_at" timestamp NOT NULL, "is_public" bool NOT NULL);
INSERT INTO "site_content_indexslider" VALUES(1,'index-slider/39c01014f382e8f5e8a272b5a90baf3a.jpg','2014-12-22 15:57:56.167190',1);
INSERT INTO "site_content_indexslider" VALUES(2,'index-slider/d26ca532cacc39fb63947f82a5307f3b.jpg','2014-12-22 15:58:05.396853',1);
CREATE TABLE "testimonials_testimonial" ("id" serial NOT NULL PRIMARY KEY, "author" text NOT NULL, "company" text NOT NULL, "created_at" timestamp NOT NULL, "updated_at" timestamp NOT NULL, "body" text NOT NULL);
CREATE TABLE "site_content_page" ("id" serial NOT NULL PRIMARY KEY, "slug" text NOT NULL, "alias" text NOT NULL);
INSERT INTO "site_content_page" VALUES(1,'domashniaia','Домашняя');
INSERT INTO "site_content_page" VALUES(2,'tkani','Ткани');
INSERT INTO "site_content_page" VALUES(3,'oplata','Оплата');
INSERT INTO "site_content_page" VALUES(4,'dostavka','Доставка');
INSERT INTO "site_content_page" VALUES(5,'kontakty','Контакты');
INSERT INTO "site_content_page" VALUES(6,'o-kompanii','О компании');
INSERT INTO "site_content_page" VALUES(7,'dokumenty','Документы');
INSERT INTO "site_content_page" VALUES(8,'makety','Макеты');
INSERT INTO "site_content_page" VALUES(9,'nanesenie','Нанесение');
CREATE TABLE "orders_order" ("id" serial NOT NULL PRIMARY KEY, "name" text NOT NULL, "phone" text NOT NULL, "email" text NOT NULL, "colors" text NULL, "sexes" text NULL, "sizes" text NULL, "order_items" text NULL, "order_qty" integer NOT NULL);
INSERT INTO "orders_order" VALUES(1,'123asdasd','123332','12@12.12','красный, оранжевый','Женский','M, L','asdasd',12);
INSERT INTO "orders_order" VALUES(2,'123asdasd','123332','12@12.12','красный, оранжевый','Женский','M, L','asdasd',12);
INSERT INTO "orders_order" VALUES(3,'123asdasd','123332','12@12.12','красный, оранжевый','Женский','M, L','asdasd',12);
INSERT INTO "orders_order" VALUES(4,'asd','123332','12@12.12','красный, оранжевый','Женский','M, L','asdasd',12);
CREATE TABLE "wares_category" ("id" serial NOT NULL PRIMARY KEY, "title" text NOT NULL UNIQUE, "slug" text NULL, "description" text NULL, "image" text NULL, "is_public" bool NOT NULL, "is_on_home_page" bool NOT NULL);
INSERT INTO "wares_category" VALUES(1,'Футболки','futbolki','<p>Omnis sit illum eveniet porro provident culpa delectus. Delectus molestiae earum harum tempore. Voluptatem dolorum qui vel et et non.</p>
','categories/14ef47233ceedf6301354d30530d84eb.jpg',1,1);
INSERT INTO "wares_category" VALUES(2,'Рубашки-поло','rubashki-polo','','categories/615acbd2aae9894bf5a1bd34957b6666.jpg',1,1);
INSERT INTO "wares_category" VALUES(3,'Толстовки','tolstovki','','categories/2d41b92b4bb91bbb11d6856ef6d2e9c8.jpg',1,1);
INSERT INTO "wares_category" VALUES(4,'Бейсболки','beisbolki','','categories/f9c5ee4b6448a32871cbd70b74e6b856.jpg',1,1);
INSERT INTO "wares_category" VALUES(6,'Omnis cum inventore suscipit eligendi pariatur.','omnis-cum-inventore-suscipit-e','<p>Omnis sit illum eveniet porro provident culpa delectus. Delectus molestiae earum harum tempore. Voluptatem dolorum qui vel et et non.</p>
','categories/2fcc3e387e90aa1ef2a2cc19297640f1.jpg',1,1);
INSERT INTO "wares_category" VALUES(7,'Velit laudantium similique nulla repellendus.','velit-laudantium-similique-nul','<p>Eius aut quo sunt sit nihil in. Vero assumenda quod quis modi sed impedit. Doloremque corporis inventore sit amet qui dolorem in. Sunt perferendis sint ipsa dolorem qui.</p>
','categories/38d8f1632e0ffcd3c83de78dd2ae77b9.jpg',1,1);
INSERT INTO "wares_category" VALUES(11,'Labore sit consequatur aspernatur voluptatum.','labore-sit-consequatur-asperna','<p>Tempore sunt sint quam ut. Velit aut officiis aut expedita dolorum minus magni. Libero est unde est.</p>
','categories/9c967efd9927052ad0f653e2b7f34f51.jpg',1,0);
INSERT INTO "wares_category" VALUES(12,'Eveniet est aliquid sapiente recusandae esse porro vel reprehenderit.','eveniet-est-aliquid-sapiente-r','<p>Enim ex est et et magni rerum. Eos blanditiis eligendi quibusdam nihil. Facilis et laborum voluptatem excepturi provident dolor. Unde expedita et sapiente hic cumque.</p>
','categories/747af7c6682cd68ad32b2ac2c1f929dd.jpg',1,0);
INSERT INTO "wares_category" VALUES(13,'In a eius culpa dolorum quos.','in-a-eius-culpa-dolorum-quos','<p>Ab temporibus non et animi voluptas voluptas et. Nemo iure occaecati voluptatum. Iure minus omnis itaque doloremque non accusamus.</p>
','categories/c049a9f50a7208bac0a1aefa0ffe939b.jpg',1,0);
INSERT INTO "wares_category" VALUES(15,'Voluptate nisi perferendis saepe dolore qui perferendis.','voluptate-nisi-perferendis-sae','<p>Consequatur dolorem accusantium maxime et eum maxime. Fugiat magnam consequatur totam consequuntur soluta. Ea blanditiis repellendus quas facere beatae aliquid asperiores.</p>
','categories/2c6fe8df0bee06cdbd4e08a83261cb54.jpg',1,0);
INSERT INTO "wares_category" VALUES(17,'Dolorem velit amet eligendi doloribus unde quisquam et.','dolorem-velit-amet-eligendi-do','<p>Ea dolores voluptatem possimus nostrum illo est. Possimus labore rem quas rerum. Enim quos quos consectetur. Perferendis recusandae quibusdam distinctio minima.</p>
','categories/84c6a4d48bb044c958f9f5001bb01379.jpg',1,0);
CREATE TABLE "wares_ware" ("id" serial NOT NULL PRIMARY KEY, "title" text NOT NULL, "slug" text NULL, "consist" text NULL, "image" text NOT NULL, "brand" text NULL, "price" integer NULL, "is_public" bool NOT NULL, "created_at" timestamp NOT NULL, "updated_at" timestamp NOT NULL, "category_id" integer NOT NULL REFERENCES "wares_category" ("id"), UNIQUE ("title", "category_id"));
INSERT INTO "wares_ware" VALUES(3,'Футболка с коротким рукавом из кулироной глади с круглым воротом','futbolka-s-korotkim-rukavom-iz','100% хлопок','wares/futbolki/a74f6bdbf8268c46d3a866ef7ceb654e.jpg','STAN',1000,1,'2014-12-21 16:45:06.116018','2014-12-24 19:02:02.934499',1);
CREATE TABLE "wares__sex_choices_model" ("id" serial NOT NULL PRIMARY KEY, "ware_fk_id" integer NOT NULL REFERENCES "wares_ware" ("id"), "title" text NOT NULL);
INSERT INTO "wares__sex_choices_model" VALUES(3,3,'Мужской');
INSERT INTO "wares__sex_choices_model" VALUES(4,3,'Женский');
INSERT INTO "wares__sex_choices_model" VALUES(5,3,'Унисекс');
CREATE TABLE "wares__size_choices_model" ("id" serial NOT NULL PRIMARY KEY, "ware_fk_id" integer NOT NULL REFERENCES "wares_ware" ("id"), "title" text NOT NULL);
INSERT INTO "wares__size_choices_model" VALUES(2,3,'XS');
INSERT INTO "wares__size_choices_model" VALUES(3,3,'S');
INSERT INTO "wares__size_choices_model" VALUES(4,3,'M');
INSERT INTO "wares__size_choices_model" VALUES(5,3,'L');
INSERT INTO "wares__size_choices_model" VALUES(6,3,'XL');
INSERT INTO "wares__size_choices_model" VALUES(7,3,'XXL');
CREATE TABLE "wares__ware_images_model" ("id" serial NOT NULL PRIMARY KEY, "ware_foreign_k_id" integer NOT NULL REFERENCES "wares_ware" ("id"), "image" text NOT NULL);
INSERT INTO "wares__ware_images_model" VALUES(1,3,'wares/images/b831647feb6c7840b4347b99dff95822.jpg');
INSERT INTO "wares__ware_images_model" VALUES(3,3,'wares/images/26cb35c0a06fa2692e8f0c5c849a495e.jpg');
INSERT INTO "wares__ware_images_model" VALUES(4,3,'wares/images/87235a4a03a036a44b8ab8c962635c1f.jpg');
INSERT INTO "wares__ware_images_model" VALUES(5,3,'wares/images/637f8942f1e3f643e6665b50d259eda0.jpg');
INSERT INTO "wares__ware_images_model" VALUES(6,3,'wares/images/b86fddcae027983f469d74b7c23b9e2a.jpg');
CREATE TABLE "fabrics_fabric" ("id" serial NOT NULL PRIMARY KEY, "title" text NOT NULL, "description" text NOT NULL, "slug" text NULL, "is_public" bool NOT NULL, "category_id" integer NOT NULL REFERENCES "fabrics_category" ("id"));
INSERT INTO "fabrics_fabric" VALUES(1,'Кулирка','Трикотажное гладкое полотно, имеет лицо и изнаночную
сторону, с лицевой стороны полотно состоит из вертикальных
«косичек», с изнаночной стороны горизонтальная «кирпичная
кладка». Из трикотажных тканей, кулирка, считается самым
тонким полотном, что делает ткань легким и комфортным.
Чаще всего из кулирного полотна: футболки, ночные рубашки,
пижамы, легкие платья. Состав ткани, как правило 100% хб,
но также существует кулирка с добавлением небольшого
процента лайкры.','kulirka',1,1);
INSERT INTO "fabrics_fabric" VALUES(2,'Интерлок','','interlok',1,1);
INSERT INTO "fabrics_fabric" VALUES(3,'Футер','','futer',1,1);
INSERT INTO "fabrics_fabric" VALUES(4,'Бязь','','biaz',1,2);
INSERT INTO "fabrics_fabric" VALUES(5,'Двунитка','','dvunitka',1,2);
INSERT INTO "fabrics_fabric" VALUES(6,'Саржа','','sarzha',1,2);
INSERT INTO "fabrics_fabric" VALUES(7,'Вафельное полотно','','vafelnoe-polotno',1,3);
INSERT INTO "fabrics_fabric" VALUES(8,'Махровое полотно','','makhrovoe-polotno',1,3);
INSERT INTO "fabrics_fabric" VALUES(9,'Шифон','','shifon',1,4);
INSERT INTO "fabrics_fabric" VALUES(10,'Шармус','','sharmus',1,4);
INSERT INTO "fabrics_fabric" VALUES(11,'Мокрый шелк','','mokryi-shelk',1,4);
INSERT INTO "fabrics_fabric" VALUES(12,'Дешайн','','deshain',1,4);
INSERT INTO "fabrics_fabric" VALUES(13,'Атлас','','atlas',1,5);
INSERT INTO "fabrics_fabric" VALUES(14,'Сатен','','saten',1,5);
INSERT INTO "fabrics_fabric" VALUES(15,'Микрофибра','','mikrofibra',1,5);
INSERT INTO "fabrics_fabric" VALUES(16,'Габардин','','gabardin',1,5);
INSERT INTO "fabrics_fabric" VALUES(17,'Оксфорд','','oksford',1,6);
INSERT INTO "fabrics_fabric" VALUES(18,'Дьюспа','','diuspa',1,6);
INSERT INTO "fabrics_fabric" VALUES(19,'Тафета','','tafeta',1,6);
INSERT INTO "fabrics_fabric" VALUES(20,'Ложная сетка','','lozhnaia-setka',1,7);
INSERT INTO "fabrics_fabric" VALUES(21,'Прима','','prima',1,7);
CREATE TABLE "site_content_text" ("id" serial NOT NULL PRIMARY KEY, "header" text NOT NULL, "content" text NOT NULL, "created_at" timestamp NOT NULL, "updated_at" timestamp NOT NULL, "page_id" integer NULL REFERENCES "site_content_page" ("id"), "attachement" text NULL);
INSERT INTO "site_content_text" VALUES(1,'Добро пожаловать на наш сайт','<p style="text-align:center">Уже более 5 лет мы занимаемся производством текстильной корпоративной рекламно- сувенирной продукции от простых футболок до бескаркасной мебели, имея за плечами большой опыт сотрудничества с крупнейшими компаниями, можем обеспечить качественный сервис и индивидуальный подход. Наша задача сделать ваш бизнес узнаваемым. Наша миссия делать мир прекрасным</p>
','2014-12-23 11:22:31.434860','2014-12-22 22:08:21.485000',1,NULL);
INSERT INTO "site_content_text" VALUES(2,'Уникальное торговое предложение','<p style="text-align:center">Приветствую Вас на сайте нашей компании. Уже более 5 лет мы занимаемся производством текстильной корпоративной рекламно-сувенирной продукции от простых футболок до бескаркасной мебели, имея за плечами большой опыт сотрудничества с крупнейшими компаниями, можем обеспечить качественный сервис и индивидуальный подход. Наша задача сделать ваш бизнес узнаваемым. Наша миссия делать мир прекрасным</p>
','2014-12-23 11:26:55.665776','2014-12-23 11:22:31.436113',1,NULL);
INSERT INTO "site_content_text" VALUES(3,'Слоган','<p>Печать на футболках<br />
оптом и от одной штуки<br />
с доставкой по России</p>
','2014-12-23 11:26:55.667691','2014-12-23 11:26:55.667732',1,NULL);
INSERT INTO "site_content_text" VALUES(4,'Ткани','<p>Тщательно подобранная ткань придает изделию изысканность, законченность и приятные тактильные ощущения. Все ткани, используемые при пошиве изделий на заказ, подобраны с особой тщательностью, имеют международную сертификацию и гипоаллергенны.</p>
','2014-12-23 11:30:42.649112','2014-12-23 11:30:42.649164',2,NULL);
INSERT INTO "site_content_text" VALUES(5,'Оплата заказа','<p>Оплатить заказ можно двумя способами: безналичным расчетом и банковской картой</p>
','2014-12-23 11:56:08.571047','2014-12-23 11:56:08.571104',3,NULL);
INSERT INTO "site_content_text" VALUES(6,'Безналичный расчет','<p>Сотрудником компании предоставляется счет с печатью посредством e-mail, который можно оплатить банковским переводом. Физические лица могут оплатить счет в любом отделении банка.</p>
','2014-12-23 11:56:08.571683','2014-12-23 11:56:08.571731',3,NULL);
INSERT INTO "site_content_text" VALUES(7,'Банковской картой','<p>Сотрудником компании по средствам e-mail, предоставляется ссылка на счет, который можно оплатить в течении 3-х дней. Оплата производится без комиссий</p>
','2014-12-23 11:56:08.572066','2014-12-23 11:56:08.572094',3,NULL);
INSERT INTO "site_content_text" VALUES(8,'Доставка','<p>Вам необходима доставка? Не волнуйтесь, мы осуществляем доставку по всей России</p>
','2014-12-23 12:05:44.815541','2014-12-23 12:05:44.815594',4,NULL);
INSERT INTO "site_content_text" VALUES(9,'Самовывоз','<p>Вы можете забратьзаказ сами в нашем магазине<br />
по адресу: ул. Большая Черемушкинская, дом 25, стр. 97</p>
','2014-12-23 12:05:44.816161','2014-12-23 12:05:44.816197',4,NULL);
INSERT INTO "site_content_text" VALUES(10,'Курьером','<p>Доставка малогабаритных грузов пешим курьером, в пределах МКАД от 500 рублей</p>
','2014-12-23 12:05:44.816584','2014-12-23 12:05:44.816625',4,NULL);
INSERT INTO "site_content_text" VALUES(11,'Крупногабаритные грузы','<p>Доставка крупногабаритных грузов<br />
от 1 000 рублей в пределах МКАД</p>
','2014-12-23 12:05:44.817536','2014-12-23 12:05:44.817575',4,NULL);
INSERT INTO "site_content_text" VALUES(12,'Доставка по России','<p>Доставка по России транспортной компанией Деловые линии. ПЭК, Байкал-Сервис. Доставка любой другой транспортной компанией 700 руб. Доставка от транспортной компании до пункта назначения оплачивается при получении заказа</p>
','2014-12-23 12:05:44.817971','2014-12-23 12:05:44.818004',4,NULL);
INSERT INTO "site_content_text" VALUES(13,'Контакты','<p>Адрес: Москва, Большая Черемушкинская, дом 25, строение 97<br />
Время работы: Пн-Пт с 10.00 до 19.00</p>
','2014-12-23 12:14:52.138249','2014-12-23 12:14:52.138339',5,NULL);
INSERT INTO "site_content_text" VALUES(14,'Хедер','<p>8 (499) 398-18-35</p>

<p><a href="mailto:info@scortex.ru">info@scortex.ru</a></p>
','2014-12-23 12:21:11.908163','2014-12-23 12:21:11.908218',5,NULL);
INSERT INTO "site_content_text" VALUES(15,'Футер','<p>8 (499) 398-18-35</p>

<p><a href="mailto:info@scortex.ru">info@scortex.ru</a></p>

<p>Москва, Б. Черемушкинская, д. 25, с. 97</p>

<p>Пн-Пт: с 10.00 - 19.00</p>
','2014-12-23 12:21:11.908836','2014-12-23 12:21:11.908872',5,NULL);
INSERT INTO "site_content_text" VALUES(16,'довольных клиентов','<p>1009000</p>
','2014-12-23 13:20:42.748025','2014-12-23 13:06:56.698695',1,NULL);
INSERT INTO "site_content_text" VALUES(17,'О нас','<p>Компания Scortex &mdash; современный и иннновационный подход к разработке и пошиву текстильной промо продукции. Scortex &mdash; это 5 лет успешной работы на рынке промо продукции, за это время компания завоевала признание многих крупных компаний. Постоянными партнерами, которой являтся компания Nvidia, Кира Пластинина студио, Актион Диджитал, VSN-realty.</p>

<p>&nbsp;</p>

<p>Направление работы компании Scortex: разработка, пошив тестильной сувенирной продукции высокого качества. Scortex &mdash; это своевремнный качественный сервис: использование совершенного высокотехнологичного оборудования и сервисов в работе.</p>
','2015-01-12 15:44:35.869103','2015-01-12 14:37:05.823043',6,NULL);
INSERT INTO "site_content_text" VALUES(18,'Документы','<p>Одним из приоритетов Scortex является прозрачность ценовой политики и работы в целом. В этом разделе можно скачать все необходимы документы для работы с компанией</p>
','2015-01-12 16:09:15.146724','2015-01-12 16:09:15.146798',7,NULL);
INSERT INTO "site_content_text" VALUES(19,'Название документа 1','','2015-01-12 17:35:42.922839','2015-01-12 17:31:23.684638',7,'page-attachements/photo-1420212648922-8515eb4de961.jpeg');
INSERT INTO "site_content_text" VALUES(20,'Название документа 2','','2015-01-12 17:36:22.214366','2015-01-12 17:36:22.215736',7,'page-attachements/YOfYx7zhTvYBGYs6g83s-IMG-8643.jpg');
INSERT INTO "site_content_text" VALUES(21,'Название документа 3','','2015-01-12 17:36:40.426513','2015-01-12 17:36:40.426627',7,'page-attachements/Snimok-ekrana-2015-01-06-v-7-39-15.png');
INSERT INTO "site_content_text" VALUES(22,'Макеты','Основа работы Scortex — доступность и предоставление качественного сервиса 24 часа в сутки.
В этом разделе вы найдете все необходимые макеты в кривых (в векторе) для самостоятельного
создания портотипа вашего изделия. ','2015-01-12 18:36:54.990818','2015-01-12 18:36:54.990863',8,'');
INSERT INTO "site_content_text" VALUES(23,'Нанесение','<p>Нанесение логотипа на одежду или любое текстильное изделие &mdash; это отдельная индустрия в производстве рекламного текстиля, в которой много нюансов и деталей. Самыми распространенными методами нанесения логотипов на текстиль являются: шелкография, термотрансферное нанесение, сублимационное нанесение и вышивка</p>
','2015-01-12 18:51:23.367206','2015-01-12 18:45:38.166311',9,'');
INSERT INTO "site_content_text" VALUES(24,'Шелкография или прямой метод нанесения','<p>Под этим методом подразумевают прямой контакт краски и текстиля, как правило, нанесение производится на изделия из хлопковых и нейлоновых тканей. Шелкография подразделяется на несколько подвидов печати, с применением различного оборудования, выбор которого зависит от характера печати и количества изделий</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<table>
	<tbody>
		<tr>
			<th>&nbsp;</th>
			<th>Карусельный станок</th>
			<th>Цифровой принтер</th>
			<th>Рулонное оборудование</th>
		</tr>
		<tr>
			<td>Минимальное количество изделий</td>
			<td>От 30 штук и более</td>
			<td>От 1 до 30 штук</td>
			<td>Печать от 10 квадратных метров</td>
		</tr>
		<tr>
			<td>Технология нанесения</td>
			<td>По изделию проводят<br />
			ракелем с краской, после<br />
			чего краска попадает<br />
			на изделие</td>
			<td>Изделие находится<br />
			в принтере, краска наносится<br />
			с помощью печатающей<br />
			головки</td>
			<td>Рулон ткани заправляют<br />
			в специальный аппарат,<br />
			ткань и краска подаются<br />
			одновременно</td>
		</tr>
		<tr>
			<td>Ценообразование</td>
			<td>Количество изделий, цветов, сложность нанесения</td>
			<td>Количество изделий</td>
			<td>Количество квадратных метров печати</td>
		</tr>
	</tbody>
</table>
','2015-01-12 19:50:19.696640','2015-01-12 18:51:23.370147',9,'');
INSERT INTO "site_content_text" VALUES(25,'Термотрансферное нанесение','<p>Метод заключается в использовании заготовки нанесения, которая переносится с использованием высоких температур, как правило, нанесение производится на изделия из хлопковых и нейлоновых тканей. Заготовки, называют термотрансферами, термотрансфера бывают нескольких видов:</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<table>
	<tbody>
		<tr>
			<th>&nbsp;</th>
			<th>Пластизоль</th>
			<th>Флок</th>
			<th>Флекс</th>
		</tr>
		<tr>
			<td>количество цветов печати</td>
			<td>До 6 цветов</td>
			<td>1 цвет</td>
			<td>1 цвет</td>
		</tr>
		<tr>
			<td>характеристика</td>
			<td>Краска, нанесенная на пленку</td>
			<td>Гладкая пленка</td>
			<td>Ворсистая пленка, напоминающая замшу</td>
		</tr>
		<tr>
			<td>ценообразование</td>
			<td>Площадь печати, количество изделий</td>
			<td>Площадь вещи</td>
			<td>Площадь вещи</td>
		</tr>
	</tbody>
</table>
','2015-01-12 19:50:19.697857','2015-01-12 18:51:23.371003',9,'');
INSERT INTO "site_content_text" VALUES(26,'Сублимационное нанесение','<p>Метод нанесения заключается в пропитке ткани краской, краска становится структурой ткани, нанесение становится не заметным на ощупь, отличительная черта метода &mdash; нанесение исключительно на синтетические ткани</p>
','2015-01-12 19:03:24.679351','2015-01-12 18:51:23.371433',9,'');
INSERT INTO "site_content_text" VALUES(27,'Вышивка','<p>Метод нанесения принципиально отличается от других, так как при вышивке используются полиэфирные шелковые нитки. В зависимости от программы можно варьировать качество и скорость вышивки</p>
','2015-01-12 19:03:24.681587','2015-01-12 18:51:23.371770',9,'');
CREATE TABLE "wares_order" ("id" serial NOT NULL PRIMARY KEY, "name" text NOT NULL, "email" text NOT NULL, "order_items" text NULL, "colors" text NULL, "sexes" text NULL, "sizes" text NULL, "order_qty" integer NOT NULL, "created_at" timestamp NOT NULL, "updated_at" timestamp NOT NULL, "phone" text NOT NULL);
INSERT INTO "wares_order" VALUES(98,'Маша Иванова','yundt.abel@streich.com','Adipisci non et aut ab minus dolore labore. Dicta doloremque et fuga quos libero. Incidunt est quam vel dolor. Fugit saepe voluptatem quibusdam cumque. Velit ab molestiae incidunt sit.','белый','женский','S, M, L',1000,'2014-12-24 00:02:02.249454','2014-12-24 00:02:20.651186','1-383-103-42');
INSERT INTO "wares_order" VALUES(99,'Вася Кузьмин','stroman.karlie@yahoo.com','Omnis magnam inventore magni sapiente eius aut. Quaerat impedit eum sapiente corrupti voluptatum.Sunt consequuntur commodi est at corrupti doloremque eos. Quos magnam velit dolores nisi. Dolorem perferendis voluptas reiciendis et eius.','фиолетовый','мужской','M, XL, XXL',500,'2014-12-24 00:02:02.249454','2014-12-24 00:02:20.651186','957-141-8007');
INSERT INTO "wares_order" VALUES(100,'Иванов Петр','hilpert.haskell@gmail.com','Nam maiores est ipsam voluptas enim fuga. Voluptas distinctio accusantium expedita vel recusandae ut. Vero et aut et provident. Hic aliquam recusandae laudantium nesciunt quos.','красный, зеленый','женский, унисекс','S, M, XXL',300,'2014-12-24 00:02:02.249454','2014-12-24 00:02:20.651186','656-034-422');
DELETE FROM sqlite_sequence;
INSERT INTO "sqlite_sequence" VALUES('django_content_type',23);
INSERT INTO "sqlite_sequence" VALUES('django_migrations',38);
INSERT INTO "sqlite_sequence" VALUES('auth_permission',69);
INSERT INTO "sqlite_sequence" VALUES('auth_user',1);
INSERT INTO "sqlite_sequence" VALUES('django_admin_log',232);
INSERT INTO "sqlite_sequence" VALUES('wares__ware_colors_model',4);
INSERT INTO "sqlite_sequence" VALUES('fabrics_section',2);
INSERT INTO "sqlite_sequence" VALUES('fabrics_category',7);
INSERT INTO "sqlite_sequence" VALUES('site_content_indexslider',2);
INSERT INTO "sqlite_sequence" VALUES('testimonials_testimonial',0);
INSERT INTO "sqlite_sequence" VALUES('site_content_page',9);
INSERT INTO "sqlite_sequence" VALUES('orders_order',4);
INSERT INTO "sqlite_sequence" VALUES('wares_category',17);
INSERT INTO "sqlite_sequence" VALUES('wares_ware',3);
INSERT INTO "sqlite_sequence" VALUES('wares__sex_choices_model',5);
INSERT INTO "sqlite_sequence" VALUES('wares__size_choices_model',7);
INSERT INTO "sqlite_sequence" VALUES('wares__ware_images_model',6);
INSERT INTO "sqlite_sequence" VALUES('fabrics_fabric',21);
INSERT INTO "sqlite_sequence" VALUES('site_content_text',27);
INSERT INTO "sqlite_sequence" VALUES('wares_order',100);
CREATE INDEX "wares_ware_colors_choices_acf0cab0" ON "wares_ware_colors_choices" ("ware_id");
CREATE INDEX "wares_ware_colors_choices_7a54911a" ON "wares_ware_colors_choices" ("_ware_colors_model_id");
CREATE INDEX "wares_ware_sexes_choices_acf0cab0" ON "wares_ware_sexes_choices" ("ware_id");
CREATE INDEX "wares_ware_sexes_choices_407f3172" ON "wares_ware_sexes_choices" ("_sex_choices_model_id");
CREATE INDEX "wares_ware_sizes_choices_acf0cab0" ON "wares_ware_sizes_choices" ("ware_id");
CREATE INDEX "wares_ware_sizes_choices_5fbba221" ON "wares_ware_sizes_choices" ("_size_choices_model_id");
CREATE INDEX "fabrics_category_b402b60b" ON "fabrics_category" ("section_id");
CREATE INDEX auth_permission_417f1b1c ON "auth_permission" ("content_type_id");
CREATE INDEX auth_group_permissions_0e939a4f ON "auth_group_permissions" ("group_id");
CREATE INDEX auth_group_permissions_8373b171 ON "auth_group_permissions" ("permission_id");
CREATE INDEX auth_user_groups_e8701ad4 ON "auth_user_groups" ("user_id");
CREATE INDEX auth_user_groups_0e939a4f ON "auth_user_groups" ("group_id");
CREATE INDEX auth_user_user_permissions_e8701ad4 ON "auth_user_user_permissions" ("user_id");
CREATE INDEX auth_user_user_permissions_8373b171 ON "auth_user_user_permissions" ("permission_id");
CREATE INDEX django_admin_log_417f1b1c ON "django_admin_log" ("content_type_id");
CREATE INDEX django_admin_log_e8701ad4 ON "django_admin_log" ("user_id");
CREATE INDEX django_session_de54fa62 ON "django_session" ("expire_date");
CREATE INDEX wares__ware_colors_model_6cfc22b5 ON "wares__ware_colors_model" ("ware_fkey_id");
CREATE INDEX wares_ware_b583a629 ON "wares_ware" ("category_id");
CREATE INDEX wares__sex_choices_model_9ee275c1 ON "wares__sex_choices_model" ("ware_fk_id");
CREATE INDEX wares__size_choices_model_9ee275c1 ON "wares__size_choices_model" ("ware_fk_id");
CREATE INDEX wares__ware_images_model_e208214a ON "wares__ware_images_model" ("ware_foreign_k_id");
CREATE INDEX fabrics_fabric_b583a629 ON "fabrics_fabric" ("category_id");
CREATE INDEX site_content_text_1a63c800 ON "site_content_text" ("page_id");
COMMIT;
