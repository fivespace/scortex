# -*- coding: utf-8 -*-

from django.apps import AppConfig

class TestimonialsConfig(AppConfig):
    name='testimonials'
    verbose_name=u'Отзывы'