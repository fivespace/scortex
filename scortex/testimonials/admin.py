from django.contrib import admin
from .models import Testimonial

@admin.register(Testimonial)
class TestimonialAdmin(admin.ModelAdmin):
    list_display = ['__unicode__', 'updated_at']
    list_filter = ['updated_at']
    search_fields = ['author', 'company']
    readonly_fields = ['created_at', 'updated_at']
