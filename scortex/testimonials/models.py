# -*- coding: utf-8 -*-
from django.db import models
from ckeditor.fields import RichTextField

class Testimonial(models.Model):
    author = models.CharField(max_length=200, verbose_name=u'Автор')
    body = RichTextField(verbose_name=u'Содержание')
    company = models.CharField(max_length=200, verbose_name=u'Компания')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=u'Создан')
    updated_at = models.DateTimeField(auto_now=True, verbose_name=u'Обновлен')

    def __unicode__(self):
        return u"%s из %s" % (self.author, self.company)

    class Meta:
        verbose_name=u'Отзыв'
        verbose_name_plural=u'Отзывы'

